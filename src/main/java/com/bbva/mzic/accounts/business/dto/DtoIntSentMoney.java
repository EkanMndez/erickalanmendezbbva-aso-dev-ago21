package com.bbva.mzic.accounts.business.dto;

import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement(name = "sentMoney", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlType(name = "sentMoney", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntSentMoney implements Serializable {

    @ApiModelProperty(value = "Digital check monetary amount.")
    //@XmlJavaTypeAdapter(BigDecimalFlatAdapter.class)
    private BigDecimal amount;

    @ApiModelProperty(value = "String based on ISO-4217 for specifying the currency related to the digital check.")
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
