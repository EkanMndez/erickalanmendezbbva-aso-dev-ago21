package com.bbva.mzic.accounts.business.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement(name = "digitalCheck", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlType(name = "digitalCheck", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntDigitalCheck implements Serializable {

    @ApiModelProperty(value = "Unique identifier of the digital ckeck.")
    private String id;

    @ApiModelProperty(value = "Number of digital check.")
    private Long number;

    @ApiModelProperty(value = "Short description related to the digital check.")
    private String concept;

    @ApiModelProperty(value = "String based on ISO-8601 date format for providing the date to operation date.")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date operationDate;

    @ApiModelProperty(value = "Validation code for the digital check")
    private String codeCheck;

    @ApiModelProperty(value = "String based on ISO-8601 date format for providing the expiration date for cashing the digital check")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date dueDate;

    @ApiModelProperty(value = "Contains the operation number of the digital check.")
    private String operationNumber;

    @ApiModelProperty(value = "Receiver Cell and holdername.")
    private DtoIntReceiver receiver;

    @ApiModelProperty(value = "Amount of the digital check")
    private Money sentMoney;

    @ApiModelProperty(value = "Status of digital check.")
    private DtoIntDigitalCheckStatus digitalCheckStatus;

    @ApiModelProperty(value = "Specify digital check type.")
    private DtoIntDigitalCheckType digitalCheckType;

    @ApiModelProperty(value = "Information related to digital check cashout.")
    private DtoIntCashout cashout;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public String getCodeCheck() {
        return codeCheck;
    }

    public void setCodeCheck(String codeCheck) {
        this.codeCheck = codeCheck;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public DtoIntReceiver getReceiver() {
        return receiver;
    }

    public void setReceiver(DtoIntReceiver receiver) {
        this.receiver = receiver;
    }

    public Money getSentMoney() {
        return sentMoney;
    }

    public void setSentMoney(Money sentMoney) {
        this.sentMoney = sentMoney;
    }

    public DtoIntDigitalCheckStatus getDigitalCheckStatus() {
        return digitalCheckStatus;
    }

    public void setDigitalCheckStatus(DtoIntDigitalCheckStatus digitalCheckStatus) {
        this.digitalCheckStatus = digitalCheckStatus;
    }

    public DtoIntDigitalCheckType getDigitalCheckType() {
        return digitalCheckType;
    }

    public void setDigitalCheckType(DtoIntDigitalCheckType digitalCheckType) {
        this.digitalCheckType = digitalCheckType;
    }

    public DtoIntCashout getCashout() {
        return cashout;
    }

    public void setCashout(DtoIntCashout cashout) {
        this.cashout = cashout;
    }
}
