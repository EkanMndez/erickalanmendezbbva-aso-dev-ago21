package com.bbva.mzic.accounts.business.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.mzic.accounts.business.ISrvIntAccountsV0;
import com.bbva.mzic.accounts.business.dto.*;
import com.bbva.mzic.accounts.dao.IAccountsDaoV0;
import com.bbva.mzic.accounts.rm.enums.DigitalCheckStatusEnum;
import com.bbva.mzic.accounts.rm.enums.StatusSavingGoalEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class SrvIntAccountsV0 implements ISrvIntAccountsV0 {

    @Autowired
    IAccountsDaoV0 accountsDaoV0;

    @Override
    public DtoIntAccount getAccount(DtoIntFilterAccount filterAccount) {

        if (filterAccount == null) {
            return null;
        }

        return new DtoIntAccount();
    }

    @Override
    public DtoIntAggregatedAvailableBalance getBalance(DtoIntFilterAccount filterAccount) {
        return accountsDaoV0.getBalanceBGL4(filterAccount);
    }

    @Override
    public DtoIntSavingGoalList listSavingGoals(DtoIntFilterAccount filterAccount) {
        DtoIntSavingGoalList savingGoalList = new DtoIntSavingGoalList();
        final List<DtoIntSavingGoal> savingGoals = new ArrayList<>();
        final DtoIntSavingGoal savingGoal = new DtoIntSavingGoal();
        final DtoIntStatusSavingGoal statusSavingGoal = new DtoIntStatusSavingGoal();

        if (!StringUtils.isBlank(filterAccount.getCustomerId())) {
            statusSavingGoal.setId(StatusSavingGoalEnum.ACTIVATED);
            savingGoal.setId("41oi423u24");
            savingGoal.setSavingGoalNumber("1839125");
            savingGoal.setStatus(statusSavingGoal);

            savingGoals.add(savingGoal);

            savingGoalList.setListSavingGoals(savingGoals);
        }

        return savingGoalList;
    }

    @Override
    public DtoIntDigitalCheckList listDigitalCheckMBGDTCHD(DtoIntFilterAccount filterAccount) {
        Pagination.Links links = new Pagination.Links();
        String urlBase, urlFirstPage;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        DtoIntDigitalCheckList digitalCheckList = accountsDaoV0.listDigitalCheckMBGDTCHD(filterAccount);

        urlFirstPage = "/accounts/v0/accounts/" + filterAccount.getAccountId() + "/digital-checks?" +
                "&fromOperationDate=" + formatter.format(filterAccount.getFromOperationDate()) +
                "&toOperationDate=" + formatter.format(filterAccount.getToOperationDate()) + "&isHistorical=" + filterAccount.getHistorical();
        urlBase = "/accounts/v0/accounts/" + filterAccount.getAccountId() + "/digital-checks?" +
                "paginationKey=" + filterAccount.getPaginationKey() + "&fromOperationDate=" +
                formatter.format(filterAccount.getFromOperationDate()) + "&toOperationDate=" +
                formatter.format(filterAccount.getToOperationDate()) + "&isHistorical=" + filterAccount.getHistorical();

        links.setFirst(urlFirstPage);
        links.setNext(urlBase);
        digitalCheckList.getPagination().setLinks(links);
        return digitalCheckList;
    }

    @Override
    public DtoIntDigitalCheck getDigitalCheck(DtoIntFilterAccount filterAccount) {
        DtoIntDigitalCheck digitalCheck = accountsDaoV0.getDigitalCheckWYRF(filterAccount);
        DtoIntDigitalCheckStatus digitalCheckStatus = new DtoIntDigitalCheckStatus();
        String statusName = null;
        String statusCheck = filterAccount.getDigitalCheckId().split("\\|")[3];
        DigitalCheckStatusEnum statusId = null;

        if ("VG".equals(statusCheck)) {
            statusId = DigitalCheckStatusEnum.VALID;
            statusName = "Valid";
        } else if ("CO".equals(statusCheck)) {
            statusId = DigitalCheckStatusEnum.CASHED;
            statusName = "Cashed";
        } else if ("CN".equals(statusCheck)) {
            statusId = DigitalCheckStatusEnum.CANCELED;
            statusName = "Canceled";
        } else if ("CI".equals(statusCheck)) {
            statusId = DigitalCheckStatusEnum.CANCELED_WITHOUT_FUNDS;
            statusName = "Canceled Without Funds";
        } else if ("CD".equals(statusCheck)) {
            statusId = DigitalCheckStatusEnum.EXPIRED;
            statusName = "Expired";
        }

        digitalCheck.setId(filterAccount.getDigitalCheckId());
        digitalCheck.setOperationNumber(filterAccount.getDigitalCheckId().split("\\|")[1]);

        digitalCheckStatus.setId(statusId);
        digitalCheckStatus.setName(statusName);
        digitalCheck.setDigitalCheckStatus(digitalCheckStatus);

        return digitalCheck;
    }
}
