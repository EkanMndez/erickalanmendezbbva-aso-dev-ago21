package com.bbva.mzic.accounts.business.dto;

import com.bbva.jee.arq.spring.core.catalog.gabi.PageableDto;
import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;

import java.util.ArrayList;
import java.util.List;

public class DtoIntDigitalCheckList implements PageableDto {
    private List<DtoIntDigitalCheck> digitalCheckList;
    private Pagination pagination;

    @Override
    public Pagination getPagination() {
        return pagination;
    }

    @Override
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<DtoIntDigitalCheck> getDigitalCheckList() {
        if (digitalCheckList == null) {
            digitalCheckList = new ArrayList<>();
        }
        return digitalCheckList;
    }

    public void setDigitalCheckList(List<DtoIntDigitalCheck> digitalCheckList) {
        this.digitalCheckList = digitalCheckList;
    }
}
