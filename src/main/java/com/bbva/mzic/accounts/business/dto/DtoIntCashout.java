package com.bbva.mzic.accounts.business.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement(name = "cashout", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlType(name = "cashout", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntCashout implements Serializable {

    @ApiModelProperty(value = "ATM identifier.")
    private String atmId;

    @ApiModelProperty(value = "String based on ISO-8601 for specifying cashout date.")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date date;

    public String getAtmId() {
        return atmId;
    }

    public void setAtmId(String atmId) {
        this.atmId = atmId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
