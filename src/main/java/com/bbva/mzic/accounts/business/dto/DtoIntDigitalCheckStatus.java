package com.bbva.mzic.accounts.business.dto;

import com.bbva.mzic.accounts.rm.enums.DigitalCheckStatusEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "digitalCheckStatus", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlType(name = "digitalCheckStatus", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntDigitalCheckStatus implements Serializable {
    @ApiModelProperty(value = "Digital check status identifier.")
    private DigitalCheckStatusEnum id;

    @ApiModelProperty(value = "Status description of digital check.")
    private String name;

    public DigitalCheckStatusEnum getId() {
        return id;
    }

    public void setId(DigitalCheckStatusEnum id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
