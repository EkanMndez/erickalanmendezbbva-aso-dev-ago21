package com.bbva.mzic.accounts.business;

import com.bbva.mzic.accounts.business.dto.*;

public interface ISrvIntAccountsV0 {
    DtoIntAccount getAccount(DtoIntFilterAccount filterAccount);

    DtoIntAggregatedAvailableBalance getBalance(DtoIntFilterAccount filterAccount);

    DtoIntSavingGoalList listSavingGoals(DtoIntFilterAccount filterAccount);

    DtoIntDigitalCheckList listDigitalCheckMBGDTCHD(DtoIntFilterAccount filterAccount);

    DtoIntDigitalCheck getDigitalCheck(DtoIntFilterAccount filterAccount);
}
