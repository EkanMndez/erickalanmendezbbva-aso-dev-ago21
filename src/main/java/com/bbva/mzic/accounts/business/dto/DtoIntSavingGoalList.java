package com.bbva.mzic.accounts.business.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "listSavingGoals", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlType(name = "listSavingGoals", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntSavingGoalList implements Serializable {
	public List<DtoIntSavingGoal> getListSavingGoals() {
		return listSavingGoals;
	}

	public void setListSavingGoals(List<DtoIntSavingGoal> listSavingGoals) {
		this.listSavingGoals = listSavingGoals;
	}

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "This balance means the maximum expendable amount at the moment of retrieval.")
	private List<DtoIntSavingGoal> listSavingGoals;

}
