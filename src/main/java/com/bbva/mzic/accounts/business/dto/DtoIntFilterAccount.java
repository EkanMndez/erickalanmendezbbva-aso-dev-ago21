package com.bbva.mzic.accounts.business.dto;

import java.io.Serializable;
import java.util.Date;

public class DtoIntFilterAccount implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String accountId;

    private String expands;

    private String clientId;

    private String accountType;

    private String accountNumber;

    private String accountFamilyId;

    private String customerId;

    private Date fromOperationDate;

    private Date toOperationDate;

    private Boolean isHistorical;

    private Integer pageSize;

    private String paginationKey;

    private String digitalCheckId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getExpands() {
        return expands;
    }

    public void setExpands(String expands) {
        this.expands = expands;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountFamilyId() {
        return accountFamilyId;
    }

    public void setAccountFamilyId(String accountFamilyId) {
        this.accountFamilyId = accountFamilyId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Date getFromOperationDate() {
        return fromOperationDate;
    }

    public void setFromOperationDate(Date fromOperationDate) {
        this.fromOperationDate = fromOperationDate;
    }

    public Date getToOperationDate() {
        return toOperationDate;
    }

    public void setToOperationDate(Date toOperationDate) {
        this.toOperationDate = toOperationDate;
    }

    public Boolean getHistorical() {
        return isHistorical;
    }

    public void setHistorical(Boolean historical) {
        isHistorical = historical;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public String getDigitalCheckId() {
        return digitalCheckId;
    }

    public void setDigitalCheckId(String digitalCheckId) {
        this.digitalCheckId = digitalCheckId;
    }
}
