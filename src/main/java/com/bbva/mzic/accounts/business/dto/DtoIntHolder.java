package com.bbva.mzic.accounts.business.dto;

import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "holder", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlType(name = "holder", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntHolder implements Serializable {

    @ApiModelProperty(value = "Full name of the recipient to whom the digital check will be generated.")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
