package com.bbva.mzic.accounts.business.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "savingGoals", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlType(name = "savingGoals", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntSavingGoal implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Identifier of the saving goal.")
	private String id;

	@ApiModelProperty(value = "Defines the status of the saving goal account.")
	private DtoIntStatusSavingGoal status;

	@ApiModelProperty(value = "Defines the status of the saving goal account.")
	private String savingGoalNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DtoIntStatusSavingGoal getStatus() {
		return status;
	}

	public void setStatus(DtoIntStatusSavingGoal status) {
		this.status = status;
	}

	public String getSavingGoalNumber() {
		return savingGoalNumber;
	}

	public void setSavingGoalNumber(String savingGoalNumber) {
		this.savingGoalNumber = savingGoalNumber;
	}

}
