package com.bbva.mzic.accounts.dao.model.bgl5;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>BGL5</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBgl5</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBgl5</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: BGL5-CCT.txt
 * BGL5DETALLE DE CUENTAS VISTA E INVERSIOBG        BG1CBGL5     01 BGML5E              BGL5  SS0265CNNNNN    SSTS A      NNNSNNNN  NN                2017-04-26XM26266 2017-04-2618.19.25XM26266 2017-04-26-08.07.51.895208XM26266 0001-01-010001-01-01
 * FICHERO: BGL5-FDF.txt
 * BGML5E  �ENTRADA FINANCIAL DASHBOARD   �F�05�00044�01�00001�TIPOASU�TIPO DE ASUNTO      �A�002�0�O�        �
 * BGML5E  �ENTRADA FINANCIAL DASHBOARD   �F�05�00044�02�00003�ASUNTO �ASUNTO A CONSULTAR  �A�020�0�O�        �
 * BGML5E  �ENTRADA FINANCIAL DASHBOARD   �F�05�00044�03�00023�SECUENC�SECUENCIA           �A�006�0�O�        �
 * BGML5E  �ENTRADA FINANCIAL DASHBOARD   �F�05�00044�04�00029�CLIENPU�CLIENTE PU          �A�008�0�O�        �
 * BGML5E  �ENTRADA FINANCIAL DASHBOARD   �F�05�00044�05�00037�USUARIO�USUARIO             �A�008�0�O�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�01�00001�TIPASUN�TIPO DE ASUNTO      �A�002�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�02�00003�ASUNTO �ASUNTO              �A�020�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�03�00023�IDCTA  �IDENTIFICADOR CTA   �A�005�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�04�00028�IDCLABE�CUENTA CLABE        �A�005�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�05�00033�CLABE  �CLABE               �A�018�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�06�00051�NUMCEL �NUMERO DE CELULAR   �A�010�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�07�00061�CATEGO �TIPO DE MONEDA      �A�020�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�08�00081�CODSUB �CODIGO SUBPRO       �A�004�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�09�00085�DESPROD�DESCRIPCION PRODUC  �A�030�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�10�00115�DESSUB �DESCRIPCION SUBPRO  �A�030�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�11�00145�CODBANC�ID DEL BANCO        �A�005�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�12�00150�DESCBAN�DESCRIP DEL BANCO   �A�030�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�13�00180�CRGEST �CR GESTOR           �A�004�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�14�00184�DESCR  �DESCRIPCION DE CR   �A�030�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�15�00214�ALIAS  �ALIAS DEL ASUNTO    �A�020�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�16�00234�FECHAP �FECHA APERTURA      �A�010�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�17�00244�ESTATUS�ESTATUS DE CUENTA   �A�001�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�18�00245�ESTATDE�DESCRIPCION ESTATUS �A�012�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�19�00257�DIVISA �DIVISA DE ASUNTO    �A�003�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�20�00260�FCURREN�MENSAJE DE MONEDA   �A�005�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�21�00265�SALDIS �SALDO DISPONIBLE    �S�017�2�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�22�00282�SALPOS �SALDO POSTEADO      �S�017�2�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�23�00299�SALPEND�SALDO PENDIENTE     �S�017�2�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�24�00316�PLAZA  �PLAZA               �A�012�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�25�00328�SALBCO �SALDO BUEN COBRO    �S�017�2�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�26�00345�IMPRET �IMPORTE RETENCION   �S�017�2�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�27�00362�MSGERR �MENSAJE DE ERROR    �A�001�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�28�00363�INDCTAM�IND CTA MANCOM      �A�001�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�29�00364�TDDASOC�TDD ASOCIADA A CTA  �A�019�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�30�00383�SMANCC �SALDO MIN MANEJO CTA�A�012�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�31�00395�COMMANC�COMISION MANEJO CTA �A�007�0�S�        �
 * BGML5YS �DETALLE CUENTAS VISTAS        �X�32�00408�32�00402�CPROMEM�COMISION FIJA       �A�007�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�01�00001�ITIPASU�TIPO DE ASUNTO      �A�002�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�02�00003�IASUNTO�ASUNTO              �A�020�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�03�00023�IIDCTA �IDENTIFICADOR DE CTA�A�005�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�04�00028�ICATEGO�MONEDA              �A�020�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�05�00048�ICODSUB�CODIGO SUBPRO       �A�004�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�06�00052�IDESPRO�DESCRIPCION PRODUCTO�A�030�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�07�00082�IDESSUB�DESCRIPCION SUBPROD �A�030�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�08�00112�ICODBAN�ID DEL BANCO        �A�005�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�09�00117�IDESBAN�DESCRIPCION BANCO   �A�030�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�10�00147�ICRGEST�CR GESTOR           �A�004�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�11�00151�IDESCR �DESCRIPCION GESTOR  �A�030�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�12�00181�IALIAS �ALIAS DE ASUNTO     �A�020�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�13�00201�IESTAT �ESTATUS DE ASUNTO   �A�001�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�14�00202�IESTATD�DESCRIPCION DE ESTAT�A�012�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�15�00214�IFECHAP�FECHA APERTUTA CTO  �A�026�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�16�00240�IFECULT�FECHA ULTIMA RENOVAC�A�026�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�17�00266�IFECAPI�FECHA APERTURA INV  �A�026�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�18�00292�IFEPRLI�FECHA PROXIMA LIQ   �A�026�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�19�00318�ITIPLIQ�TIPO LIQUIDACION    �A�002�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�20�00320�IINTBRU�INTERES BRUTO       �S�017�2�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�21�00337�IINTNET�INTERES NET         �S�017�2�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�22�00354�IDIVISA�DIVISA DE ASUNTO    �A�003�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�23�00357�IFCURRE�TIPO MONEDA         �A�005�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�24�00362�IPLAZO �PLAZO DE INVERSION  �A�005�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�25�00367�ISALDIN�SALDO INICIAL       �S�017�2�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�26�00384�ISALDAC�SALDO ACTUAL        �S�017�2�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�27�00401�INUMCNT�NUMERO CONTRIBUCIONE�N�002�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�28�00403�IMSGERR�MENSAJE DE ERROR    �A�001�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�29�00404�ICTACHQ�CTA CHEQ ASOCIADO   �A�020�0�S�        �
 * BGML5ZS �DETALLE DE INVERSIONES        �X�30�00432�30�00424�ITASA  �TASA DE LA INVERSION�A�009�0�S�        �
 * FICHERO: BGL5-FDX.txt
 * BGL5BGML5ZS BGCSBGL5BG1CBGL51S0423N000                     XM26266 2017-04-26-08.14.07.937523CICSDM112017-05-10-12.32.40.619647
 * BGL5BGML5YS BGCSBGL5BG1CBGL51S0379N000                     XM26266 2017-04-26-08.13.50.760782XM26266 2017-04-28-11.57.50.960657
</pre></code>
 * 
 * @see RespuestaTransaccionBgl5
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BGL5", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBgl5.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBGML5E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBgl5 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
