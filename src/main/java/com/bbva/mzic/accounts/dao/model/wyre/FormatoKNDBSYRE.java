package com.bbva.mzic.accounts.dao.model.wyre;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNDBSYRE</code> de la transacci&oacute;n <code>WYRE</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBSYRE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBSYRE {

    /**
     * <p>
     * Campo <code>ALIAS</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "ALIAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
    private String alias;

    /**
     * <p>
     * Campo <code>CONCEPT</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CONCEPT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 55, longitudMaxima = 55)
    private String concept;

    /**
     * <p>
     * Campo <code>ESTADO</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "ESTADO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String estado;

    /**
     * <p>
     * Campo <code>IMPOPER</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 4, nombre = "IMPOPER", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal impoper;

    /**
     * <p>
     * Campo <code>FOLIOCA</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "FOLIOCA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String folioca;

    /**
     * <p>
     * Campo <code>FECHALT</code>, &iacute;ndice: <code>6</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 6, nombre = "FECHALT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fechalt;

    /**
     * <p>
     * Campo <code>NUMCEL</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "NUMCEL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String numcel;

    /**
     * <p>
     * Campo <code>CVEOTP</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "CVEOTP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String cveotp;

    /**
     * <p>
     * Campo <code>FOLIOCH</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "FOLIOCH", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
    private String folioch;

    /**
     * <p>
     * Campo <code>TIPCHEQ</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "TIPCHEQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String tipcheq;

}
