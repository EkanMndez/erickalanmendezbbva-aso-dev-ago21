package com.bbva.mzic.accounts.dao.model.mtkdt033_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>movementReferences</code>, utilizado por la clase
 * <code>Deposit</code>
 * </p>
 * 
 * @see Deposit
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Movementreferences {

    /**
     * <p>
     * Campo <code>firstDescription</code>, &iacute;ndice: <code>1</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "firstDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 300, signo = true)
    private String firstdescription;

    /**
     * <p>
     * Campo <code>secondDescription</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "secondDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 300, signo = true)
    private String seconddescription;

    /**
     * <p>
     * Campo <code>thirdDescription</code>, &iacute;ndice: <code>3</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "thirdDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 300, signo = true)
    private String thirddescription;

    /**
     * <p>
     * Campo <code>fourthDescription</code>, &iacute;ndice: <code>4</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "fourthDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 300, signo = true)
    private String fourthdescription;

}
