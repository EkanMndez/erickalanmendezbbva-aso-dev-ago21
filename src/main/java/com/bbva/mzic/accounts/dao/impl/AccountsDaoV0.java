package com.bbva.mzic.accounts.dao.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.bbva.mzic.accounts.business.dto.*;
import com.bbva.mzic.accounts.dao.IAccountsDaoV0;
import com.bbva.mzic.accounts.dao.model.wyrf.PeticionTransaccionWyrf;
import com.bbva.mzic.accounts.rm.enums.DigitalCheckStatusEnum;
import com.bbva.mzic.accounts.rm.enums.DigitalCheckTypeEnum;
import com.bbva.mzic.accounts.rm.enums.ReceiverTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Repository
public class AccountsDaoV0 implements IAccountsDaoV0 {

    @Autowired
    ServicioTransacciones servicioTransacciones;

    @Override
    public DtoIntAggregatedAvailableBalance getBalanceBGL4(DtoIntFilterAccount filterAccount) {
        final List<String> familyIds = Arrays.asList("CREDIT", "CREDIT-EXTENSION", "SAVINGS");
        final DtoIntAggregatedAvailableBalance aggregatedAvailableBalance = new DtoIntAggregatedAvailableBalance();
        final DtoIntBalance balance = new DtoIntBalance();
        aggregatedAvailableBalance.setCurrentBalances(new ArrayList<>());
        aggregatedAvailableBalance.setPostedBalances(new ArrayList<>());
        aggregatedAvailableBalance.setPendingBalances(new ArrayList<>());
        if (!familyIds.contains(filterAccount.getAccountFamilyId())) {
            balance.setAmount(new BigDecimal("123456.00"));
            balance.setCurrency("MXN");
            aggregatedAvailableBalance.getCurrentBalances().add(balance);
            aggregatedAvailableBalance.getPostedBalances().add(balance);
            aggregatedAvailableBalance.getPendingBalances().add(balance);

        }
        return aggregatedAvailableBalance;

        // return servicioTransacciones.invoke(PeticionTransaccionBgl4.class,
        // filterAccount);
    }

    @Override
    public DtoIntDigitalCheckList listDigitalCheckMBGDTCHD(DtoIntFilterAccount filterAccount) {
        DtoIntDigitalCheckList digitalCheckList = new DtoIntDigitalCheckList();
        DtoIntDigitalCheck intDigitalCheck = new DtoIntDigitalCheck();
        DtoIntReceiver receiver = new DtoIntReceiver();
        DtoIntHolder holder = new DtoIntHolder();
        Money sentMoney = new Money();
        DtoIntDigitalCheckStatus digitalCheckStatus = new DtoIntDigitalCheckStatus();
        DtoIntDigitalCheckType digitalCheckType = new DtoIntDigitalCheckType();
        DtoIntCashout cashout = new DtoIntCashout();
        Pagination pagination = new Pagination();
        Pagination.Links links = new Pagination.Links();


        cashout.setDate(new Date());
        cashout.setAtmId("11412341");

        digitalCheckType.setId(DigitalCheckTypeEnum.RESERVED_AMOUNT_CHECK);
        digitalCheckType.setName("Reserved Amount Check");

        digitalCheckStatus.setId(DigitalCheckStatusEnum.CASHED);
        digitalCheckStatus.setName("Cashed");

        sentMoney.setAmount(new BigDecimal("12141.23"));
        sentMoney.setCurrency("USD");

        holder.setName("Pedro Hernandez Garcia");

        receiver.setHolder(holder);
        receiver.setType(ReceiverTypeEnum.PHONE);
        receiver.setValue("5512345678");

        intDigitalCheck.setDigitalCheckType(digitalCheckType);
        intDigitalCheck.setDigitalCheckStatus(digitalCheckStatus);
        intDigitalCheck.setReceiver(receiver);
        intDigitalCheck.setDueDate(new Date());
        intDigitalCheck.setOperationNumber("1789441");
        intDigitalCheck.setNumber(147892L);
        intDigitalCheck.setOperationDate(new Date());
        intDigitalCheck.setConcept("cheque ficticio 1");
        intDigitalCheck.setId("1324890412");
        intDigitalCheck.setSentMoney(sentMoney);
        intDigitalCheck.setCashout(cashout);

        digitalCheckList.getDigitalCheckList().add(intDigitalCheck);

        links.setFirst("1");
        links.setNext("2");
        links.setPrevious("1");
        pagination.setPage(21L);
        pagination.setLinks(links);
        pagination.setPageSize(20L);

        digitalCheckList.setPagination(pagination);

        return digitalCheckList;
        // return servicioTransacciones.invoke(PeticionTransaccionMBGDTCHD_1.class,
        // filterAccount);
    }

    @Override
    public DtoIntDigitalCheck getDigitalCheckWYRF(DtoIntFilterAccount filterAccount) {
        return servicioTransacciones.invoke(PeticionTransaccionWyrf.class, filterAccount);
    }


}
