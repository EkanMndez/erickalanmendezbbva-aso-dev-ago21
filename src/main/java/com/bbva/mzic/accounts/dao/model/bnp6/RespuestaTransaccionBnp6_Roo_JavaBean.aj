package com.bbva.mzic.accounts.dao.model.bnp6;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import java.lang.String;

privileged aspect RespuestaTransaccionBnp6_Roo_JavaBean {
    
    public String RespuestaTransaccionBnp6.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    public void RespuestaTransaccionBnp6.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }
    
    public String RespuestaTransaccionBnp6.getCodigoControl() {
        return this.codigoControl;
    }
    
    public void RespuestaTransaccionBnp6.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }
    
    public void RespuestaTransaccionBnp6.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
    }
    
}
