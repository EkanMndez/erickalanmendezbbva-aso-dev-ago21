// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.wyrd;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import java.lang.String;

privileged aspect RespuestaTransaccionWyrd_Roo_JavaBean {
    
    public String RespuestaTransaccionWyrd.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    public void RespuestaTransaccionWyrd.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }
    
    public String RespuestaTransaccionWyrd.getCodigoControl() {
        return this.codigoControl;
    }
    
    public void RespuestaTransaccionWyrd.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }
    
    public void RespuestaTransaccionWyrd.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
    }
    
}
