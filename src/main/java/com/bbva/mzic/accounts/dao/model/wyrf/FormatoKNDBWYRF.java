package com.bbva.mzic.accounts.dao.model.wyrf;


import java.util.Date;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>KNDBWYRF</code> de la transacci&oacute;n <code>WYRF</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBWYRF")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBWYRF implements IFormat {

    /**
     * <p>
     * Campo <code>CLIEPU</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CLIEPU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliepu;

    /**
     * <p>
     * Campo <code>FOLIOCA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "FOLIOCA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String folioca;

    /**
     * <p>
     * Campo <code>FECHALT</code>, &iacute;ndice: <code>3</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 3, nombre = "FECHALT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fechalt;

}
