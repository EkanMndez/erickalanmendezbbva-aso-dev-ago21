package com.bbva.mzic.accounts.dao.model.bg6b;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BG6BFS2</code> de la transacci&oacute;n <code>BG6B</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BG6BFS2")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBG6BFS2 {

    /**
     * <p>
     * Campo <code>ITIPASU</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "ITIPASU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String itipasu;

    /**
     * <p>
     * Campo <code>IASUNTO</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "IASUNTO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String iasunto;

    /**
     * <p>
     * Campo <code>ISECUEN</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "ISECUEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 13, longitudMaxima = 13)
    private String isecuen;

    /**
     * <p>
     * Campo <code>IIDCTA</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "IIDCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String iidcta;

    /**
     * <p>
     * Campo <code>ICATEGO</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "ICATEGO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String icatego;

    /**
     * <p>
     * Campo <code>ICODSUB</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "ICODSUB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String icodsub;

    /**
     * <p>
     * Campo <code>IDESPRO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "IDESPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String idespro;

    /**
     * <p>
     * Campo <code>IDESSUB</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "IDESSUB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String idessub;

    /**
     * <p>
     * Campo <code>IALIAS</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "IALIAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String ialias;

    /**
     * <p>
     * Campo <code>IFECHAP</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "IFECHAP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
    private String ifechap;

    /**
     * <p>
     * Campo <code>IFECULT</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "IFECULT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
    private String ifecult;

    /**
     * <p>
     * Campo <code>ISALDIN</code>, &iacute;ndice: <code>12</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 12, nombre = "ISALDIN", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal isaldin;

    /**
     * <p>
     * Campo <code>IDIVISA</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "IDIVISA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String idivisa;

    /**
     * <p>
     * Campo <code>ISALACT</code>, &iacute;ndice: <code>14</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 14, nombre = "ISALACT", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal isalact;

    /**
     * <p>
     * Campo <code>IPLAZO</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "IPLAZO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String iplazo;

    /**
     * <p>
     * Campo <code>IESTAT</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "IESTAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String iestat;

    /**
     * <p>
     * Campo <code>IESTATD</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "IESTATD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
    private String iestatd;

    /**
     * <p>
     * Campo <code>IMSGERR</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "IMSGERR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String imsgerr;

    /**
     * <p>
     * Campo <code>IFCURRE</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "IFCURRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String ifcurre;

    /**
     * <p>
     * Campo <code>INUMCNT</code>, &iacute;ndice: <code>20</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 20, nombre = "INUMCNT", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
    private Integer inumcnt;

    /**
     * <p>
     * Campo <code>TOTMOVN</code>, &iacute;ndice: <code>21</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 21, nombre = "TOTMOVN", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer totmovn;

    /**
     * <p>
     * Campo <code>TOTMOVP</code>, &iacute;ndice: <code>22</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 22, nombre = "TOTMOVP", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer totmovp;

    /**
     * <p>
     * Campo <code>IMHABER</code>, &iacute;ndice: <code>23</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 23, nombre = "IMHABER", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal imhaber;

    /**
     * <p>
     * Campo <code>IMPDEBE</code>, &iacute;ndice: <code>24</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 24, nombre = "IMPDEBE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal impdebe;

}
