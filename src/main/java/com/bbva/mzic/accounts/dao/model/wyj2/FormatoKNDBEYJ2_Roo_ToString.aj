// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.wyj2;

import java.lang.String;

privileged aspect FormatoKNDBEYJ2_Roo_ToString {
    
    public String FormatoKNDBEYJ2.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Idruc: ").append(getIdruc()).append(", ");
        sb.append("Idtipac: ").append(getIdtipac()).append(", ");
        sb.append("Motcanc: ").append(getMotcanc()).append(", ");
        sb.append("Numclie: ").append(getNumclie()).append(", ");
        sb.append("Numcta: ").append(getNumcta()).append(", ");
        sb.append("Tipoace: ").append(getTipoace()).append(", ");
        sb.append("Tipocta: ").append(getTipocta());
        return sb.toString();
    }
    
}
