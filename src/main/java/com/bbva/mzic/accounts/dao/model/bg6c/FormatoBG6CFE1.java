package com.bbva.mzic.accounts.dao.model.bg6c;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>BG6CFE1</code> de la transacci&oacute;n <code>BG6C</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BG6CFE1")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBG6CFE1 {

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String cuenta;

    /**
     * <p>
     * Campo <code>IDIVISA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "IDIVISA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String idivisa;

    /**
     * <p>
     * Campo <code>TICUENT</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "TICUENT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String ticuent;

}
