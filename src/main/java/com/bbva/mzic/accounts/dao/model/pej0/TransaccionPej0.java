package com.bbva.mzic.accounts.dao.model.pej0;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>PEJ0</code>
 * 
 * @see PeticionTransaccionPej0
 * @see RespuestaTransaccionPej0
 */
@Component("transaccion-pej0")
public class TransaccionPej0 implements InvocadorTransaccion<PeticionTransaccionPej0, RespuestaTransaccionPej0> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionPej0 invocar(PeticionTransaccionPej0 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPej0.class, RespuestaTransaccionPej0.class, transaccion);
	}

	@Override
	public RespuestaTransaccionPej0 invocarCache(PeticionTransaccionPej0 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPej0.class, RespuestaTransaccionPej0.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionPej0.class, "vaciearCache");
	}
}
