package com.bbva.mzic.accounts.dao.model.mtkdt053_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>references</code>, utilizado por la clase <code>RespuestaTransaccionMtkdt053_1</code></p>
 * 
 * @see RespuestaTransaccionMtkdt053_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class References {
	
	/**
	 * <p>Campo <code>referencesId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "referencesId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 18, signo = true)
	private String referencesid;
	
	/**
	 * <p>Campo <code>referencesName</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "referencesName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
	private String referencesname;
	
	/**
	 * <p>Campo <code>operativeDescription</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "operativeDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
	private String operativedescription;
	
}
