package com.bbva.mzic.accounts.dao.model.mchqt002_1;

import java.math.BigDecimal;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Bean de respuesta para la transacci&oacute;n <code>MCHQT002</code>
 *
 * @see PeticionTransaccionMchqt002_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMchqt002_1 {

	/**
	 * <p>
	 * Cabecera <code>COD-AVISO</code>
	 * </p>
	 */
	@Cabecera(nombre = NombreCabecera.CODIGO_AVISO)
	private String codigoAviso;

	/**
	 * <p>
	 * Cabecera <code>DES-AVISO</code>
	 * </p>
	 */
	@Cabecera(nombre = NombreCabecera.DESCRIPCION_AVISO)
	private String descripcionAviso;

	/**
	 * <p>
	 * Cabecera <code>COD-UUAA-AVISO</code>
	 * </p>
	 */
	@Cabecera(nombre = NombreCabecera.APLICACION_AVISO)
	private String aplicacionAviso;

	/**
	 * <p>
	 * Cabecera <code>COD-RETORNO</code>
	 * </p>
	 */
	@Cabecera(nombre = NombreCabecera.CODIGO_RETORNO)
	private String codigoRetorno;

	/**
	 * <p>
	 * Campo <code>incomeId</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "incomeId", tipo = TipoCampo.ENTERO, longitudMaxima = 18, signo = true, obligatorio = true)
	private long incomeid;

	/**
	 * <p>
	 * Campo <code>operationId</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "operationId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 9, signo = true, obligatorio = true)
	private String operationid;

	/**
	 * <p>
	 * Campo <code>channelName</code>, &iacute;ndice: <code>3</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "channelName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String channelname;

	/**
	 * <p>
	 * Campo <code>accountId</code>, &iacute;ndice: <code>4</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true)
	private String accountid;

	/**
	 * <p>
	 * Campo <code>receiverName</code>, &iacute;ndice: <code>5</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "receiverName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 255, signo = true, obligatorio = true)
	private String receivername;

	/**
	 * <p>
	 * Campo <code>senderContractNumber</code>, &iacute;ndice: <code>6</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "senderContractNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String sendercontractnumber;

	/**
	 * <p>
	 * Campo <code>incomeAmount</code>, &iacute;ndice: <code>7</code>, tipo:
	 * <code>DECIMAL</code>
	 */
	@Campo(indice = 7, nombre = "incomeAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 18, signo = true, obligatorio = true)
	private BigDecimal incomeamount;

	/**
	 * <p>
	 * Campo <code>checkNumber</code>, &iacute;ndice: <code>8</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "checkNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
	private String checknumber;

	/**
	 * <p>
	 * Campo <code>imageId</code>, &iacute;ndice: <code>9</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "imageId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
	private String imageid;

	/**
	 * <p>
	 * Campo <code>checkbookSequentialNumber</code>, &iacute;ndice: <code>10</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "checkbookSequentialNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String checkbooksequentialnumber;

}
