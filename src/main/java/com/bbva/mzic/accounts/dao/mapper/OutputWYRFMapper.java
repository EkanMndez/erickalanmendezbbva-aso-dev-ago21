package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.bbva.mzic.accounts.business.dto.*;
import com.bbva.mzic.accounts.dao.model.wyrf.FormatoKNDBSYRF;
import com.bbva.mzic.accounts.dao.model.wyrf.RespuestaTransaccionWyrf;
import com.bbva.mzic.accounts.rm.enums.DigitalCheckTypeEnum;
import com.bbva.mzic.accounts.rm.enums.ReceiverTypeEnum;
import com.bbva.mzic.serviceutils.rm.utils.tx.FilterResponseHost;

import java.util.List;
import java.util.stream.Collectors;

public class OutputWYRFMapper implements IMapper<RespuestaTransaccionWyrf, DtoIntDigitalCheck> {
    @Override
    public DtoIntDigitalCheck map(RespuestaTransaccionWyrf responseTransaccionWyrf) {
        final List<Object> formats = responseTransaccionWyrf.getCuerpo().getPartes().stream()
                .filter(cs -> cs != null && cs.getClass().equals(CopySalida.class)).map(cs -> (CopySalida) cs).map(CopySalida::getCopy)
                .collect(Collectors.toList());
        final FormatoKNDBSYRF formatoKNDBSYRF = FilterResponseHost.findFirstFormat(formats, FormatoKNDBSYRF.class);

        DtoIntDigitalCheck digitalCheck = new DtoIntDigitalCheck();
        DtoIntReceiver receiver = new DtoIntReceiver();
        DtoIntHolder holder = new DtoIntHolder();
        Money sentMoney = new Money();
        DtoIntDigitalCheckType digitalCheckType = new DtoIntDigitalCheckType();
        DtoIntDigitalCheckStatus digitalCheckStatus = new DtoIntDigitalCheckStatus();

        digitalCheck.setNumber(Long.parseLong(formatoKNDBSYRF.getFolioch()));
        digitalCheck.setConcept(formatoKNDBSYRF.getConcept());
        digitalCheck.setOperationDate(formatoKNDBSYRF.getFecalta());
        digitalCheck.setCodeCheck(formatoKNDBSYRF.getCveotp());

        holder.setName(formatoKNDBSYRF.getAlias());
        receiver.setHolder(holder);
        receiver.setType(ReceiverTypeEnum.PHONE);
        receiver.setValue(formatoKNDBSYRF.getNumcel());
        digitalCheck.setReceiver(receiver);

        sentMoney.setCurrency("MXN");
        sentMoney.setAmount(formatoKNDBSYRF.getImpope());
        digitalCheck.setSentMoney(sentMoney);

        digitalCheckType.setId(getDigitalCheckTypeId(formatoKNDBSYRF.getTipcheq()));
        digitalCheckType.setName(getDigitalCheckTypeName(formatoKNDBSYRF.getTipcheq()));
        digitalCheck.setDigitalCheckType(digitalCheckType);

        return digitalCheck;
    }

    private DigitalCheckTypeEnum getDigitalCheckTypeId(String id) {
        DigitalCheckTypeEnum typeEnum = null;

        if ("01".equals(id)) {
            typeEnum = DigitalCheckTypeEnum.UNRESERVED_AMOUNT_CHECK;
        } else if ("02".equals(id)) {
            typeEnum = DigitalCheckTypeEnum.RESERVED_AMOUNT_CHECK;
        }
        return typeEnum;
    }

    private String getDigitalCheckTypeName(String id) {
        String typeName = "";

        if ("01".equals(id)) {
            typeName = "Unreserved Amount Check";
        } else if ("02".equals(id)) {
            typeName = "Reserved Amount Check";
        }
        return typeName;
    }

}
