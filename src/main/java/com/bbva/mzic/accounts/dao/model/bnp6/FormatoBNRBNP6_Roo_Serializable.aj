package com.bbva.mzic.accounts.dao.model.bnp6;

import java.io.Serializable;

privileged aspect FormatoBNRBNP6_Roo_Serializable {
    
    declare parents: FormatoBNRBNP6 implements Serializable;
    
    private static final long FormatoBNRBNP6.serialVersionUID = 1L;
    
}
