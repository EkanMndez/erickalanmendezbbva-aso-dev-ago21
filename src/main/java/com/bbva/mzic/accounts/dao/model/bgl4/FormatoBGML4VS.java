package com.bbva.mzic.accounts.dao.model.bgl4;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGML4VS</code> de la transacci&oacute;n <code>BGL4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGML4VS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGML4VS implements IFormat {

    /**
     * <p>
     * Campo <code>TOPVIMX</code>, &iacute;ndice: <code>1</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 1, nombre = "TOPVIMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal topvimx;

    /**
     * <p>
     * Campo <code>TOPVIUS</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "TOPVIUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal topvius;

    /**
     * <p>
     * Campo <code>TOPVIEU</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 3, nombre = "TOPVIEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal topvieu;

    /**
     * <p>
     * Campo <code>TOPINMX</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 4, nombre = "TOPINMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal topinmx;

    /**
     * <p>
     * Campo <code>TOPINUS</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 5, nombre = "TOPINUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal topinus;

    /**
     * <p>
     * Campo <code>TOPINEU</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 6, nombre = "TOPINEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal topineu;

    /**
     * <p>
     * Campo <code>TOPPKMX</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 7, nombre = "TOPPKMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal toppkmx;

    /**
     * <p>
     * Campo <code>TOPPKUS</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 8, nombre = "TOPPKUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal toppkus;

    /**
     * <p>
     * Campo <code>TOPPKEU</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 9, nombre = "TOPPKEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal toppkeu;

}
