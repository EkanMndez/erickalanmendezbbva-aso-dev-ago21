package com.bbva.mzic.accounts.dao.model.mtkdt035_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MTKDT035</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt035_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt035_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MTKDT035-01-MX.xml
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MTKDT035&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;ticketId&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;applicant&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;message&quot; type=&quot;String&quot; size=&quot;90&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;
 * Transacción utilizada para actualizar el motivo y estatus del producto contratado.
 * &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt035_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MTKDT035", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMtkdt035_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt035_1 {

    /**
     * <p>
     * Campo <code>accountId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
    private String accountid;

    /**
     * <p>
     * Campo <code>ticketId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "ticketId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true, obligatorio = true)
    private String ticketid;

    /**
     * <p>
     * Campo <code>reason</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "reason", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
    private String reason;

    /**
     * <p>
     * Campo <code>applicant</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "applicant", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 80, signo = true, obligatorio = true)
    private String applicant;

}
