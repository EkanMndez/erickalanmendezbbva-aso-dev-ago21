package com.bbva.mzic.accounts.dao.model.mchqt002_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCHQT002</code>
 *
 * @see PeticionTransaccionMchqt002_1
 * @see RespuestaTransaccionMchqt002_1
 */
@Component
public class TransaccionMchqt002_1 implements InvocadorTransaccion<PeticionTransaccionMchqt002_1, RespuestaTransaccionMchqt002_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMchqt002_1 invocar(final PeticionTransaccionMchqt002_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchqt002_1.class, RespuestaTransaccionMchqt002_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMchqt002_1 invocarCache(final PeticionTransaccionMchqt002_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchqt002_1.class, RespuestaTransaccionMchqt002_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMchqt002_1.class, "vaciearCache");
	}
}
