package com.bbva.mzic.accounts.dao.model.mtkdt058_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT058</code>
 *
 * @see PeticionTransaccionMtkdt058_1
 * @see RespuestaTransaccionMtkdt058_1
 */
@Component
public class TransaccionMtkdt058_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt058_1, RespuestaTransaccionMtkdt058_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt058_1 invocar(final PeticionTransaccionMtkdt058_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt058_1.class, RespuestaTransaccionMtkdt058_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt058_1 invocarCache(final PeticionTransaccionMtkdt058_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt058_1.class, RespuestaTransaccionMtkdt058_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt058_1.class, "vaciearCache");
	}
}
