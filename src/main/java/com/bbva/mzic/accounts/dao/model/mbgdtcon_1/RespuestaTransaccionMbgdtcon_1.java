package com.bbva.mzic.accounts.dao.model.mbgdtcon_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Bean de respuesta para la transacci&oacute;n <code>MBGDTCON</code>
 *
 * @see PeticionTransaccionMbgdtcon_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMbgdtcon_1 {

	/**
	 * <p>
	 * Cabecera <code>COD-AVISO</code>
	 * </p>
	 */
	@Cabecera(nombre = NombreCabecera.CODIGO_AVISO)
	private String codigoAviso;

	/**
	 * <p>
	 * Cabecera <code>DES-AVISO</code>
	 * </p>
	 */
	@Cabecera(nombre = NombreCabecera.DESCRIPCION_AVISO)
	private String descripcionAviso;

	/**
	 * <p>
	 * Cabecera <code>COD-UUAA-AVISO</code>
	 * </p>
	 */
	@Cabecera(nombre = NombreCabecera.APLICACION_AVISO)
	private String aplicacionAviso;

	/**
	 * <p>
	 * Cabecera <code>COD-RETORNO</code>
	 * </p>
	 */
	@Cabecera(nombre = NombreCabecera.CODIGO_RETORNO)
	private String codigoRetorno;

	/**
	 * <p>
	 * Campo <code>paginationOutDTO</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>TABULAR</code>
	 */
	@Campo(indice = 1,
			nombre = "paginationOutDTO",
			tipo = TipoCampo.TABULAR)
	private List<Paginationoutdto> paginationoutdto;

	/**
	 * <p>
	 * Campo <code>accountReconciliationRegistersList</code>, &iacute;ndice:
	 * <code>2</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 2,
			nombre = "accountReconciliationRegistersList",
			tipo = TipoCampo.TABULAR)
	private List<Accountreconciliationregisterslist> accountreconciliationregisterslist;

}
