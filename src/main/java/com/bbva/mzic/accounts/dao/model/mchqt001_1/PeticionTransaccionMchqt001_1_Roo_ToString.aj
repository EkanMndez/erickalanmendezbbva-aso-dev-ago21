package com.bbva.mzic.accounts.dao.model.mchqt001_1;

import java.lang.String;

privileged aspect PeticionTransaccionMchqt001_1_Roo_ToString {
    
    public String PeticionTransaccionMchqt001_1.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Accountid: ").append(getAccountid()).append(", ");
        sb.append("Authorizationnumber: ").append(getAuthorizationnumber()).append(", ");
        sb.append("Branchid: ").append(getBranchid()).append(", ");
        sb.append("Channelname: ").append(getChannelname()).append(", ");
        sb.append("Chargeretention: ").append(getChargeretention()).append(", ");
        sb.append("Checkbooksequentialnumber: ").append(getCheckbooksequentialnumber()).append(", ");
        sb.append("Checknumber: ").append(getChecknumber()).append(", ");
        sb.append("Checktype: ").append(getChecktype()).append(", ");
        sb.append("Creditmovement: ").append(getCreditmovement()).append(", ");
        sb.append("Creditretention: ").append(getCreditretention()).append(", ");
        sb.append("Imageid: ").append(getImageid()).append(", ");
        sb.append("Imagename: ").append(getImagename()).append(", ");
        sb.append("Incomeamount: ").append(getIncomeamount()).append(", ");
        sb.append("Operationid: ").append(getOperationid()).append(", ");
        sb.append("Receivername: ").append(getReceivername()).append(", ");
        sb.append("Referenceid: ").append(getReferenceid()).append(", ");
        sb.append("Sendercontractnumber: ").append(getSendercontractnumber()).append(", ");
        sb.append("Terminalcode: ").append(getTerminalcode()).append(", ");
        sb.append("Userid: ").append(getUserid());
        return sb.toString();
    }
    
}
