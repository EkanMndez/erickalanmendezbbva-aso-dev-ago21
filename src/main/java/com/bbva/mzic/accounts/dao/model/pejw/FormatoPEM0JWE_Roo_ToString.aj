package com.bbva.mzic.accounts.dao.model.pejw;

import java.lang.String;

privileged aspect FormatoPEM0JWE_Roo_ToString {
    
    public String FormatoPEM0JWE.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cuenta: ").append(getCuenta()).append(", ");
        sb.append("Numclie: ").append(getNumclie());
        return sb.toString();
    }
    
}
