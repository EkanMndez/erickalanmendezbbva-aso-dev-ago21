package com.bbva.mzic.accounts.dao.model.mtkdt058_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>reference</code>, utilizado por la clase
 * <code>RespuestaTransaccionMtkdt058_1</code>
 * </p>
 *
 * @see RespuestaTransaccionMtkdt058_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Reference {

    /**
     * <p>
     * Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 45, signo = true, obligatorio = true)
    private String id;

    /**
     * <p>
     * Campo <code>name</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "name", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 45, signo = true, obligatorio = true)
    private String name;

    /**
     * <p>
     * Campo <code>value</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "value", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 45, signo = true, obligatorio = true)
    private String value;

}
