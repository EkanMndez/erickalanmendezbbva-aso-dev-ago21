package com.bbva.mzic.accounts.dao.model.b458;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>B458</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionB458</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionB458</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: QGDTCCT.B458.TXT
 * B458TALONARIOS DE UNA CUENTA           BG        BG2C5600     01 BGM458              B458  NN0074CNNNNN    SSTN    C   NNNSNNNN  NN                2018-08-23CICSTM112018-08-3015.49.55CICSDM112018-08-23-13.10.59.770163CICSTM110001-01-010001-01-01
 * FICHERO: QGDTFDF.B458.TXT
 * BGM458  �CONSULTA DE TALONARIOS N.CUENT�F�05�00047�01�00001�CUENTA �CUENTA CLIENTE      �A�020�0�R�        �
 * BGM458  �CONSULTA DE TALONARIOS N.CUENT�F�05�00047�02�00021�FECDESD�FECHA DESDE         �A�008�0�O�        �
 * BGM458  �CONSULTA DE TALONARIOS N.CUENT�F�05�00047�03�00029�FECHAST�FECHA FIN           �A�008�0�O�        �
 * BGM458  �CONSULTA DE TALONARIOS N.CUENT�F�05�00047�04�00037�OPCION �OPCION DE TALONARIO �A�001�0�R�        �
 * BGM458  �CONSULTA DE TALONARIOS N.CUENT�F�05�00047�05�00038�SIGPAG �OPCION DE PAGINA    �A�010�0�O�        �
 * BGS458  �CONSULTA TALONARIOS N.CUENT   �X�08�00067�01�00001�PRICHEQ�CHEQUE INICIO       �N�009�0�S�        �
 * BGS458  �CONSULTA TALONARIOS N.CUENT   �X�08�00067�02�00010�ULTCHEQ�ULTIMO CHEQUE       �N�009�0�S�        �
 * BGS458  �CONSULTA TALONARIOS N.CUENT   �X�08�00067�03�00019�TOTAL  �NUMERO TOTAL DE CHEQ�N�006�0�S�        �
 * BGS458  �CONSULTA TALONARIOS N.CUENT   �X�08�00067�04�00025�DISP   �NUMERO CHEQUES DISP �N�006�0�S�        �
 * BGS458  �CONSULTA TALONARIOS N.CUENT   �X�08�00067�05�00031�FECALTA�FECHA DE ALTA       �A�010�0�S�        �
 * BGS458  �CONSULTA TALONARIOS N.CUENT   �X�08�00067�06�00041�FECENT �FECHA DE ENTREGA    �A�010�0�S�        �
 * BGS458  �CONSULTA TALONARIOS N.CUENT   �X�08�00067�07�00051�ESTATUS�ESTATUS DE CHEQUERA �A�016�0�S�        �
 * BGS458  �CONSULTA TALONARIOS N.CUENT   �X�08�00067�08�00067�MASDAT �MAS DATOS           �A�001�0�S�        �
 * FICHERO: QGDTFDX.B458.TXT
 * B458BGS458  BGWCS458BG2C56001S                             CICSTM112018-08-23-19.56.59.409752CICSTM112018-08-23-19.56.59.409782
</pre></code>
 * 
 * @see RespuestaTransaccionB458
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "B458", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionB458.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBGM458.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionB458 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
