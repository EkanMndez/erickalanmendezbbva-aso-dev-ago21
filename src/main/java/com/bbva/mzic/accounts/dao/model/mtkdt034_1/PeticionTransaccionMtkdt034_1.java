package com.bbva.mzic.accounts.dao.model.mtkdt034_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>MTKDT034</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt034_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt034_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT034-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MTKDT034&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountId&quot; type=&quot;Long&quot; size=&quot;6&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;ticketId&quot; type=&quot;Long&quot; size=&quot;13&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ticketId&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;functionaryId&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;functionaryName&quot; type=&quot;String&quot; size=&quot;90&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;bankId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;bankName&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;branchName&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;group name=&quot;deposit&quot; order=&quot;10&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;mainReference&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;14&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;creationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;applicationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;typeId&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;documentId&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;group name=&quot;movementReferences&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;firstDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;secondDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;thirdDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;forthDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion empleada para obtener el detalle de un deposito realizado sobre una cuenta CED.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMtkdt034_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
		nombre = "MTKDT034",
		tipo = 1,
		subtipo = 1,
		version = 1,
		configuracion = "default_apx",
		respuesta = RespuestaTransaccionMtkdt034_1.class,
		atributos = {@Atributo(nombre = "country", valor = "MX")}
		)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt034_1 {

    /**
     * <p>
     * Campo <code>accountId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true)
    private String accountid;

	/**
	 * <p>Campo <code>ticketId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "ticketId", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true)
	private Long ticketid;

}
