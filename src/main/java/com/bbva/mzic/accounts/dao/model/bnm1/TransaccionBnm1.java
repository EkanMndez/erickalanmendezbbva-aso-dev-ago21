package com.bbva.mzic.accounts.dao.model.bnm1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BNM1</code>
 * 
 * @see PeticionTransaccionBnm1
 * @see RespuestaTransaccionBnm1
 */
@Component
public class TransaccionBnm1 implements InvocadorTransaccion<PeticionTransaccionBnm1, RespuestaTransaccionBnm1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBnm1 invocar(PeticionTransaccionBnm1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBnm1.class, RespuestaTransaccionBnm1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBnm1 invocarCache(PeticionTransaccionBnm1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBnm1.class, RespuestaTransaccionBnm1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBnm1.class, "vaciearCache");
	}
}
