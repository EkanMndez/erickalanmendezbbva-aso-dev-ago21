package com.bbva.mzic.accounts.dao.model.wyrf;

import com.bbva.jee.arq.spring.core.host.*;
import com.bbva.mzic.accounts.dao.mapper.InputWYRFMapper;
import com.bbva.mzic.accounts.dao.mapper.OutputWYRFMapper;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>WYRF</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWyrf</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionWyrf</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: CCTWYRF.txt
 * WYRFCONSULTA DETALLE CHEQUE DIGITAL    KN        KN1CWYRF     01 KNDBWYRF            WYRF  SS0028CNNNNN    SSTS A      NNNSNNNN  NN                2017-11-07CICSDM112017-11-0713.17.36CICSDM112017-11-07-12.54.15.171189CICSDM110001-01-010001-01-01
 * FICHERO: FDFWYRF1_KNDBWYRF.txt
 * KNDBWYRF�CONSULTA DETALLE CHEQUE DIGITA�F�03�00028�01�00001�CLIEPU �CLIENTE             �A�008�0�R�        �
 * KNDBWYRF�CONSULTA DETALLE CHEQUE DIGITA�F�03�00028�02�00009�FOLIOCA�FOLIO CANAL         �A�010�0�R�        �
 * KNDBWYRF�CONSULTA DETALLE CHEQUE DIGITA�F�03�00028�03�00019�FECHALT�FECHA ALTA          �A�010�0�R�        �
 * FICHERO: FDFWYRF2_KNDBSYRF.txt
 * KNDBSYRF�CONSULTA DETALLE CHEQUE DIGITA�X�08�00170�01�00001�NUMCEL �N�MERO DE TEL�FONO  �A�010�0�S�        �
 * KNDBSYRF�CONSULTA DETALLE CHEQUE DIGITA�X�08�00170�02�00011�ALIAS  �NOMBRE DEL BENEFICIA�A�060�0�S�        �
 * KNDBSYRF�CONSULTA DETALLE CHEQUE DIGITA�X�08�00170�03�00071�CONCEPT�CONCEPTO            �A�055�0�S�        �
 * KNDBSYRF�CONSULTA DETALLE CHEQUE DIGITA�X�08�00170�04�00126�FECALTA�FECHA DE OPERACI�N  �A�010�0�S�        �
 * KNDBSYRF�CONSULTA DETALLE CHEQUE DIGITA�X�08�00170�05�00136�IMPOPE �IMPORTE DEL CHEQUE  �S�017�2�S�        �
 * KNDBSYRF�CONSULTA DETALLE CHEQUE DIGITA�X�08�00170�06�00153�CVEOTP �C�DIGO DE SEGURIDAD �A�016�0�S�        �
 * KNDBSYRF�CONSULTA DETALLE CHEQUE DIGITA�X�08�00170�07�00157�FOLIOCH�FOLIO CHEQUE        �A�012�0�S�        �
 * KNDBSYRF�CONSULTA DETALLE CHEQUE DIGITA�X�08�00170�08�00169�TIPCHEQ�TIPO CHEQUE         �A�002�0�S�        �
 * FICHERO: FDXWYRF.txt
 * WYRFKNDBSYRFKNDBSYRFKN1CWYRF1S0170N000                     CICSDM112017-11-07-13.16.49.093091CICSDM112017-11-07-13.16.49.093112
 * </pre></code>
 *
 * @author Arquitectura Spring BBVA
 * @see RespuestaTransaccionWyrf
 */
@Transaccion(nombre = "WYRF", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionWyrf.class, inputMapper = InputWYRFMapper.class, outputMapper = OutputWYRFMapper.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoKNDBWYRF.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWyrf implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
