package com.bbva.mzic.accounts.dao.model.wyit;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>WYIT</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWyit</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionWyit</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: CCT.WYIT.txt
 * WYITUPGRADE DE NIVEL DE CUENTA         KN        KN1CWYITCORP    KNDBEYIT            WYIT  SS0108CNNNNN    SSTS   M    NNNSNNNN  NN                2014-02-19CICSDM112019-07-1611.27.56XMZ0895 2014-02-19-18.24.19.131209CICSDM110001-01-010001-01-01
 * FICHERO: FDF.WYIT.txt
 * KNDBEYIT�UPGRADE DE NIVEL DE CUENTA    �F�05�00108�01�00001�NUMCTA �NUMERO DE CUENTA    �A�020�0�R�        �
 * KNDBEYIT�UPGRADE DE NIVEL DE CUENTA    �F�05�00108�02�00021�CLIENTE�NUMERO DE CLIENTE   �A�008�0�R�        �
 * KNDBEYIT�UPGRADE DE NIVEL DE CUENTA    �F�05�00108�03�00029�FIRMA  �FIRMA DIGITAL       �A�020�0�O�        �
 * KNDBEYIT�UPGRADE DE NIVEL DE CUENTA    �F�05�00108�04�00049�FECHACL�FECHA CALCULO       �A�030�0�O�        �
 * KNDBEYIT�UPGRADE DE NIVEL DE CUENTA    �F�05�00108�05�00079�UDIS   �UDIS                �A�030�0�O�        �
 * KNDBSYIT�SALIDA UPGRADE DE NIVEL DE CTA�X�04�00182�01�00001�FOLIO  �FOLIO OPERACION AST �A�010�0�S�        �
 * KNDBSYIT�SALIDA UPGRADE DE NIVEL DE CTA�X�04�00182�02�00011�CORREO �CORREO ELECTRONICO  �A�080�0�S�        �
 * KNDBSYIT�SALIDA UPGRADE DE NIVEL DE CTA�X�04�00182�03�00091�NOMCLIE�NOMBRE COMPLETO CTE �A�062�0�S�        �
 * KNDBSYIT�SALIDA UPGRADE DE NIVEL DE CTA�X�04�00182�04�00153�DESCRIP�DESCRIPCION CUENTA  �A�030�0�S�        �
 * FICHERO: FDX.WYIT.txt
 * WYITKNDBSYITKNDBSYITKN1CWYIT1S0182N000                     CICSDM112019-03-15-13.41.56.164881XMZ0895 2019-07-16-13.15.46.491979
</pre></code>
 * 
 * @see RespuestaTransaccionWyit
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "WYIT", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionWyit.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoKNDBEYIT.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWyit implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
