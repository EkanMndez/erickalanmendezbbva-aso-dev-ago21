package com.bbva.mzic.accounts.dao.model.mchft103_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MCHFT103</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMchft103_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMchft103_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MCHFT103-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MCHFT103&quot; application=&quot;MCHF&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;idClient&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;startPeriodDate&quot; type=&quot;String&quot;
 * size=&quot;30&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;endPeriodDate&quot; type=&quot;String&quot;
 * size=&quot;30&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;idClient&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;group name=&quot;month&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;monthName&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;totalCreditPayment&quot; type=&quot;Double&quot;
 * size=&quot;17&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;totalSpends&quot; type=&quot;Double&quot; size=&quot;17&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;totalSavings&quot; type=&quot;Double&quot; size=&quot;17&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;totalInvestments&quot; type=&quot;Double&quot;
 * size=&quot;17&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;totalInsurance&quot; type=&quot;Double&quot;
 * size=&quot;17&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;initialBalance&quot; type=&quot;Double&quot;
 * size=&quot;17&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;totalDeposits&quot; type=&quot;Double&quot;
 * size=&quot;17&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;available&quot; type=&quot;Double&quot; size=&quot;17&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion para calculo de totales en checkup salud
 * financiera&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMchft103_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MCHFT103", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMchft103_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMchft103_1 {

    /**
     * <p>
     * Campo <code>idClient</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "idClient", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String idclient;

    /**
     * <p>
     * Campo <code>startPeriodDate</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "startPeriodDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
    private String startperioddate;

    /**
     * <p>
     * Campo <code>endPeriodDate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "endPeriodDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
    private String endperioddate;

}
