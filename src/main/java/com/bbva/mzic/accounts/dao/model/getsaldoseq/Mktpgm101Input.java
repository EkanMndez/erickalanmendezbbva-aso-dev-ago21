
package com.bbva.mzic.accounts.dao.model.getsaldoseq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mktpgm101Input complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mktpgm101Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WK_CVECTA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mktpgm101Input", propOrder = {
    "wkcvecta"
})
public class Mktpgm101Input {

    @XmlElement(name = "WK_CVECTA", required = true)
    protected String wkcvecta;

    /**
     * Gets the value of the wkcvecta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKCVECTA() {
        return wkcvecta;
    }

    /**
     * Sets the value of the wkcvecta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKCVECTA(String value) {
        this.wkcvecta = value;
    }

}
