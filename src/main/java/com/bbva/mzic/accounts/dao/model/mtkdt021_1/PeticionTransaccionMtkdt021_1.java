package com.bbva.mzic.accounts.dao.model.mtkdt021_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MTKDT021</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt021_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt021_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MTKDT021.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT021&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;localAmountCurrency&quot; type=&quot;String&quot;
 * size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;initialPeriodDate&quot; type=&quot;String&quot;
 * size=&quot;19&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;endedPeriodDate&quot; type=&quot;String&quot;
 * size=&quot;19&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;movementType&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;statusMovement&quot; type=&quot;String&quot;
 * size=&quot;21&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;paginationIn&quot; order=&quot;7&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;pageSize&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;movement&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ticketId&quot; type=&quot;Long&quot; size=&quot;13&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;14&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;amountCurrency&quot; type=&quot;String&quot;
 * size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;100&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;transactionTypeId&quot; type=&quot;String&quot;
 * size=&quot;24&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;transactionTypeName&quot; type=&quot;String&quot;
 * size=&quot;100&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;creationDate&quot; type=&quot;String&quot;
 * size=&quot;19&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;applicationDate&quot; type=&quot;String&quot;
 * size=&quot;19&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;valuationDate&quot; type=&quot;String&quot;
 * size=&quot;19&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;financingTypeId&quot; type=&quot;String&quot;
 * size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;financingTypeName&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;status&quot; type=&quot;String&quot; size=&quot;21&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;statusName&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;statusUpdateDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;contractId&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;16&quot; name=&quot;contractNumber&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;17&quot; name=&quot;numberTypeId&quot; type=&quot;String&quot;
 * size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;18&quot; name=&quot;numberTypeName&quot; type=&quot;String&quot;
 * size=&quot;21&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;19&quot; name=&quot;contractProductId&quot; type=&quot;String&quot;
 * size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;20&quot; name=&quot;contractProductName&quot; type=&quot;String&quot;
 * size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;21&quot; name=&quot;productTypeId&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;22&quot; name=&quot;productTypeName&quot; type=&quot;String&quot;
 * size=&quot;35&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;23&quot; name=&quot;alias&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;24&quot; name=&quot;bankId&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;25&quot; name=&quot;bankName&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;26&quot; name=&quot;tags&quot; type=&quot;String&quot; size=&quot;300&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;27&quot; name=&quot;movementType&quot; type=&quot;String&quot;
 * size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;28&quot; name=&quot;movementName&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;paginationOut&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;hasMoreData&quot; type=&quot;String&quot; size=&quot;1&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion encargada de listar los movimientos asociados
 * a una cuenta CED, estos movimientos podran ser filtrados por rango de
 * fechas y por estatus.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt021_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MTKDT021", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMtkdt021_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt021_1 {

    /**
     * <p>
     * Campo <code>accountId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
    private String accountid;

    /**
     * <p>
     * Campo <code>localAmountCurrency</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "localAmountCurrency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
    private String localamountcurrency;

    /**
     * <p>
     * Campo <code>initialPeriodDate</code>, &iacute;ndice: <code>3</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "initialPeriodDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true)
    private String initialperioddate;

    /**
     * <p>
     * Campo <code>endedPeriodDate</code>, &iacute;ndice: <code>4</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "endedPeriodDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true)
    private String endedperioddate;

    /**
     * <p>
     * Campo <code>movementType</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "movementType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
    private String movementtype;

    /**
     * <p>
     * Campo <code>statusMovement</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "statusMovement", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 21, signo = true)
    private String statusmovement;

    /**
     * <p>
     * Campo <code>paginationIn</code>, &iacute;ndice: <code>7</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 7, nombre = "paginationIn", tipo = TipoCampo.TABULAR)
    private List<Paginationin> paginationin;

}
