// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt033_1;

import java.lang.String;
import java.util.List;

privileged aspect PeticionTransaccionMtkdt033_1_Roo_JavaBean {
    
    public String PeticionTransaccionMtkdt033_1.getAccountid() {
        return this.accountid;
    }
    
    public void PeticionTransaccionMtkdt033_1.setAccountid(String accountid) {
        this.accountid = accountid;
    }
    
    public String PeticionTransaccionMtkdt033_1.getFunctionaryid() {
        return this.functionaryid;
    }
    
    public void PeticionTransaccionMtkdt033_1.setFunctionaryid(String functionaryid) {
        this.functionaryid = functionaryid;
    }
    
    public String PeticionTransaccionMtkdt033_1.getBankid() {
        return this.bankid;
    }
    
    public void PeticionTransaccionMtkdt033_1.setBankid(String bankid) {
        this.bankid = bankid;
    }
    
    public String PeticionTransaccionMtkdt033_1.getBranchid() {
        return this.branchid;
    }
    
    public void PeticionTransaccionMtkdt033_1.setBranchid(String branchid) {
        this.branchid = branchid;
    }
    
    public String PeticionTransaccionMtkdt033_1.getCurrency() {
        return this.currency;
    }
    
    public void PeticionTransaccionMtkdt033_1.setCurrency(String currency) {
        this.currency = currency;
    }
    
    public List<Deposit> PeticionTransaccionMtkdt033_1.getDeposit() {
        return this.deposit;
    }
    
    public void PeticionTransaccionMtkdt033_1.setDeposit(List<Deposit> deposit) {
        this.deposit = deposit;
    }
    
    public String PeticionTransaccionMtkdt033_1.getBankname() {
        return this.bankname;
    }
    
    public void PeticionTransaccionMtkdt033_1.setBankname(String bankname) {
        this.bankname = bankname;
    }
    
}
