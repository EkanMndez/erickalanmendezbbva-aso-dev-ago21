package com.bbva.mzic.accounts.dao.model.peh8;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>PEM0H8E</code> de la transacci&oacute;n <code>PEH8</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEM0H8E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEM0H8E {

    /**
     * <p>
     * Campo <code>NUMCTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "NUMCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 18, longitudMaxima = 18)
    private String numcta;

    /**
     * <p>
     * Campo <code>ULTCLA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "ULTCLA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String ultcla;

    /**
     * <p>
     * Campo <code>ULTSEC</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "ULTSEC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String ultsec;

}
