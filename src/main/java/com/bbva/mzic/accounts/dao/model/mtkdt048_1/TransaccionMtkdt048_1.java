package com.bbva.mzic.accounts.dao.model.mtkdt048_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT048</code>
 *
 * @see PeticionTransaccionMtkdt048_1
 * @see RespuestaTransaccionMtkdt048_1
 */
@Component
public class TransaccionMtkdt048_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt048_1, RespuestaTransaccionMtkdt048_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt048_1 invocar(final PeticionTransaccionMtkdt048_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt048_1.class, RespuestaTransaccionMtkdt048_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt048_1 invocarCache(final PeticionTransaccionMtkdt048_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt048_1.class, RespuestaTransaccionMtkdt048_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt048_1.class, "vaciearCache");
	}
}
