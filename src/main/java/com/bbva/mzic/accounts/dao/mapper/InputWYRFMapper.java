package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.wyrf.FormatoKNDBWYRF;
import com.bbva.mzic.accounts.dao.model.wyrf.PeticionTransaccionWyrf;
import com.bbva.mzic.serviceutils.rm.utils.errors.EnumError;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class InputWYRFMapper implements IMapper<DtoIntFilterAccount, PeticionTransaccionWyrf> {
    @Override
    public PeticionTransaccionWyrf map(DtoIntFilterAccount filterAccount) {
        PeticionTransaccionWyrf peticionTransaccionWyrf = new PeticionTransaccionWyrf();
        FormatoKNDBWYRF formatoKNDBWYRF = new FormatoKNDBWYRF();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String[] digitalCheckIdData = filterAccount.getDigitalCheckId().split("\\|");
        String fechAlta, folioCA;

        fechAlta = digitalCheckIdData[2];
        folioCA = digitalCheckIdData[1];

        formatoKNDBWYRF.setCliepu(filterAccount.getCustomerId());
        formatoKNDBWYRF.setFolioca(folioCA);
        try {
            formatoKNDBWYRF.setFechalt(formatter.parse(fechAlta));
        } catch (ParseException pe) {
            throw new BusinessServiceException(EnumError.WRONG_PARAMETERS.getAlias());
        }
        // construccion del objeto para llamar a host
        peticionTransaccionWyrf.getCuerpo().getPartes().add(formatoKNDBWYRF);

        return peticionTransaccionWyrf;
    }
}
