// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.wyit;

import java.lang.String;

privileged aspect FormatoKNDBSYIT_Roo_ToString {
    
    public String FormatoKNDBSYIT.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Correo: ").append(getCorreo()).append(", ");
        sb.append("Descrip: ").append(getDescrip()).append(", ");
        sb.append("Folio: ").append(getFolio()).append(", ");
        sb.append("Nomclie: ").append(getNomclie());
        return sb.toString();
    }
    
}
