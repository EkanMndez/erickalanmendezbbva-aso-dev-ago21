// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import com.bbva.mzic.accounts.dao.model.mtkdt061_1.Contentreport;
import java.util.List;

privileged aspect Reportlisttransaction_Roo_JavaBean {
    
    public List<Contentreport> Reportlisttransaction.getContentreport() {
        return this.contentreport;
    }
    
    public void Reportlisttransaction.setContentreport(List<Contentreport> contentreport) {
        this.contentreport = contentreport;
    }
    
}
