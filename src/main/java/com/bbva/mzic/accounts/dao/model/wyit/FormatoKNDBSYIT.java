package com.bbva.mzic.accounts.dao.model.wyit;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * Formato de datos <code>KNDBSYIT</code> de la transacci&oacute;n <code>WYIT</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBSYIT")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBSYIT {

    /**
     * <p>
     * Campo <code>FOLIO</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "FOLIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String folio;

    /**
     * <p>
     * Campo <code>CORREO</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CORREO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
    private String correo;

    /**
     * <p>
     * Campo <code>NOMCLIE</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "NOMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 62, longitudMaxima = 62)
    private String nomclie;

    /**
     * <p>
     * Campo <code>DESCRIP</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "DESCRIP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String descrip;

}
