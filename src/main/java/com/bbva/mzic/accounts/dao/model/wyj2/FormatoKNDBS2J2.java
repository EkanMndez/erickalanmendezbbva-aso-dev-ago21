package com.bbva.mzic.accounts.dao.model.wyj2;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Formato de datos <code>KNDBS2J2</code> de la transacci&oacute;n <code>WYJ2</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBS2J2")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBS2J2 {

    /**
     * <p>
     * Campo <code>IDENTSE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "IDENTSE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String identse;

    /**
     * <p>
     * Campo <code>DESCRSE</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "DESCRSE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String descrse;

}
