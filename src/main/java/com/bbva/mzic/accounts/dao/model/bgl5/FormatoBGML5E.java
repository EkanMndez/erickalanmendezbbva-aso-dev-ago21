package com.bbva.mzic.accounts.dao.model.bgl5;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>BGML5E</code> de la transacci&oacute;n <code>BGL5</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGML5E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGML5E implements IFormat {

    /**
     * <p>
     * Campo <code>TIPOASU</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "TIPOASU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String tipoasu;

    /**
     * <p>
     * Campo <code>ASUNTO</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "ASUNTO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String asunto;

    /**
     * <p>
     * Campo <code>SECUENC</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "SECUENC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 6, longitudMaxima = 6)
    private String secuenc;

    /**
     * <p>
     * Campo <code>CLIENPU</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "CLIENPU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String clienpu;

    /**
     * <p>
     * Campo <code>USUARIO</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "USUARIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String usuario;

}
