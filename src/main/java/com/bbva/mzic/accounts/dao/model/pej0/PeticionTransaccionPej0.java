package com.bbva.mzic.accounts.dao.model.pej0;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>PEJ0</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPej0</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionPej0</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: PEJ0.QGDTFDF.txt
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�01�00001�TARJETA�TARJETA DE DEBITO   �A�016�0�O�        �
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�02�00017�CELULAR�NUMERO DE CELULAR   �A�010�0�O�        �
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�03�00027�COMPCEL�COMPANIA CELUAR     �A�002�0�O�        �
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�04�00029�ENTIDAD�NUMERO DE ENTIDAD   �A�004�0�O�        �
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�05�00033�OFICINA�OFICINA DE APERTURA �A�004�0�O�        �
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�06�00037�PRODUCT�PRODUCTO            �A�002�0�O�        �
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�07�00039�CUENTA �NUMERO DE CUENTA    �A�008�0�O�        �
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�08�00047�STATUS �ESTATUS A CONSULTAR �A�004�0�O�        �
 * PEM0J0E �CONSULTA CTA. EXPRESS         �F�09�00068�09�00051�INDICAP�IND. DE PAGINACION  �A�018�0�O�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�01�00001�CAMPO01�REG. ENCONTRADOS    �N�003�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�02�00004�CAMPO02�TARJETA DE DEBITO   �A�016�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�03�00020�CAMPO03�CVE. CIA. CELULAR   �A�002�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�04�00022�CAMPO04�DESC. CIA. CELULAR  �A�020�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�05�00042�CAMPO05�NUMERO CELULAR      �A�010�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�06�00052�CAMPO06�CVE. CEL. PAIS      �A�003�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�07�00055�CAMPO07�DESCRP. PAIS        �A�015�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�08�00070�CAMPO08�ENTIDAD             �A�004�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�09�00074�CAMPO09�OFICINA APERTURA    �A�004�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�10�00078�CAMPO10�PRODUCTO            �A�002�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�11�00080�CAMPO11�NUM. CUENTA         �A�008�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�12�00088�CAMPO12�NUM. CLIENTE        �A�008�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�13�00096�CAMPO13�ESTATUS             �A�004�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�14�00100�CAMPO14�DESC. ESTATUS       �A�020�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�15�00120�CAMPO15�NUM.CEL. CANCELADO  �A�010�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�16�00130�CAMPO16�NOMBRE CLIENTE      �A�020�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�17�00150�CAMPO17�PRIMER APELLIDO     �A�020�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�18�00170�CAMPO18�SEGUNDO APELLIDO    �A�020�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�19�00190�CAMPO19�TIPO DE TELEFONO    �A�001�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�20�00191�CAMPO20�DESC DE TELEFONO    �A�020�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�21�00211�CAMPO21�LADA DE TELEFONO    �A�003�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�22�00214�CAMPO22�NUM. DE TELEFONO    �A�007�0�S�        �
 * PEM0J0S �CONSULTA CTA. EXPRESS         �X�23�00224�23�00221�CAMPO23�EXT. DE TELEFONO    �N�004�0�S�        �
 * FICHERO: PEJ0.QGDTFDX.txt
 * PEJ0PEM0J0S PENC0J0SPE2C00J01S                             CICSDM112010-01-14-13.14.02.268893CICSDM112010-01-14-13.14.02.268908
 * FICHERO: PEJ0.QGDTCCT.txt
 * PEJ0CONSULTA CTA. EXPRESS              PE        PE2C00J0     01 PEM0J0E             PEJ0  NN3000CNNNNN    SSTN ABMC   NNNNNNNN  NN                2010-01-14CICSDM112018-03-0815.42.41XMZ2603 2010-01-14-12.00.25.774098CICSDM110001-01-010001-01-01
</pre></code>
 * 
 * @see RespuestaTransaccionPej0
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "PEJ0", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionPej0.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoPEM0J0E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionPej0 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
