package com.bbva.mzic.accounts.dao.model.bgl5;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>BGML5YS</code> de la transacci&oacute;n
 * <code>BGL5</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGML5YS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGML5YS implements IFormat {

	/**
	 * <p>
	 * Campo <code>TIPASUN</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TIPASUN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipasun;

	/**
	 * <p>
	 * Campo <code>ASUNTO</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "ASUNTO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String asunto;

	/**
	 * <p>
	 * Campo <code>IDCTA</code>, &iacute;ndice: <code>3</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IDCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String idcta;

	/**
	 * <p>
	 * Campo <code>IDCLABE</code>, &iacute;ndice: <code>4</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "IDCLABE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String idclabe;

	/**
	 * <p>
	 * Campo <code>CLABE</code>, &iacute;ndice: <code>5</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "CLABE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 18, longitudMaxima = 18)
	private String clabe;

	/**
	 * <p>
	 * Campo <code>NUMCEL</code>, &iacute;ndice: <code>6</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "NUMCEL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String numcel;

	/**
	 * <p>
	 * Campo <code>CATEGO</code>, &iacute;ndice: <code>7</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "CATEGO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String catego;

	/**
	 * <p>
	 * Campo <code>CODSUB</code>, &iacute;ndice: <code>8</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "CODSUB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String codsub;

	/**
	 * <p>
	 * Campo <code>DESPROD</code>, &iacute;ndice: <code>9</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "DESPROD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String desprod;

	/**
	 * <p>
	 * Campo <code>DESSUB</code>, &iacute;ndice: <code>10</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "DESSUB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String dessub;

	/**
	 * <p>
	 * Campo <code>CODBANC</code>, &iacute;ndice: <code>11</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "CODBANC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String codbanc;

	/**
	 * <p>
	 * Campo <code>DESCBAN</code>, &iacute;ndice: <code>12</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "DESCBAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String descban;

	/**
	 * <p>
	 * Campo <code>CRGEST</code>, &iacute;ndice: <code>13</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "CRGEST", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String crgest;

	/**
	 * <p>
	 * Campo <code>DESCR</code>, &iacute;ndice: <code>14</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "DESCR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String descr;

	/**
	 * <p>
	 * Campo <code>ALIAS</code>, &iacute;ndice: <code>15</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "ALIAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String alias;

	/**
	 * <p>
	 * Campo <code>FECHAP</code>, &iacute;ndice: <code>16</code>, tipo:
	 * <code>FECHA</code>
	 */
	@Campo(indice = 16, nombre = "FECHAP", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fechap;

	/**
	 * <p>
	 * Campo <code>ESTATUS</code>, &iacute;ndice: <code>17</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "ESTATUS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String estatus;

	/**
	 * <p>
	 * Campo <code>ESTATDE</code>, &iacute;ndice: <code>18</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "ESTATDE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
	private String estatde;

	/**
	 * <p>
	 * Campo <code>DIVISA</code>, &iacute;ndice: <code>19</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "DIVISA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divisa;

	/**
	 * <p>
	 * Campo <code>FCURREN</code>, &iacute;ndice: <code>20</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "FCURREN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String fcurren;

	/**
	 * <p>
	 * Campo <code>SALDIS</code>, &iacute;ndice: <code>21</code>, tipo:
	 * <code>DECIMAL</code>
	 */
	@Campo(indice = 21, nombre = "SALDIS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
	private BigDecimal saldis;

	/**
	 * <p>
	 * Campo <code>SALPOS</code>, &iacute;ndice: <code>22</code>, tipo:
	 * <code>DECIMAL</code>
	 */
	@Campo(indice = 22, nombre = "SALPOS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
	private BigDecimal salpos;

	/**
	 * <p>
	 * Campo <code>SALPEND</code>, &iacute;ndice: <code>23</code>, tipo:
	 * <code>DECIMAL</code>
	 */
	@Campo(indice = 23, nombre = "SALPEND", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
	private BigDecimal salpend;

	/**
	 * <p>
	 * Campo <code>PLAZA</code>, &iacute;ndice: <code>24</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "PLAZA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
	private String plaza;

	/**
	 * <p>
	 * Campo <code>SALBCO</code>, &iacute;ndice: <code>25</code>, tipo:
	 * <code>DECIMAL</code>
	 */
	@Campo(indice = 25, nombre = "SALBCO", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
	private BigDecimal salbco;

	/**
	 * <p>
	 * Campo <code>IMPRET</code>, &iacute;ndice: <code>26</code>, tipo:
	 * <code>DECIMAL</code>
	 */
	@Campo(indice = 26, nombre = "IMPRET", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
	private BigDecimal impret;

	/**
	 * <p>
	 * Campo <code>MSGERR</code>, &iacute;ndice: <code>27</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 27, nombre = "MSGERR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String msgerr;

	/**
	 * <p>
	 * Campo <code>INDCTAM</code>, &iacute;ndice: <code>28</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 28, nombre = "INDCTAM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indctam;

	/**
	 * <p>
	 * Campo <code>TDDASOC</code>, &iacute;ndice: <code>29</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 29, nombre = "TDDASOC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
	private String tddasoc;

	/**
	 * <p>
	 * Campo <code>SMANCC</code>, &iacute;ndice: <code>30</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 30, nombre = "SMANCC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
	private String smancc;

	/**
	 * <p>
	 * Campo <code>COMMANC</code>, &iacute;ndice: <code>31</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 31, nombre = "COMMANC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String commanc;

	/**
	 * <p>
	 * Campo <code>CPROMEM</code>, &iacute;ndice: <code>32</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "CPROMEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String cpromem;

}
