package com.bbva.mzic.accounts.dao.model.mtkdt055_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>account</code>, utilizado por la clase <code>Functionary</code></p>
 * 
 * @see Functionary
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Account {
	
	/**
	 * <p>Campo <code>accountNumber</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "accountNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 16, signo = true, obligatorio = true)
	private String accountnumber;
	
	/**
	 * <p>Campo <code>creationDate</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "creationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String creationdate;
	
	/**
	 * <p>Campo <code>accountStatusId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "accountStatusId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String accountstatusid;
	
	/**
	 * <p>Campo <code>accountStatusDescription</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "accountStatusDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 35, signo = true, obligatorio = true)
	private String accountstatusdescription;
	
	/**
	 * <p>Campo <code>customerName</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "customerName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 60, signo = true, obligatorio = true)
	private String customername;
	
	/**
	 * <p>Campo <code>customerLastName</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "customerLastName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String customerlastname;
	
	/**
	 * <p>Campo <code>customerSecondLastName</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "customerSecondLastName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String customersecondlastname;
	
	/**
	 * <p>Campo <code>accountMx</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "accountMx", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 12, signo = true, obligatorio = true)
	private String accountmx;
	
	/**
	 * <p>Campo <code>functionaryAccountMx</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "functionaryAccountMx", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String functionaryaccountmx;
	
}
