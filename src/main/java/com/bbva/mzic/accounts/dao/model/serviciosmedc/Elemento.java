
package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for Elemento complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Elemento"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="elementos" type="{http://servicio}ArrayOfElemento"/&gt;
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="datos" type="{http://servicio}ArrayOfDato"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Elemento", propOrder = {"elementos", "nombre", "datos"})
public class Elemento {

    @XmlElement(required = true, nillable = true)
    protected ArrayOfElemento elementos;
    @XmlElement(required = true, nillable = true)
    protected String nombre;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfDato datos;

    /**
     * Gets the value of the elementos property.
     * 
     * @return possible object is {@link ArrayOfElemento }
     * 
     */
    public ArrayOfElemento getElementos() {
        return elementos;
    }

    /**
     * Sets the value of the elementos property.
     * 
     * @param value allowed object is {@link ArrayOfElemento }
     * 
     */
    public void setElementos(ArrayOfElemento value) {
        this.elementos = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the datos property.
     * 
     * @return possible object is {@link ArrayOfDato }
     * 
     */
    public ArrayOfDato getDatos() {
        return datos;
    }

    /**
     * Sets the value of the datos property.
     * 
     * @param value allowed object is {@link ArrayOfDato }
     * 
     */
    public void setDatos(ArrayOfDato value) {
        this.datos = value;
    }

}
