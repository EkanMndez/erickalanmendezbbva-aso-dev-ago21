package com.bbva.mzic.accounts.dao.model.mtkdt023_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>paginationOut</code>, utilizado por la clase
 * <code>RespuestaTransaccionMtkdt023_1</code>
 * </p>
 * 
 * @see RespuestaTransaccionMtkdt023_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Paginationout {

    /**
     * <p>
     * Campo <code>paginationKey</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "paginationKey", tipo = TipoCampo.ENTERO, longitudMaxima = 4, signo = true)
    private Integer paginationkey;

    /**
     * <p>
     * Campo <code>hasMoreData</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "hasMoreData", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
    private String hasmoredata;

}
