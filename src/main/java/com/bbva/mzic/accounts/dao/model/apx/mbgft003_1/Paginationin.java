package com.bbva.mzic.accounts.dao.model.apx.mbgft003_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>paginationIn</code>, utilizado por la clase
 * <code>PeticionTransaccionMbgdt003_1</code>
 * </p>
 * 
 * @see PeticionTransaccionMbgdt003_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Paginationin {

    /**
     * <p>
     * Campo <code>paginationKey</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "paginationKey", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
    private String paginationkey;

    /**
     * <p>
     * Campo <code>paginationSize</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "paginationSize", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
    private String paginationsize;

    /**
     * <p>
     * Campo <code>saldoAntMov</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 3, nombre = "saldoAntMov", tipo = TipoCampo.DECIMAL, longitudMaxima = 16, signo = true)
    private BigDecimal saldoantmov;

}
