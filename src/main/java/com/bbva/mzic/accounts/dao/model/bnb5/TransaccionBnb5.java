package com.bbva.mzic.accounts.dao.model.bnb5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BNB5</code>
 * 
 * @see PeticionTransaccionBnb5
 * @see RespuestaTransaccionBnb5
 */
@Component
public class TransaccionBnb5 implements InvocadorTransaccion<PeticionTransaccionBnb5, RespuestaTransaccionBnb5> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBnb5 invocar(PeticionTransaccionBnb5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBnb5.class, RespuestaTransaccionBnb5.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBnb5 invocarCache(PeticionTransaccionBnb5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBnb5.class, RespuestaTransaccionBnb5.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBnb5.class, "vaciearCache");
	}
}
