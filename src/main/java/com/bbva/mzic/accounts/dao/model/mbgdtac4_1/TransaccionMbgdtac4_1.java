package com.bbva.mzic.accounts.dao.model.mbgdtac4_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDTAC4</code>
 * 
 * @see PeticionTransaccionMbgdtac4_1
 * @see RespuestaTransaccionMbgdtac4_1
 */
@Component
public class TransaccionMbgdtac4_1 implements InvocadorTransaccion<PeticionTransaccionMbgdtac4_1, RespuestaTransaccionMbgdtac4_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdtac4_1 invocar(PeticionTransaccionMbgdtac4_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtac4_1.class, RespuestaTransaccionMbgdtac4_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdtac4_1 invocarCache(PeticionTransaccionMbgdtac4_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtac4_1.class, RespuestaTransaccionMbgdtac4_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdtac4_1.class, "vaciearCache");
	}
}
