package com.bbva.mzic.accounts.dao.model.bnp6;

import java.lang.String;

privileged aspect FormatoBNRBNP6_Roo_JavaBean {
    
    public String FormatoBNRBNP6.getCliente() {
        return this.cliente;
    }
    
    public void FormatoBNRBNP6.setCliente(String cliente) {
        this.cliente = cliente;
    }
    
    public String FormatoBNRBNP6.getProdcta() {
        return this.prodcta;
    }
    
    public void FormatoBNRBNP6.setProdcta(String prodcta) {
        this.prodcta = prodcta;
    }
    
    public String FormatoBNRBNP6.getTipprod() {
        return this.tipprod;
    }
    
    public void FormatoBNRBNP6.setTipprod(String tipprod) {
        this.tipprod = tipprod;
    }
    
}
