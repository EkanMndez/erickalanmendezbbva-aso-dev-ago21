package com.bbva.mzic.accounts.dao.model.mtkdt045_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT045</code>
 *
 * @see PeticionTransaccionMtkdt045_1
 * @see RespuestaTransaccionMtkdt045_1
 */
@Component
public class TransaccionMtkdt045_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt045_1, RespuestaTransaccionMtkdt045_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt045_1 invocar(final PeticionTransaccionMtkdt045_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt045_1.class, RespuestaTransaccionMtkdt045_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt045_1 invocarCache(final PeticionTransaccionMtkdt045_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt045_1.class, RespuestaTransaccionMtkdt045_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt045_1.class, "vaciearCache");
	}
}
