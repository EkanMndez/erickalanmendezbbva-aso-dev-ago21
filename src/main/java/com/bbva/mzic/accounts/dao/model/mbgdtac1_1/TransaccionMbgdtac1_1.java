package com.bbva.mzic.accounts.dao.model.mbgdtac1_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDTAC1</code>
 * 
 * @see PeticionTransaccionMbgdtac1_1
 * @see RespuestaTransaccionMbgdtac1_1
 */
@Component
public class TransaccionMbgdtac1_1 implements InvocadorTransaccion<PeticionTransaccionMbgdtac1_1, RespuestaTransaccionMbgdtac1_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdtac1_1 invocar(PeticionTransaccionMbgdtac1_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtac1_1.class, RespuestaTransaccionMbgdtac1_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdtac1_1 invocarCache(PeticionTransaccionMbgdtac1_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtac1_1.class, RespuestaTransaccionMbgdtac1_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdtac1_1.class, "vaciearCache");
	}

}
