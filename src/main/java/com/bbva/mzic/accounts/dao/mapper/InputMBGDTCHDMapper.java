package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.mbgdtchd_1.PeticionTransaccionMbgdtchd_1;

import java.text.SimpleDateFormat;

public class InputMBGDTCHDMapper implements IMapper<DtoIntFilterAccount, PeticionTransaccionMbgdtchd_1> {
    @Override
    public PeticionTransaccionMbgdtchd_1 map(DtoIntFilterAccount filterAccount) {
        PeticionTransaccionMbgdtchd_1 peticionTransaccionMbgdtchd_1 = new PeticionTransaccionMbgdtchd_1();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        peticionTransaccionMbgdtchd_1.setAccount(filterAccount.getAccountId());
        peticionTransaccionMbgdtchd_1.setFromoperationdate(format.format(filterAccount.getFromOperationDate()));
        peticionTransaccionMbgdtchd_1.setTooperationdate(format.format(filterAccount.getToOperationDate()));
        peticionTransaccionMbgdtchd_1.setPaginationkey(Integer.parseInt(filterAccount.getPaginationKey()));

        return peticionTransaccionMbgdtchd_1;
    }
}
