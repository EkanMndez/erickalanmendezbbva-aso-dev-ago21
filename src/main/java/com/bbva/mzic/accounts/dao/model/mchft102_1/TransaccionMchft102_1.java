package com.bbva.mzic.accounts.dao.model.mchft102_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCHFT102</code>
 * 
 * @see PeticionTransaccionMchft102_1
 * @see RespuestaTransaccionMchft102_1
 */
@Component
public class TransaccionMchft102_1 implements InvocadorTransaccion<PeticionTransaccionMchft102_1, RespuestaTransaccionMchft102_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMchft102_1 invocar(PeticionTransaccionMchft102_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchft102_1.class, RespuestaTransaccionMchft102_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMchft102_1 invocarCache(PeticionTransaccionMchft102_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchft102_1.class, RespuestaTransaccionMchft102_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMchft102_1.class, "vaciearCache");
	}
}
