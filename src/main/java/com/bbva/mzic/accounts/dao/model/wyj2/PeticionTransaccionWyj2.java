package com.bbva.mzic.accounts.dao.model.wyj2;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>WYJ2</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWyj2</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionWyj2</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: FDF-WYJ2.TXT
 * KNDBEYJ2�ENTRADA TRX CANCELACION DE CTA�F�07�00139�01�00001�NUMCTA �NUMECTA             �A�020�0�R�        �
 * KNDBEYJ2�ENTRADA TRX CANCELACION DE CTA�F�07�00139�02�00021�NUMCLIE�NUMCLIE             �A�008�0�R�        �
 * KNDBEYJ2�ENTRADA TRX CANCELACION DE CTA�F�07�00139�03�00029�TIPOCTA�TIPOCTA             �A�001�0�R�        �
 * KNDBEYJ2�ENTRADA TRX CANCELACION DE CTA�F�07�00139�04�00030�IDRUC  �IDRUC               �A�010�0�R�        �
 * KNDBEYJ2�ENTRADA TRX CANCELACION DE CTA�F�07�00139�05�00040�MOTCANC�MOTCANCEL           �A�050�0�R�        �
 * KNDBEYJ2�ENTRADA TRX CANCELACION DE CTA�F�07�00139�06�00090�TIPOACE�TIPOACEP            �A�020�0�R�        �
 * KNDBEYJ2�ENTRADA TRX CANCELACION DE CTA�F�07�00139�07�00110�IDTIPAC�IDTIPACP            �A�030�0�R�        �
 * KNDBSYJ2�SALIDA TRX CANCELACION DE CTAS�X�07�00162�01�00001�NUMCTO �NUMCTO              �A�020�0�S�        �
 * KNDBSYJ2�SALIDA TRX CANCELACION DE CTAS�X�07�00162�02�00021�TIPOTDD�TIPOTDD             �A�003�0�S�        �
 * KNDBSYJ2�SALIDA TRX CANCELACION DE CTAS�X�07�00162�03�00024�NUMTDD �NUMTDD              �A�016�0�S�        �
 * KNDBSYJ2�SALIDA TRX CANCELACION DE CTAS�X�07�00162�04�00040�NAMETDD�NAMETDD             �A�020�0�S�        �
 * KNDBSYJ2�SALIDA TRX CANCELACION DE CTAS�X�07�00162�05�00060�FOLIOAS�FOLIOAST            �A�010�0�S�        �
 * KNDBSYJ2�SALIDA TRX CANCELACION DE CTAS�X�07�00162�06�00070�NUMCEL �NUMCEL              �A�013�0�S�        �
 * KNDBSYJ2�SALIDA TRX CANCELACION DE CTAS�X�07�00162�07�00083�CORREO �CORREO              �A�080�0�S�        �
 * KNDBS2J2�SALIDA TRX CANCELACION DE CTAS�X�02�00030�01�00001�IDENTSE�IDENTSER            �A�010�0�S�        �
 * KNDBS2J2�SALIDA TRX CANCELACION DE CTAS�X�02�00030�02�00011�DESCRSE�DESCRSER            �A�020�0�S�        �
 * FICHERO: FDX-WYJ2.TXT
 * WYJ2KNDBSYJ2KNDBSYJ2KN1CWYJ21S0170N000                     XM02672 2019-06-26-17.45.23.044640XM02672 2019-06-26-17.45.23.044654
 * WYJ2KNDBS2J2KNDBS2J2KN1CWYJ21S0030S020                     XM02672 2019-06-26-17.46.25.821557XM02672 2019-06-26-17.46.25.821580
 * FICHERO: CCT-WYJ2.TXT
 * WYJ2CANCELACION DE CUENTAS BCOM        KN        KN1CWYJ2     01 KNDBEYJ2            WYJ2  NS0039ANNNNN    SSTS    C   NNNSNNNN  NN                2014-02-18CICSDM112019-06-2617.54.50XM02672 2014-02-18-13.14.52.295252CICSDM110001-01-010001-01-01
</pre></code>
 * 
 * @see RespuestaTransaccionWyj2
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "WYJ2", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionWyj2.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoKNDBEYJ2.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWyj2 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
