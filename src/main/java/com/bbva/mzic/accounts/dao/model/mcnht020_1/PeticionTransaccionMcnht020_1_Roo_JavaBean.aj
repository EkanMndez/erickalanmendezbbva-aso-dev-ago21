// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mcnht020_1;

import java.lang.String;

privileged aspect PeticionTransaccionMcnht020_1_Roo_JavaBean {
    
    public String PeticionTransaccionMcnht020_1.getCustomerid() {
        return this.customerid;
    }
    
    public void PeticionTransaccionMcnht020_1.setCustomerid(String customerid) {
        this.customerid = customerid;
    }
    
    public String PeticionTransaccionMcnht020_1.getCustomeraccount() {
        return this.customeraccount;
    }
    
    public void PeticionTransaccionMcnht020_1.setCustomeraccount(String customeraccount) {
        this.customeraccount = customeraccount;
    }
    
}
