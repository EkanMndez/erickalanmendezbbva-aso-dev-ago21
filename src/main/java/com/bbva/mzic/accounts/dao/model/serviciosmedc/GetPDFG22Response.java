
package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EXIT_ESTATUS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
 *         &lt;element name="URL_Consulta" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ERR_DESC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"exitestatus", "documento", "urlConsulta", "errdesc"})
@XmlRootElement(name = "getPDF_G22Response")
public class GetPDFG22Response {

    @XmlElement(name = "EXIT_ESTATUS", required = true, nillable = true)
    protected String exitestatus;
    @XmlElement(required = true)
    protected byte[] documento;
    @XmlElement(name = "URL_Consulta", required = true, nillable = true)
    protected String urlConsulta;
    @XmlElement(name = "ERR_DESC", required = true, nillable = true)
    protected String errdesc;

    /**
     * Gets the value of the exitestatus property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEXITESTATUS() {
        return exitestatus;
    }

    /**
     * Sets the value of the exitestatus property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setEXITESTATUS(String value) {
        this.exitestatus = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     * @return possible object is byte[]
     */
    public byte[] getDocumento() {
        return documento == null ? null : documento.clone();
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value allowed object is byte[]
     */
    public void setDocumento(byte[] value) {
        if (value == null) {
            this.documento = new byte[0];
        } else {
            this.documento = Arrays.copyOf(value, value.length);
        }
    }

    /**
     * Gets the value of the urlConsulta property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getURLConsulta() {
        return urlConsulta;
    }

    /**
     * Sets the value of the urlConsulta property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setURLConsulta(String value) {
        this.urlConsulta = value;
    }

    /**
     * Gets the value of the errdesc property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getERRDESC() {
        return errdesc;
    }

    /**
     * Sets the value of the errdesc property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setERRDESC(String value) {
        this.errdesc = value;
    }

}
