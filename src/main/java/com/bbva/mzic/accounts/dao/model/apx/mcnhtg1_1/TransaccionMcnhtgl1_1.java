package com.bbva.mzic.accounts.dao.model.apx.mcnhtg1_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCNHTGL1</code>
 * 
 * @see PeticionTransaccionMcnhtgl1_1
 * @see RespuestaTransaccionMcnhtgl1_1
 */
@Component
public class TransaccionMcnhtgl1_1 implements InvocadorTransaccion<PeticionTransaccionMcnhtgl1_1, RespuestaTransaccionMcnhtgl1_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMcnhtgl1_1 invocar(PeticionTransaccionMcnhtgl1_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMcnhtgl1_1.class, RespuestaTransaccionMcnhtgl1_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMcnhtgl1_1 invocarCache(PeticionTransaccionMcnhtgl1_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMcnhtgl1_1.class, RespuestaTransaccionMcnhtgl1_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMcnhtgl1_1.class, "vaciearCache");
	}
}
