
package com.bbva.mzic.accounts.dao.model.getsaldoseq;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * com.bbva.mzic.accounts.dao.model.getsaldoseq package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	private final static String HTTP_NAMESPACE = "http://getsaldoseq.wsbeans.iseries/";
	private final static QName _Mktpgm101Response_QNAME = new QName(HTTP_NAMESPACE, "mktpgm101Response");
	private final static QName _Mktpgm101_QNAME = new QName(HTTP_NAMESPACE, "mktpgm101");
	private final static QName _Mktpgm101XMLResponse_QNAME = new QName(HTTP_NAMESPACE, "mktpgm101_XMLResponse");
	private final static QName _Mktpgm101XML_QNAME = new QName(HTTP_NAMESPACE, "mktpgm101_XML");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema
	 * derived classes for package: com.bbva.mzic.accounts.dao.model.getsaldoseq
	 * 
	 */
	public ObjectFactory() {
		super();
	}

	/**
	 * Create an instance of {@link Mktpgm101Response }
	 * 
	 */
	public Mktpgm101Response createMktpgm101Response() {
		return new Mktpgm101Response();
	}

	/**
	 * Create an instance of {@link Mktpgm101 }
	 * 
	 */
	public Mktpgm101 createMktpgm101() {
		return new Mktpgm101();
	}

	/**
	 * Create an instance of {@link Mktpgm101XML }
	 * 
	 */
	public Mktpgm101XML createMktpgm101XML() {
		return new Mktpgm101XML();
	}

	/**
	 * Create an instance of {@link Mktpgm101XMLResponse }
	 * 
	 */
	public Mktpgm101XMLResponse createMktpgm101XMLResponse() {
		return new Mktpgm101XMLResponse();
	}

	/**
	 * Create an instance of {@link Mktpgm101Result }
	 * 
	 */
	public Mktpgm101Result createMktpgm101Result() {
		return new Mktpgm101Result();
	}

	/**
	 * Create an instance of {@link Mktpgm101Input }
	 * 
	 */
	public Mktpgm101Input createMktpgm101Input() {
		return new Mktpgm101Input();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Mktpgm101Response
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = HTTP_NAMESPACE, name = "mktpgm101Response")
	public JAXBElement<Mktpgm101Response> createMktpgm101Response(Mktpgm101Response value) {
		return new JAXBElement<Mktpgm101Response>(_Mktpgm101Response_QNAME, Mktpgm101Response.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Mktpgm101
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = HTTP_NAMESPACE, name = "mktpgm101")
	public JAXBElement<Mktpgm101> createMktpgm101(Mktpgm101 value) {
		return new JAXBElement<Mktpgm101>(_Mktpgm101_QNAME, Mktpgm101.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link Mktpgm101XMLResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = HTTP_NAMESPACE, name = "mktpgm101_XMLResponse")
	public JAXBElement<Mktpgm101XMLResponse> createMktpgm101XMLResponse(Mktpgm101XMLResponse value) {
		return new JAXBElement<Mktpgm101XMLResponse>(_Mktpgm101XMLResponse_QNAME, Mktpgm101XMLResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Mktpgm101XML
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = HTTP_NAMESPACE, name = "mktpgm101_XML")
	public JAXBElement<Mktpgm101XML> createMktpgm101XML(Mktpgm101XML value) {
		return new JAXBElement<Mktpgm101XML>(_Mktpgm101XML_QNAME, Mktpgm101XML.class, null, value);
	}

}
