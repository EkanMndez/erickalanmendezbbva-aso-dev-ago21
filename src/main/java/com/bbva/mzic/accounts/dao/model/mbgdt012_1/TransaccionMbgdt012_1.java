package com.bbva.mzic.accounts.dao.model.mbgdt012_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDT012</code>
 * 
 * @see PeticionTransaccionMbgdt012_1
 * @see RespuestaTransaccionMbgdt012_1
 */
@Component
public class TransaccionMbgdt012_1 implements InvocadorTransaccion<PeticionTransaccionMbgdt012_1, RespuestaTransaccionMbgdt012_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdt012_1 invocar(PeticionTransaccionMbgdt012_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdt012_1.class, RespuestaTransaccionMbgdt012_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdt012_1 invocarCache(PeticionTransaccionMbgdt012_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdt012_1.class, RespuestaTransaccionMbgdt012_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdt012_1.class, "vaciearCache");
	}
}
