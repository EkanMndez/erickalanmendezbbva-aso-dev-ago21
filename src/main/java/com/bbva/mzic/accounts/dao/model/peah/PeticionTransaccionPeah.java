package com.bbva.mzic.accounts.dao.model.peah;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>PEAH</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPeah</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionPeah</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: PEAH-CCT.txt
 * PEAHCONSULTA DATOS BASICOS DOMICILIO   PE        PE2C00AH     01 PEM0AHE             PEAH  NN3000CNNNNN    SSTN    C   NNNNNNNN  NN                2015-07-20XMCB198 2018-03-2010.43.58XMZ2603 2015-07-20-08.19.02.485584XMCB198 0001-01-010001-01-01
 * FICHERO: PEAH-FDF.txt
 * PEM0AHE �CONSULTA DATOS DOMICILIO      �F�02�00022�01�00001�NUMCLIE�NUMERO DE CLIENTE   �A�008�0�O�        �
 * PEM0AHE �CONSULTA DATOS DOMICILIO      �F�02�00022�02�00009�IDENTIF�NUMERO DE IDENTIFICA�A�014�0�O�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�01�00001�NUMCLIE�NUMERO DE CLIENTE   �A�008�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�02�00009�CODIDEN�CODIGO ID           �A�001�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�03�00010�CLAIDEN�CLAVE ID            �A�010�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�04�00020�DIGIDEN�DIGITO IDENTIFICADOR�A�001�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�05�00021�SECUIDE�SECUENCIA ID        �A�002�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�06�00023�DIDENTI�DESCRIPCION ID      �A�020�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�07�00043�TITULO �TITULO              �A�005�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�08�00048�NOMBRE �NOMBRE              �A�020�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�09�00068�PRIAPE �PRIMER APELLIDO     �A�020�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�10�00088�SEGAPE �SEGUNDO APELLIDO    �A�020�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�11�00108�PETDOMI�TIPO DOMICILIO      �A�001�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�12�00109�DPETDOM�DESCRIPCION TIPO DOM�A�016�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�13�00125�TIPOPER�TIPO PERTENENCIA    �A�001�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�14�00126�DTIPOPE�DESCRIPCION TIPO PER�A�016�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�15�00142�FECRESI�FECHA DE RESIDENCIA �A�010�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�16�00152�TIPOVIA�TIPO DE VIALIDAD    �A�005�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�17�00157�CALLE  �CALLE               �A�050�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�18�00207�NUMEXT �NUMERO EXTERIOR     �A�008�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�19�00215�TIPOVIV�TIPO DE VIVIENDA    �A�007�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�20�00222�NUMINT �NUMERO INTERIOR     �A�008�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�21�00230�TIPOASE�TIPO DE ASENTAMIENTO�A�005�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�22�00235�COLONIA�COLONIA             �A�020�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�23�00255�ECALLE1�ENTRE CALLE         �A�020�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�24�00275�ECALLE2�Y CALLE             �A�020�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�25�00295�POBLACI�DELEGACION O MUNICIP�A�030�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�26�00325�CODPOST�CODIGO POSTAL       �A�005�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�27�00330�ESTADO �ESTADO              �A�002�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�28�00332�DESTADO�DESCTIPCION ESTADO  �A�020�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�29�00352�CODPAIS�PAIS                �A�004�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�30�00356�DCODPAI�DESCRIPCION PAIS    �A�030�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�31�00386�TIPTEL1�TIPO DE TELEFONO 1  �A�001�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�32�00387�PREFIJ1�PREFIJO TELEFONO 1  �A�003�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�33�00390�NUMTEL1�NUMERO TELEFONO 1   �A�007�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�34�00397�EXTEN1 �EXTENCION TELEFONO 1�N�004�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�35�00401�TIPTEL2�TIPO DE TELEFONO 2  �A�001�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�36�00402�PREFIJ2�PREFIJO TELEFONO 2  �A�003�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�37�00405�NUMTEL2�NUMERO TELEFONO 2   �A�007�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�38�00412�EXTEN2 �EXTENCION TELEFONO 2�N�004�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�39�00416�TIPTEL3�TIPO DE TELEFONO 3  �A�001�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�40�00417�PREFIJ3�PREFIJO TELEFONO 3  �A�003�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�41�00420�NUMTEL3�NUMERO TELEFONO 3   �A�007�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�42�00427�EXTEN3 �EXTENCION TELEFONO 3�N�004�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�43�00431�FEALTCL�FECHA ALTA CLIENTE  �A�010�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�44�00441�FEULBAJ�FECHA BAJA CLIENTE  �A�010�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�45�00451�FEULMOD�FECHA MOD CLIENTE   �A�010�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�46�00461�NUMTER �NUMERO DE TERMINAL  �A�004�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�47�00465�HORA   �HORA MODIFICACION   �A�008�0�S�        �
 * PEM0AHS �CONSULTA DATOS DOMICILIO      �X�48�00480�48�00473�USUARIO�USUARIO             �A�008�0�S�        �
 * FICHERO: PEAH-FDX.txt
 * PEAHPEM0AHS PENC0AHSPE2C00AH1S                             XMCB198 2015-07-20-08.29.14.618306XMCB198 2015-07-20-08.29.14.618331
</pre></code>
 * 
 * @see RespuestaTransaccionPeah
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "PEAH", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx", respuesta = RespuestaTransaccionPeah.class, atributos = {
		@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA") })
@Multiformato(formatos = { FormatoPEM0AHE.class })
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionPeah implements MensajeMultiparte {

	/**
	 * <p>
	 * Cuerpo del mensaje de petici&oacute;n multiparte
	 * </p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>
	 * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
	 * </p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
