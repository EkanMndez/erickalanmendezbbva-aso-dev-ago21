package com.bbva.mzic.accounts.dao.model.wykx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYKX</code>
 * 
 * @see PeticionTransaccionWykx
 * @see RespuestaTransaccionWykx
 */
@Component
public class TransaccionWykx implements InvocadorTransaccion<PeticionTransaccionWykx, RespuestaTransaccionWykx> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWykx invocar(PeticionTransaccionWykx transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWykx.class, RespuestaTransaccionWykx.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWykx invocarCache(PeticionTransaccionWykx transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWykx.class, RespuestaTransaccionWykx.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWykx.class, "vaciearCache");
	}
}
