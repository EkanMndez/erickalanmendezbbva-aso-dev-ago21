package com.bbva.mzic.accounts.dao.model.mtkdt031_1;

import java.math.BigDecimal;
import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MTKDT031</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt031_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt031_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT031.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT031&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;functionaryId&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;bankId&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;withdrawalAmount&quot; type=&quot;Double&quot;
 * size=&quot;14&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;withdrawalMainReference&quot; type=&quot;String&quot;
 * size=&quot;100&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;withdrawalCreationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;withdrawalReason&quot; type=&quot;String&quot;
 * size=&quot;100&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;withdrawalApplicationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;withdrawalTypeId&quot; type=&quot;String&quot;
 * size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;withdrawalDocumentId&quot; type=&quot;Long&quot;
 * size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;group name=&quot;references&quot; order=&quot;13&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;firstDescription&quot; type=&quot;String&quot;
 * size=&quot;300&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;secondDescription&quot; type=&quot;String&quot;
 * size=&quot;300&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;thirdDescription&quot; type=&quot;String&quot;
 * size=&quot;300&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;fourthDescription&quot; type=&quot;String&quot;
 * size=&quot;300&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ticketId&quot; type=&quot;String&quot; size=&quot;13&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion utilizada para realizar el alta de un retiro
 * por area interna en Ticket APX.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt031_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MTKDT031",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMtkdt031_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt031_1 {
		
		/**
	 * <p>Campo <code>accountId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
	private String accountid;
	
	/**
	 * <p>Campo <code>functionaryId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "functionaryId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String functionaryid;
	
	/**
	 * <p>Campo <code>bankId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "bankId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String bankid;
	
	/**
	 * <p>Campo <code>branchId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String branchid;
	
	/**
	 * <p>Campo <code>withdrawalAmount</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 5, nombre = "withdrawalAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 14, signo = true, obligatorio = true)
	private BigDecimal withdrawalamount;
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String currency;
	
	/**
	 * <p>Campo <code>withdrawalMainReference</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "withdrawalMainReference", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
	private String withdrawalmainreference;
	
	/**
	 * <p>Campo <code>withdrawalCreationDate</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "withdrawalCreationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String withdrawalcreationdate;
	
	/**
	 * <p>Campo <code>withdrawalReason</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "withdrawalReason", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true)
	private String withdrawalreason;
	
	/**
	 * <p>Campo <code>withdrawalApplicationDate</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "withdrawalApplicationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String withdrawalapplicationdate;
	
	/**
	 * <p>Campo <code>withdrawalTypeId</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "withdrawalTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String withdrawaltypeid;
	
	/**
	 * <p>Campo <code>withdrawalDocumentId</code>, &iacute;ndice: <code>12</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 12, nombre = "withdrawalDocumentId", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true, obligatorio = true)
	private int withdrawaldocumentid;
	
	/**
	 * <p>Campo <code>references</code>, &iacute;ndice: <code>13</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 13, nombre = "references", tipo = TipoCampo.TABULAR)
	private List<References> references;
	
}
