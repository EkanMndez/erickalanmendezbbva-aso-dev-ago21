package com.bbva.mzic.accounts.dao.model.wyis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYIS</code>
 * 
 * @see PeticionTransaccionWyis
 * @see RespuestaTransaccionWyis
 */
@Component("transaccion-wyis")
public class TransaccionWyis implements InvocadorTransaccion<PeticionTransaccionWyis, RespuestaTransaccionWyis> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWyis invocar(PeticionTransaccionWyis transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyis.class, RespuestaTransaccionWyis.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWyis invocarCache(PeticionTransaccionWyis transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyis.class, RespuestaTransaccionWyis.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWyis.class, "vaciearCache");
	}
}
