package com.bbva.mzic.accounts.dao.model.bgl1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>BGL1</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBgl1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBgl1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: CCT_BGL1.txt
 * BGL1CONSULTA CONDICIONES DE CUENTA     BG        BG2CBGL1     01 BGCEBGL1            BGL1  SS0000NNNNNN    SSTN    C   NNNSNNNN  NN                2016-10-04CICSDM112016-10-0518.39.10XM07178 2016-10-04-12.44.05.319986XM07178 0001-01-010001-01-01
 * FICHERO: FDF_BGL1.txt
 * BGCEBGL1�CONSULTA CONDICIONES DE CUENTA�F�01�00020�01�00001�CCC    �CUENTA CLIENTE      �A�020�0�R�        �
 * BGCSBGL1�DETALLE CONDICIONES DE CUENTA �X�04�00033�01�00001�SMANCC �SALDO  MANEJO CCC   �A�012�0�S�        �
 * BGCSBGL1�DETALLE CONDICIONES DE CUENTA �X�04�00033�02�00013�COMMANC�COMISION MANEJO CCC �A�007�0�S�        �
 * BGCSBGL1�DETALLE CONDICIONES DE CUENTA �X�04�00033�03�00020�SPROMEM�COM SALDO PROMEDIO  �A�007�0�S�        �
 * BGCSBGL1�DETALLE CONDICIONES DE CUENTA �X�04�00033�04�00027�CPROMEM�COM FIJA            �A�007�0�S�        �
 * FICHERO: FDX_BGL1.txt
 * BGL1BGCSBGL1BGCSBGL1BG2CBGL11S                             XM07178 2016-10-04-11.31.16.285605XM07178 2016-10-04-11.31.16.285624
</pre></code>
 * 
 * @see RespuestaTransaccionBgl1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BGL1", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBgl1.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBGCEBGL1.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBgl1 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
