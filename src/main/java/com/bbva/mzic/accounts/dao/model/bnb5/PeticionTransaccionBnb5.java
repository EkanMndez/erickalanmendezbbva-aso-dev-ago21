package com.bbva.mzic.accounts.dao.model.bnb5;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>BNB5</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBnb5</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBnb5</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: BNB5.CCT.txt
 * BNB5CONSULTA EL NIVEL DE UNA CTA       BN        BN2CBNB5     01 BNB500E             BNB5  NN3000CNNNNN    SSTN A MC   NNNNNNNN  NN                2019-02-13CICSDM112019-02-2813.34.11CICSDM112019-02-13-17.13.40.689004CICSDM110001-01-010001-01-01
 * FICHERO: BNB5.FDF.txt
 * BNB500E �CONSULTA DEL NIVEL DE LA CTA. �F�02�00028�01�00001�CUENTA �NUMERO DE CUENTA    �A�020�0�O�        �
 * BNB500E �CONSULTA DEL NIVEL DE LA CTA. �F�02�00028�02�00021�CLIENTE�NUMERO DE CLIENTE   �A�008�0�O�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�01�00001�PRODUC �PRODUCTO            �A�002�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�02�00003�SUBPROD�SUBPRODUCTO         �A�004�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�03�00007�DSUBPRO�DESC SUBPRODUCTO    �A�020�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�04�00027�DIVISA �DIVISA              �A�003�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�05�00030�REGCTA �REGIMEN DE LA CUENTA�A�001�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�06�00031�DREGCTA�DESC REGIMEN CUENTA �A�011�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�07�00042�REGFIS �REGIMEN FISCAL      �A�004�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�08�00046�DREGFIS�DESC REGIMEN FISCAL �A�020�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�09�00066�TARIFA �TARIFA              �A�004�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�10�00070�DTARIFA�DESC TARIFA         �A�020�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�11�00090�CTAASO �CUENTA ASOCIADA     �A�020�0�S�        �
 * BNB500S �CONSULTA DEL NIVEL DE LA CTA  �X�12�00111�12�00110�INDRES �INDICADOR DE ESTADO �A�002�0�S�        �
 * FICHERO: BNB5.FDX.txt
 * BNB5BNB500S BNCBNB5SBN2CBNB51S+DC                          CICSDM112019-02-14-09.06.23.130446CICSDM112019-02-14-09.06.23.130473
</pre></code>
 * 
 * @see RespuestaTransaccionBnb5
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BNB5", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBnb5.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBNB500E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBnb5 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
