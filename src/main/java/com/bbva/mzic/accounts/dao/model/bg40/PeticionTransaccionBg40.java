package com.bbva.mzic.accounts.dao.model.bg40;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>BG40</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBg40</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBg40</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: FDX-BG40.txt
 * BG40BGMF40A BGMF40A BG2CG4001S                             XM01AUX 2016-05-09-12.32.19.861054XM01AUX 2016-05-09-12.32.19.861077
 * FICHERO: FDF-BG40.txt
 * BGMF40  �CONSULTA DE METAS             �F�01�00008�01�00001�CLIENTE�NUMERO DE CLIENTE   �A�008�0�R�        �
 * BGMF40A �CONSULTA DE METAS             �X�04�00041�01�00001�CUENTA �NUMERO DE CUENTA    �A�010�0�S�        �
 * BGMF40A �CONSULTA DE METAS             �X�04�00041�02�00011�SECUENC�SECUENCIA           �N�010�0�S�        �
 * BGMF40A �CONSULTA DE METAS             �X�04�00041�03�00021�INDESTA�INDICADOR DE ESTATUS�A�001�0�S�        �
 * BGMF40A �CONSULTA DE METAS             �X�04�00041�04�00022�CONTRAT�NUMERO DE CONTRATO  �A�020�0�S�        �
 * 
 * FICHERO: CCT-BG40.txt
 * BG40CONSULTA DE METAS                  BG        BG2CG400     01 BGMF40              BG40  SS0000CNNNNN    SSTN    C   NNNSNNNN  NN                2016-05-09CICSDM112017-12-0114.02.52XMZ0896 2016-05-09-11.00.32.221487CICSDM110001-01-010001-01-01
</pre></code>
 * 
 * @see RespuestaTransaccionBg40
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BG40", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBg40.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBGMF40.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBg40 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
