package com.bbva.mzic.accounts.dao.model.mtkdt055_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>branch</code>, utilizado por la clase <code>RespuestaTransaccionMtkdt055_1</code></p>
 * 
 * @see RespuestaTransaccionMtkdt055_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Branch {
	
	/**
	 * <p>Campo <code>branchId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String branchid;
	
	/**
	 * <p>Campo <code>branchDescription</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "branchDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 45, signo = true, obligatorio = true)
	private String branchdescription;
	
	/**
	 * <p>Campo <code>branchBankingTypeId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "branchBankingTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String branchbankingtypeid;
	
	/**
	 * <p>Campo <code>branchBankingTypeDescription</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "branchBankingTypeDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String branchbankingtypedescription;
	
	/**
	 * <p>Campo <code>branchDivision</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "branchDivision", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
	private String branchdivision;
	
	/**
	 * <p>Campo <code>branchStatusId</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "branchStatusId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String branchstatusid;
	
	/**
	 * <p>Campo <code>branchStatusDescription</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "branchStatusDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
	private String branchstatusdescription;
	
	/**
	 * <p>Campo <code>stateId</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "stateId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String stateid;
	
	/**
	 * <p>Campo <code>state</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "state", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String state;
	
	/**
	 * <p>Campo <code>town</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "town", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String town;
	
	/**
	 * <p>Campo <code>totalAccountsByBranch</code>, &iacute;ndice: <code>11</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 11, nombre = "totalAccountsByBranch", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true, obligatorio = true)
	private int totalaccountsbybranch;
	
	/**
	 * <p>Campo <code>totalFunctionaryByBranch</code>, &iacute;ndice: <code>12</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 12, nombre = "totalFunctionaryByBranch", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true, obligatorio = true)
	private int totalfunctionarybybranch;
	
	/**
	 * <p>Campo <code>functionary</code>, &iacute;ndice: <code>13</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 13, nombre = "functionary", tipo = TipoCampo.TABULAR)
	private List<Functionary> functionary;
	
}
