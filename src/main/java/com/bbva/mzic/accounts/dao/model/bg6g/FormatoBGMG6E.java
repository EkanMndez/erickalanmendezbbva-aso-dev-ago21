package com.bbva.mzic.accounts.dao.model.bg6g;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGMG6E</code> de la transacci&oacute;n <code>BG6G</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMG6E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGMG6E implements IFormat {

    /**
     * <p>
     * Campo <code>NUMCUEN</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "NUMCUEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String numcuen;

    /**
     * <p>
     * Campo <code>IDPRODU</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "IDPRODU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String idprodu;

    /**
     * <p>
     * Campo <code>POSICIO</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "POSICIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String posicio;

    /**
     * <p>
     * Campo <code>NUMTYPE</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "NUMTYPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 14, longitudMaxima = 14)
    private String numtype;

    /**
     * <p>
     * Campo <code>RELTYPE</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "RELTYPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 14, longitudMaxima = 14)
    private String reltype;

}
