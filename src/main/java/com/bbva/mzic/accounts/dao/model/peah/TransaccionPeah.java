package com.bbva.mzic.accounts.dao.model.peah;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>PEAH</code>
 * 
 * @see PeticionTransaccionPeah
 * @see RespuestaTransaccionPeah
 */
@Component
public class TransaccionPeah implements InvocadorTransaccion<PeticionTransaccionPeah, RespuestaTransaccionPeah> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionPeah invocar(PeticionTransaccionPeah transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeah.class, RespuestaTransaccionPeah.class, transaccion);
	}

	@Override
	public RespuestaTransaccionPeah invocarCache(PeticionTransaccionPeah transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeah.class, RespuestaTransaccionPeah.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionPeah.class, "vaciearCache");
	}
}
