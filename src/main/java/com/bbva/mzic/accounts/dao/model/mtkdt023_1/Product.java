package com.bbva.mzic.accounts.dao.model.mtkdt023_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>product</code>, utilizado por la clase
 * <code>RespuestaTransaccionMtkdt023_1</code>
 * </p>
 * 
 * @see RespuestaTransaccionMtkdt023_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Product {

    /**
     * <p>
     * Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
    private String id;

    /**
     * <p>
     * Campo <code>contractId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 2, nombre = "contractId", tipo = TipoCampo.ENTERO, longitudMaxima = 8, signo = true, obligatorio = true)
    private int contractid;

    /**
     * <p>
     * Campo <code>statusCode</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "statusCode", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
    private String statuscode;

    /**
     * <p>
     * Campo <code>status</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "status", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
    private String status;

    /**
     * <p>
     * Campo <code>ticketId</code>, &iacute;ndice: <code>5</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 5, nombre = "ticketId", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true, obligatorio = true)
    private long ticketid;

    /**
     * <p>
     * Campo <code>description</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "description", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 35, signo = true, obligatorio = true)
    private String description;

    /**
     * <p>
     * Campo <code>relationType</code>, &iacute;ndice: <code>7</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 7, nombre = "relationType", tipo = TipoCampo.TABULAR)
    private List<Relationtype> relationtype;

}
