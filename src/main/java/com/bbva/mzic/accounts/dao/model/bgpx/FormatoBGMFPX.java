package com.bbva.mzic.accounts.dao.model.bgpx;


import java.math.BigDecimal;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>BGMFPX</code> de la transacci&oacute;n <code>BGPX</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMFPX")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGMFPX implements IFormat {

    /**
     * <p>
     * Campo <code>PAGKEY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "PAGKEY", tipo = TipoCampo.ENTERO, longitudMinima = 8, longitudMaxima = 8)
    private Integer pagkey;

    /**
     * <p>
     * Campo <code>PAGSIZE</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "PAGSIZE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String pagsize;

    /**
     * <p>
     * Campo <code>PAGSDO</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 3, nombre = "PAGSDO", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
    private BigDecimal pagsdo;

    /**
     * <p>
     * Campo <code>ORDER</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "ORDER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String order;

    /**
     * <p>
     * Campo <code>CONTRAC</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "CONTRAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String contrac;

    /**
     * <p>
     * Campo <code>OPEDATE</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "OPEDATE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String opedate;

    /**
     * <p>
     * Campo <code>FOPDATE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "FOPDATE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String fopdate;

    /**
     * <p>
     * Campo <code>TOPDATE</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "TOPDATE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String topdate;

    /**
     * <p>
     * Campo <code>AMOUNT</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "AMOUNT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 17, longitudMaxima = 17)
    private String amount;

    /**
     * <p>
     * Campo <code>FAMOUNT</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "FAMOUNT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 17, longitudMaxima = 17)
    private String famount;

    /**
     * <p>
     * Campo <code>TAMOUNT</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "TAMOUNT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 17, longitudMaxima = 17)
    private String tamount;

    /**
     * <p>
     * Campo <code>FMOVMT</code>, &iacute;ndice: <code>12</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 12, nombre = "FMOVMT", tipo = TipoCampo.ENTERO, longitudMinima = 13, longitudMaxima = 13)
    private Long fmovmt;

    /**
     * <p>
     * Campo <code>TMOVMT</code>, &iacute;ndice: <code>13</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 13, nombre = "TMOVMT", tipo = TipoCampo.ENTERO, longitudMinima = 13, longitudMaxima = 13)
    private Long tmovmt;

    /**
     * <p>
     * Campo <code>MONEYFL</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "MONEYFL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String moneyfl;

    /**
     * <p>
     * Campo <code>SWITCH</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "SWITCHS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String switchS;

}
