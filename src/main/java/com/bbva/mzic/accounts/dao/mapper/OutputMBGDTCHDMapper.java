package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.bbva.mzic.accounts.business.dto.*;
import com.bbva.mzic.accounts.dao.model.mbgdtchd_1.Digitalchecks;
import com.bbva.mzic.accounts.dao.model.mbgdtchd_1.RespuestaTransaccionMbgdtchd_1;
import com.bbva.mzic.accounts.rm.enums.DigitalCheckStatusEnum;
import com.bbva.mzic.accounts.rm.enums.DigitalCheckTypeEnum;
import com.bbva.mzic.accounts.rm.enums.ReceiverTypeEnum;
import com.bbva.mzic.serviceutils.rm.utils.errors.EnumError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OutputMBGDTCHDMapper implements IMapper<RespuestaTransaccionMbgdtchd_1, DtoIntDigitalCheckList> {
    @Override
    public DtoIntDigitalCheckList map(RespuestaTransaccionMbgdtchd_1 response) {

        if (response == null) {
            return null;
        }

        DtoIntDigitalCheckList digitalCheckList = new DtoIntDigitalCheckList();
        DtoIntDigitalCheck intDigitalCheck = new DtoIntDigitalCheck();
        DtoIntReceiver receiver = new DtoIntReceiver();
        DtoIntHolder holder = new DtoIntHolder();
        Money sentMoney = new Money();
        DtoIntDigitalCheckStatus digitalCheckStatus = new DtoIntDigitalCheckStatus();
        DtoIntDigitalCheckType digitalCheckType = new DtoIntDigitalCheckType();
        DtoIntCashout cashout = new DtoIntCashout();

        List<Digitalchecks> digitalChecksResponse = response.getDigitalchecks();

        for (Digitalchecks digitalCheck : digitalChecksResponse) {
            intDigitalCheck.setId(digitalCheck.getId());
            intDigitalCheck.setNumber(digitalCheck.getNumber());
            intDigitalCheck.setConcept(digitalCheck.getConcept());
            intDigitalCheck.setOperationDate(getDate(digitalCheck.getOperationdate()));
            intDigitalCheck.setDueDate(getDate(digitalCheck.getDuedate()));
            intDigitalCheck.setOperationNumber(digitalCheck.getOperationnumber());

            receiver.setType(ReceiverTypeEnum.PHONE);
            receiver.setValue(digitalCheck.getReceiver().get(0).getValue());
            holder.setName(digitalCheck.getReceiver().get(0).getHolder().get(0).getName());
            receiver.setHolder(holder);
            intDigitalCheck.setReceiver(receiver);

            sentMoney.setAmount(digitalCheck.getSentmoney().get(0).getAmount());
            sentMoney.setCurrency(digitalCheck.getSentmoney().get(0).getCurrency());
            intDigitalCheck.setSentMoney(sentMoney);

            digitalCheckStatus.setId(getDigitalCheckStatus(digitalCheck.getDigitalcheckstatus().get(0).getId()));
            digitalCheckStatus.setName(getDigitalCheckStatusName(digitalCheck.getDigitalcheckstatus().get(0).getId()));
            intDigitalCheck.setDigitalCheckStatus(digitalCheckStatus);

            digitalCheckType.setId(getDigitalCheckTypeId(digitalCheck.getDigitalchecktype().get(0).getId()));
            digitalCheckType.setName(getDigitalCheckTypeName(digitalCheck.getDigitalchecktype().get(0).getId()));
            intDigitalCheck.setDigitalCheckType(digitalCheckType);

            cashout.setAtmId(digitalCheck.getCashout().get(0).getAtmid());
            cashout.setDate(getDate(digitalCheck.getCashout().get(0).getDate()));

            digitalCheckList.getDigitalCheckList().add(intDigitalCheck);

            intDigitalCheck = new DtoIntDigitalCheck();
            receiver = new DtoIntReceiver();
            holder = new DtoIntHolder();
            sentMoney = new Money();
            digitalCheckStatus = new DtoIntDigitalCheckStatus();
            digitalCheckType = new DtoIntDigitalCheckType();
            cashout = new DtoIntCashout();
        }

        Integer paginationKey = response.getPaginationout().get(0).getPaginationkey();
        String hasMoreData = response.getPaginationout().get(0).getHasmoredata();

        digitalCheckList.setPagination(getPagination(paginationKey, hasMoreData));

        return digitalCheckList;

    }

    private DigitalCheckStatusEnum getDigitalCheckStatus(String status) {
        DigitalCheckStatusEnum statusEnum = DigitalCheckStatusEnum.VALID;
        if ("PA".equals(status)) {
            statusEnum = DigitalCheckStatusEnum.CASHED;
        } else if ("RS".equals(status)) {
            statusEnum = DigitalCheckStatusEnum.CANCELED_WITHOUT_FUNDS;
        }

        return statusEnum;
    }

    private String getDigitalCheckStatusName(String status) {
        String statusName = "Valid";
        if ("PA".equals(status)) {
            statusName = "Cashed";
        } else if ("RS".equals(status)) {
            statusName = "Canceled Without Funds";
        }

        return statusName;
    }

    private DigitalCheckTypeEnum getDigitalCheckTypeId(String id) {
        DigitalCheckTypeEnum typeEnum = DigitalCheckTypeEnum.RESERVED_AMOUNT_CHECK;

        if ("5".equals(id)) {
            typeEnum = DigitalCheckTypeEnum.UNRESERVED_AMOUNT_CHECK;
        } else if ("6".equals(id)) {
            typeEnum = DigitalCheckTypeEnum.RESERVED_AMOUNT_CHECK;
        }
        return typeEnum;
    }

    private String getDigitalCheckTypeName(String id) {
        String typeName = "";

        if ("5".equals(id)) {
            typeName = "Unreserved Amount Check";
        } else if ("6".equals(id)) {
            typeName = "Reserved Amount Check";
        }
        return typeName;
    }

    private Pagination getPagination(Integer paginationKey, String hasMoreData) {
        Pagination pagination = new Pagination();

        if ("S".equals(hasMoreData)) {
            pagination.setPage(paginationKey.longValue() + 1);
        } else {
            pagination.setPage(paginationKey.longValue());
        }

        return pagination;
    }

    private Date getDate(String dateInput) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = format.parse(dateInput);
        } catch (ParseException pe) {
            throw new BusinessServiceException(EnumError.DATE_INCORRECT_FORMAT.getAlias());
        }
        return date;
    }
}
