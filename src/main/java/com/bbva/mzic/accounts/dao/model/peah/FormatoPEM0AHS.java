package com.bbva.mzic.accounts.dao.model.peah;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * Formato de datos <code>PEM0AHS</code> de la transacci&oacute;n <code>PEAH</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEM0AHS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEM0AHS {
	
	/**
	 * <p>Campo <code>NUMCLIE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NUMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String numclie;
	
	/**
	 * <p>Campo <code>CODIDEN</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "CODIDEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String codiden;
	
	/**
	 * <p>Campo <code>CLAIDEN</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "CLAIDEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String claiden;
	
	/**
	 * <p>Campo <code>DIGIDEN</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "DIGIDEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String digiden;
	
	/**
	 * <p>Campo <code>SECUIDE</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "SECUIDE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String secuide;
	
	/**
	 * <p>Campo <code>DIDENTI</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "DIDENTI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String didenti;
	
	/**
	 * <p>Campo <code>TITULO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "TITULO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String titulo;
	
	/**
	 * <p>Campo <code>NOMBRE</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "NOMBRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nombre;
	
	/**
	 * <p>Campo <code>PRIAPE</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "PRIAPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String priape;
	
	/**
	 * <p>Campo <code>SEGAPE</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "SEGAPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String segape;
	
	/**
	 * <p>Campo <code>PETDOMI</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "PETDOMI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String petdomi;
	
	/**
	 * <p>Campo <code>DPETDOM</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "DPETDOM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String dpetdom;
	
	/**
	 * <p>Campo <code>TIPOPER</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "TIPOPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipoper;
	
	/**
	 * <p>Campo <code>DTIPOPE</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "DTIPOPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String dtipope;
	
	/**
	 * <p>Campo <code>FECRESI</code>, &iacute;ndice: <code>15</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 15, nombre = "FECRESI", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecresi;
	
	/**
	 * <p>Campo <code>TIPOVIA</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "TIPOVIA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String tipovia;
	
	/**
	 * <p>Campo <code>CALLE</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "CALLE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
	private String calle;
	
	/**
	 * <p>Campo <code>NUMEXT</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "NUMEXT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String numext;
	
	/**
	 * <p>Campo <code>TIPOVIV</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "TIPOVIV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String tipoviv;
	
	/**
	 * <p>Campo <code>NUMINT</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "NUMINT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String numint;
	
	/**
	 * <p>Campo <code>TIPOASE</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "TIPOASE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String tipoase;
	
	/**
	 * <p>Campo <code>COLONIA</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "COLONIA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String colonia;
	
	/**
	 * <p>Campo <code>ECALLE1</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "ECALLE1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ecalle1;
	
	/**
	 * <p>Campo <code>ECALLE2</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "ECALLE2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String ecalle2;
	
	/**
	 * <p>Campo <code>POBLACI</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "POBLACI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String poblaci;
	
	/**
	 * <p>Campo <code>CODPOST</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "CODPOST", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String codpost;
	
	/**
	 * <p>Campo <code>ESTADO</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 27, nombre = "ESTADO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String estado;
	
	/**
	 * <p>Campo <code>DESTADO</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 28, nombre = "DESTADO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String destado;
	
	/**
	 * <p>Campo <code>CODPAIS</code>, &iacute;ndice: <code>29</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 29, nombre = "CODPAIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String codpais;
	
	/**
	 * <p>Campo <code>DCODPAI</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 30, nombre = "DCODPAI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String dcodpai;
	
	/**
	 * <p>Campo <code>TIPTEL1</code>, &iacute;ndice: <code>31</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 31, nombre = "TIPTEL1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tiptel1;
	
	/**
	 * <p>Campo <code>PREFIJ1</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "PREFIJ1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String prefij1;
	
	/**
	 * <p>Campo <code>NUMTEL1</code>, &iacute;ndice: <code>33</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 33, nombre = "NUMTEL1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String numtel1;
	
	/**
	 * <p>Campo <code>EXTEN1</code>, &iacute;ndice: <code>34</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 34, nombre = "EXTEN1", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer exten1;
	
	/**
	 * <p>Campo <code>TIPTEL2</code>, &iacute;ndice: <code>35</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 35, nombre = "TIPTEL2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tiptel2;
	
	/**
	 * <p>Campo <code>PREFIJ2</code>, &iacute;ndice: <code>36</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 36, nombre = "PREFIJ2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String prefij2;
	
	/**
	 * <p>Campo <code>NUMTEL2</code>, &iacute;ndice: <code>37</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 37, nombre = "NUMTEL2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String numtel2;
	
	/**
	 * <p>Campo <code>EXTEN2</code>, &iacute;ndice: <code>38</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 38, nombre = "EXTEN2", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer exten2;
	
	/**
	 * <p>Campo <code>TIPTEL3</code>, &iacute;ndice: <code>39</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 39, nombre = "TIPTEL3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tiptel3;
	
	/**
	 * <p>Campo <code>PREFIJ3</code>, &iacute;ndice: <code>40</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 40, nombre = "PREFIJ3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String prefij3;
	
	/**
	 * <p>Campo <code>NUMTEL3</code>, &iacute;ndice: <code>41</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 41, nombre = "NUMTEL3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String numtel3;
	
	/**
	 * <p>Campo <code>EXTEN3</code>, &iacute;ndice: <code>42</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 42, nombre = "EXTEN3", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer exten3;
	
	/**
	 * <p>Campo <code>FEALTCL</code>, &iacute;ndice: <code>43</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 43, nombre = "FEALTCL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String fealtcl;
	
	/**
	 * <p>Campo <code>FEULBAJ</code>, &iacute;ndice: <code>44</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 44, nombre = "FEULBAJ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String feulbaj;
	
	/**
	 * <p>Campo <code>FEULMOD</code>, &iacute;ndice: <code>45</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 45, nombre = "FEULMOD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String feulmod;
	
	/**
	 * <p>Campo <code>NUMTER</code>, &iacute;ndice: <code>46</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 46, nombre = "NUMTER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String numter;
	
	/**
	 * <p>Campo <code>HORA</code>, &iacute;ndice: <code>47</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 47, nombre = "HORA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String hora;
	
	/**
	 * <p>Campo <code>USUARIO</code>, &iacute;ndice: <code>48</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 48, nombre = "USUARIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String usuario;
	
}
