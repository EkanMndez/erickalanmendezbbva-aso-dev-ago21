package com.bbva.mzic.accounts.dao.model.mtkdt056_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT056</code>
 * 
 * @see PeticionTransaccionMtkdt056_1
 * @see RespuestaTransaccionMtkdt056_1
 */
@Component
public class TransaccionMtkdt056_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt056_1, RespuestaTransaccionMtkdt056_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt056_1 invocar(PeticionTransaccionMtkdt056_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt056_1.class, RespuestaTransaccionMtkdt056_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt056_1 invocarCache(PeticionTransaccionMtkdt056_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt056_1.class, RespuestaTransaccionMtkdt056_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt056_1.class, "vaciearCache");
	}
}
