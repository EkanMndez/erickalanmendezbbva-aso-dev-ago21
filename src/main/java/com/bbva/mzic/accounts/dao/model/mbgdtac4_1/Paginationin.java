package com.bbva.mzic.accounts.dao.model.mbgdtac4_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Bean fila para el campo tabular <code>paginationIn</code>, utilizado por la clase <code>PeticionTransaccionMbgdtac4_1</code></p>
 * 
 * @see PeticionTransaccionMbgdtac4_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Paginationin {
	
	/**
	 * <p>Campo <code>paginationKey</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "paginationKey", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 9, signo = true)
	private String paginationkey;
	
	/**
	 * <p>Campo <code>paginationSize</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "paginationSize", tipo = TipoCampo.ENTERO, longitudMaxima = 9, signo = true)
	private Integer paginationsize;
	
}
