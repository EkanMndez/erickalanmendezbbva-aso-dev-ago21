package com.bbva.mzic.accounts.dao.model.bg40;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>BGMF40</code> de la transacci&oacute;n <code>BG40</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMF40")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGMF40 implements IFormat {

    /**
     * <p>
     * Campo <code>CLIENTE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CLIENTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliente;

}
