package com.bbva.mzic.accounts.dao.model.mbgdtchd_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>holder</code>, utilizado por la clase <code>Receiver</code>
 * </p>
 * 
 * @see Receiver
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Holder {

    /**
     * <p>
     * Campo <code>name</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "name", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 60, signo = true, obligatorio = true)
    private String name;

}
