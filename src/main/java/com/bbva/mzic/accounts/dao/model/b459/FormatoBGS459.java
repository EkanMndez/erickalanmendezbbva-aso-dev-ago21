package com.bbva.mzic.accounts.dao.model.b459;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGS459</code> de la transacci&oacute;n <code>B459</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGS459")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGS459 {

    /**
     * <p>
     * Campo <code>RESOPER</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "RESOPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String resoper;

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String cuenta;

    /**
     * <p>
     * Campo <code>PRICHEQ</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 3, nombre = "PRICHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer pricheq;

    /**
     * <p>
     * Campo <code>ULTCHEQ</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 4, nombre = "ULTCHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer ultcheq;

    /**
     * <p>
     * Campo <code>TOTOPER</code>, &iacute;ndice: <code>5</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 5, nombre = "TOTOPER", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer totoper;

    /**
     * <p>
     * Campo <code>TOTOPNK</code>, &iacute;ndice: <code>6</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 6, nombre = "TOTOPNK", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer totopnk;

}
