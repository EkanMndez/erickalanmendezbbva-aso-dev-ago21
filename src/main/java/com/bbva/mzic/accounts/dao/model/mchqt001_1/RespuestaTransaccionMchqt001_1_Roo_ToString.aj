package com.bbva.mzic.accounts.dao.model.mchqt001_1;

import java.lang.String;

privileged aspect RespuestaTransaccionMchqt001_1_Roo_ToString {
    
    public String RespuestaTransaccionMchqt001_1.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AplicacionAviso: ").append(getAplicacionAviso()).append(", ");
        sb.append("CodigoAviso: ").append(getCodigoAviso()).append(", ");
        sb.append("CodigoRetorno: ").append(getCodigoRetorno()).append(", ");
        sb.append("DescripcionAviso: ").append(getDescripcionAviso()).append(", ");
        sb.append("Incomeid: ").append(getIncomeid()).append(", ");
        sb.append("Messageresponse: ").append(getMessageresponse());
        return sb.toString();
    }
    
}
