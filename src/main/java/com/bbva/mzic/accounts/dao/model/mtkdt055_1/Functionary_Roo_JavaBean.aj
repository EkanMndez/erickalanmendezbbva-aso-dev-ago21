// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt055_1;

import java.lang.String;
import java.util.List;

privileged aspect Functionary_Roo_JavaBean {
    
    public String Functionary.getFunctionaryid() {
        return this.functionaryid;
    }
    
    public void Functionary.setFunctionaryid(String functionaryid) {
        this.functionaryid = functionaryid;
    }
    
    public String Functionary.getFunctionaryname() {
        return this.functionaryname;
    }
    
    public void Functionary.setFunctionaryname(String functionaryname) {
        this.functionaryname = functionaryname;
    }
    
    public int Functionary.getPldnationalstatus() {
        return this.pldnationalstatus;
    }
    
    public void Functionary.setPldnationalstatus(int pldnationalstatus) {
        this.pldnationalstatus = pldnationalstatus;
    }
    
    public int Functionary.getPldinternationalstatus() {
        return this.pldinternationalstatus;
    }
    
    public void Functionary.setPldinternationalstatus(int pldinternationalstatus) {
        this.pldinternationalstatus = pldinternationalstatus;
    }
    
    public int Functionary.getTotalaccountsbyfunctionary() {
        return this.totalaccountsbyfunctionary;
    }
    
    public void Functionary.setTotalaccountsbyfunctionary(int totalaccountsbyfunctionary) {
        this.totalaccountsbyfunctionary = totalaccountsbyfunctionary;
    }
    
    public List<Account> Functionary.getAccount() {
        return this.account;
    }
    
    public void Functionary.setAccount(List<Account> account) {
        this.account = account;
    }
    
}
