package com.bbva.mzic.accounts.dao.model.mpbdt003_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Bean fila para el campo tabular <code>variableEvidencia</code>, utilizado por la clase <code>PeticionTransaccionMpbdt003_1</code></p>
 * 
 * @see PeticionTransaccionMpbdt003_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Variableevidencia {
	
	/**
	 * <p>Campo <code>Campo</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "Campo", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String campo;
	
	/**
	 * <p>Campo <code>Valor</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "Valor", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10000, signo = true, obligatorio = true)
	private String valor;
	
}
