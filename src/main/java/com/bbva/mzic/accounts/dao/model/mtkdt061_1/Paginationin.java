package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>paginationIn</code>, utilizado por la clase <code>PeticionTransaccionMtkdt061_1</code></p>
 * 
 * @see PeticionTransaccionMtkdt061_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Paginationin {
	
	/**
	 * <p>Campo <code>paginationKey</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "paginationKey", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String paginationkey;
	
	/**
	 * <p>Campo <code>pageSize</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "pageSize", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String pagesize;
	
}
