package com.bbva.mzic.accounts.dao;

import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheck;
import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheckList;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;

public interface IAccountsDaoV0 {

    DtoIntAggregatedAvailableBalance getBalanceBGL4(DtoIntFilterAccount filterAccount);

    DtoIntDigitalCheckList listDigitalCheckMBGDTCHD(DtoIntFilterAccount filterAccount);

    DtoIntDigitalCheck getDigitalCheckWYRF(DtoIntFilterAccount filterAccount);
}
