// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mbgdtac2_1;

import java.lang.String;

privileged aspect RespuestaTransaccionMbgdtac2_1_Roo_JavaBean {

    public String RespuestaTransaccionMbgdtac2_1.getCodigoAviso() {
        return this.codigoAviso;
    }

    public void RespuestaTransaccionMbgdtac2_1.setCodigoAviso(String codigoAviso) {
        this.codigoAviso = codigoAviso;
    }

    public String RespuestaTransaccionMbgdtac2_1.getDescripcionAviso() {
        return this.descripcionAviso;
    }

    public void RespuestaTransaccionMbgdtac2_1.setDescripcionAviso(String descripcionAviso) {
        this.descripcionAviso = descripcionAviso;
    }

    public String RespuestaTransaccionMbgdtac2_1.getAplicacionAviso() {
        return this.aplicacionAviso;
    }

    public void RespuestaTransaccionMbgdtac2_1.setAplicacionAviso(String aplicacionAviso) {
        this.aplicacionAviso = aplicacionAviso;
    }

    public String RespuestaTransaccionMbgdtac2_1.getCodigoRetorno() {
        return this.codigoRetorno;
    }

    public void RespuestaTransaccionMbgdtac2_1.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public Integer RespuestaTransaccionMbgdtac2_1.getUpdatecode() {
        return this.updatecode;
    }

    public void RespuestaTransaccionMbgdtac2_1.setUpdatecode(Integer updatecode) {
        this.updatecode = updatecode;
    }

    public String RespuestaTransaccionMbgdtac2_1.getLegendupdate() {
        return this.legendupdate;
    }

    public void RespuestaTransaccionMbgdtac2_1.setLegendupdate(String legendupdate) {
        this.legendupdate = legendupdate;
    }

}
