package com.bbva.mzic.accounts.dao.model.pejw;

import java.io.Serializable;

privileged aspect FormatoPEM0JWS_Roo_Serializable {
    
    declare parents: FormatoPEM0JWS implements Serializable;
    
    private static final long FormatoPEM0JWS.serialVersionUID = 1L;
    
}
