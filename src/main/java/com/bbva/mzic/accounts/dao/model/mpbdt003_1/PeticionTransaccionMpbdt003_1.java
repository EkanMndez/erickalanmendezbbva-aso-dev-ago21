package com.bbva.mzic.accounts.dao.model.mpbdt003_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MPBDT003</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMpbdt003_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMpbdt003_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MPBDT003-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MPBDT003&quot; application=&quot;MPBD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;numeroCliente&quot; type=&quot;String&quot;
 * size=&quot;30&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;tipoEvidencia&quot; type=&quot;String&quot;
 * size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;plantillaRuc&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;variableEvidencia&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;Campo&quot; type=&quot;String&quot; size=&quot;40&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;Valor&quot; type=&quot;String&quot; size=&quot;10000&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;codigo&quot; type=&quot;String&quot; size=&quot;12&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;descripcion&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;operationId&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion encargada de realizar el respaldo de&amp;#xD;
 * evidencias
 * &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMpbdt003_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MPBDT003", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx", respuesta = RespuestaTransaccionMpbdt003_1.class, atributos = {
		@Atributo(nombre = "country", valor = "MX") })
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMpbdt003_1 {

	/**
	 * <p>
	 * Campo <code>numeroCliente</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "numeroCliente", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
	private String numerocliente;

	/**
	 * <p>
	 * Campo <code>tipoEvidencia</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "tipoEvidencia", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String tipoevidencia;

	/**
	 * <p>
	 * Campo <code>plantillaRuc</code>, &iacute;ndice: <code>3</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "plantillaRuc", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String plantillaruc;

	/**
	 * <p>
	 * Campo <code>variableEvidencia</code>, &iacute;ndice: <code>4</code>, tipo:
	 * <code>TABULAR</code>
	 */
	@Campo(indice = 4, nombre = "variableEvidencia", tipo = TipoCampo.TABULAR)
	private List<Variableevidencia> variableevidencia;

}
