package com.bbva.mzic.accounts.dao.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntBalance;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4TS;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4US;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4VS;
import com.bbva.mzic.accounts.dao.model.bgl4.RespuestaTransaccionBgl4;
import com.bbva.mzic.serviceutils.rm.utils.tx.FilterResponseHost;

public class OutputBGL4Mapper implements IMapper<RespuestaTransaccionBgl4, DtoIntAggregatedAvailableBalance> {

	private static final String CURRENCY_MXN = "MXN";
	private static final String CURRENCY_USD = "USD";
	private static final String CURRENCY_EUR = "EUR";

	@Override
	public DtoIntAggregatedAvailableBalance map(RespuestaTransaccionBgl4 response) {
		if (response == null) {
			return null;
		}
		final List<Object> formats = response.getCuerpo().getPartes().stream()
				.filter(cs -> cs != null && cs.getClass().equals(CopySalida.class)).map(cs -> (CopySalida) cs).map(CopySalida::getCopy)
				.collect(Collectors.toList());

		final FormatoBGML4TS formatoBGML4TS = FilterResponseHost.findFirstFormat(formats, FormatoBGML4TS.class);
		final FormatoBGML4VS formatoBGML4VS = FilterResponseHost.findFirstFormat(formats, FormatoBGML4VS.class);
		final FormatoBGML4US formatoBGML4US = FilterResponseHost.findFirstFormat(formats, FormatoBGML4US.class);

		final DtoIntAggregatedAvailableBalance aggregatedAvailableBalance = new DtoIntAggregatedAvailableBalance();
		final List<DtoIntBalance> currentBalances = new ArrayList<>();
		final List<DtoIntBalance> postedBalances = new ArrayList<>();
		final List<DtoIntBalance> pendingBalances = new ArrayList<>();
		DtoIntBalance balance = new DtoIntBalance();

		//balance.setAmount(formatoBGML4TS.getTotvimx());
		balance.setCurrency(CURRENCY_MXN);
		currentBalances.add(balance);

		balance = new DtoIntBalance();
		//balance.setAmount(formatoBGML4TS.getTotvius());
		balance.setCurrency(CURRENCY_USD);
		currentBalances.add(balance);

		balance = new DtoIntBalance();
		//balance.setAmount(formatoBGML4TS.getTotvieu());
		balance.setCurrency(CURRENCY_EUR);
		currentBalances.add(balance);

		aggregatedAvailableBalance.setCurrentBalances(currentBalances);

		balance = new DtoIntBalance();
		//balance.setAmount(formatoBGML4VS.getTopvimx());
		balance.setCurrency(CURRENCY_MXN);
		postedBalances.add(balance);

		balance = new DtoIntBalance();
		//balance.setAmount(formatoBGML4VS.getTopvius());
		balance.setCurrency(CURRENCY_USD);
		postedBalances.add(balance);

		balance = new DtoIntBalance();
		//balance.setAmount(formatoBGML4VS.getTopvieu());
		balance.setCurrency(CURRENCY_EUR);
		postedBalances.add(balance);

		aggregatedAvailableBalance.setPostedBalances(postedBalances);

		balance = new DtoIntBalance();
		balance.setAmount(formatoBGML4US.getTorvimx());
		balance.setCurrency(CURRENCY_MXN);
		pendingBalances.add(balance);

		balance = new DtoIntBalance();
		balance.setAmount(formatoBGML4US.getTorvius());
		balance.setCurrency(CURRENCY_USD);
		pendingBalances.add(balance);

		balance = new DtoIntBalance();
		balance.setAmount(formatoBGML4US.getTorvieu());
		balance.setCurrency(CURRENCY_EUR);
		pendingBalances.add(balance);

		aggregatedAvailableBalance.setPendingBalances(pendingBalances);

		return new DtoIntAggregatedAvailableBalance();
	}

}
