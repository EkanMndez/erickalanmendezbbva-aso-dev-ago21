package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>MTKDT061</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt061_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt061_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT061-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MTKDT061&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;reportId&quot; type=&quot;String&quot; size=&quot;31&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;movementType&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;statusMovement&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;initialDateOperation&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;finalDateOperation&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;21&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;initialAmount&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;finalAmount&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;paginationIn&quot; order=&quot;10&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;paginationKey&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;pageSize&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;reportListTransaction&quot; order=&quot;1&quot;&gt;
 * &lt;group name=&quot;contentReport&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;reportDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;totalAmountReport&quot; type=&quot;String&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;totalAmountCurrency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;agruperOperations&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;operativeId&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;operativeName&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;operationSubtotal&quot; type=&quot;String&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;currencySubtotal&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;operationQuantity&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;dataReport&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ticketId&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;branchName&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;bankingId&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;bankingName&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;accountStatusId&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;accountStatus&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;customerId&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;customerName&quot; type=&quot;String&quot; size=&quot;150&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;16&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;currencyAmount&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;creationDate&quot; type=&quot;String&quot; size=&quot;19&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;applicationDate&quot; type=&quot;String&quot; size=&quot;19&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;statusMovementId&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;16&quot; name=&quot;statusMovementDescription&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;17&quot; name=&quot;movementType&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;18&quot; name=&quot;movementDescription&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;20&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;21&quot; name=&quot;statusACK&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;22&quot; name=&quot;foreignId&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;23&quot; name=&quot;statusMT202&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;24&quot; name=&quot;accountingStatus&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;25&quot; name=&quot;ticketHoldId&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;26&quot; name=&quot;proccessInformation&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;functionary&quot; order=&quot;19&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;functionaryId&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;functionaryName&quot; type=&quot;String&quot; size=&quot;90&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;functionaryType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;functionaryDescription&quot; type=&quot;String&quot; size=&quot;90&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;paginationOut&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;nextKey&quot; type=&quot;Long&quot; size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;hasMoreData&quot; type=&quot;Long&quot; size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Metodo utilizado para obtener reportes de movimientos en Ticket APX.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt061_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MTKDT061",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMtkdt061_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt061_1 {
		
		/**
	 * <p>Campo <code>reportId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "reportId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 31, signo = true, obligatorio = true)
	private String reportid;
	
	/**
	 * <p>Campo <code>movementType</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "movementType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String movementtype;
	
	/**
	 * <p>Campo <code>statusMovement</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "statusMovement", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String statusmovement;
	
	/**
	 * <p>Campo <code>initialDateOperation</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "initialDateOperation", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String initialdateoperation;
	
	/**
	 * <p>Campo <code>finalDateOperation</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "finalDateOperation", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String finaldateoperation;
	
	/**
	 * <p>Campo <code>userId</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 21, signo = true)
	private String userid;
	
	/**
	 * <p>Campo <code>initialAmount</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 7, nombre = "initialAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 16, signo = true)
	private BigDecimal initialamount;
	
	/**
	 * <p>Campo <code>finalAmount</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 8, nombre = "finalAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 16, signo = true)
	private BigDecimal finalamount;
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
	private String currency;
	
	/**
	 * <p>Campo <code>paginationIn</code>, &iacute;ndice: <code>10</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 10, nombre = "paginationIn", tipo = TipoCampo.TABULAR)
	private List<Paginationin> paginationin;
	
}
