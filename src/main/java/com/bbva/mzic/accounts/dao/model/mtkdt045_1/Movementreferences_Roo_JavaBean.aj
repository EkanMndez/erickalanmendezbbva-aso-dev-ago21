// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt045_1;

import java.lang.String;

privileged aspect Movementreferences_Roo_JavaBean {
    
    public String Movementreferences.getFirstdescription() {
        return this.firstdescription;
    }
    
    public void Movementreferences.setFirstdescription(String firstdescription) {
        this.firstdescription = firstdescription;
    }
    
    public String Movementreferences.getSeconddescription() {
        return this.seconddescription;
    }
    
    public void Movementreferences.setSeconddescription(String seconddescription) {
        this.seconddescription = seconddescription;
    }
    
    public String Movementreferences.getThirddescription() {
        return this.thirddescription;
    }
    
    public void Movementreferences.setThirddescription(String thirddescription) {
        this.thirddescription = thirddescription;
    }
    
    public String Movementreferences.getFourthdescription() {
        return this.fourthdescription;
    }
    
    public void Movementreferences.setFourthdescription(String fourthdescription) {
        this.fourthdescription = fourthdescription;
    }
    
}
