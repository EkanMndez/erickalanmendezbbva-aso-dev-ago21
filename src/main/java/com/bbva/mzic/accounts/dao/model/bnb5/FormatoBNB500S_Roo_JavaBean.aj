// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.bnb5;

import java.lang.String;

privileged aspect FormatoBNB500S_Roo_JavaBean {
    
    public String FormatoBNB500S.getProduc() {
        return this.produc;
    }
    
    public void FormatoBNB500S.setProduc(String produc) {
        this.produc = produc;
    }
    
    public String FormatoBNB500S.getSubprod() {
        return this.subprod;
    }
    
    public void FormatoBNB500S.setSubprod(String subprod) {
        this.subprod = subprod;
    }
    
    public String FormatoBNB500S.getDsubpro() {
        return this.dsubpro;
    }
    
    public void FormatoBNB500S.setDsubpro(String dsubpro) {
        this.dsubpro = dsubpro;
    }
    
    public String FormatoBNB500S.getDivisa() {
        return this.divisa;
    }
    
    public void FormatoBNB500S.setDivisa(String divisa) {
        this.divisa = divisa;
    }
    
    public String FormatoBNB500S.getRegcta() {
        return this.regcta;
    }
    
    public void FormatoBNB500S.setRegcta(String regcta) {
        this.regcta = regcta;
    }
    
    public String FormatoBNB500S.getDregcta() {
        return this.dregcta;
    }
    
    public void FormatoBNB500S.setDregcta(String dregcta) {
        this.dregcta = dregcta;
    }
    
    public String FormatoBNB500S.getRegfis() {
        return this.regfis;
    }
    
    public void FormatoBNB500S.setRegfis(String regfis) {
        this.regfis = regfis;
    }
    
    public String FormatoBNB500S.getDregfis() {
        return this.dregfis;
    }
    
    public void FormatoBNB500S.setDregfis(String dregfis) {
        this.dregfis = dregfis;
    }
    
    public String FormatoBNB500S.getTarifa() {
        return this.tarifa;
    }
    
    public void FormatoBNB500S.setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }
    
    public String FormatoBNB500S.getDtarifa() {
        return this.dtarifa;
    }
    
    public void FormatoBNB500S.setDtarifa(String dtarifa) {
        this.dtarifa = dtarifa;
    }
    
    public String FormatoBNB500S.getCtaaso() {
        return this.ctaaso;
    }
    
    public void FormatoBNB500S.setCtaaso(String ctaaso) {
        this.ctaaso = ctaaso;
    }
    
    public String FormatoBNB500S.getIndres() {
        return this.indres;
    }
    
    public void FormatoBNB500S.setIndres(String indres) {
        this.indres = indres;
    }
    
}
