package com.bbva.mzic.accounts.dao.model.pej0;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Formato de datos <code>PEM0J0S</code> de la transacci&oacute;n <code>PEJ0</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEM0J0S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEM0J0S {

    /**
     * <p>
     * Campo <code>CAMPO01</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "CAMPO01", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
    private Integer campo01;

    /**
     * <p>
     * Campo <code>CAMPO02</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CAMPO02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String campo02;

    /**
     * <p>
     * Campo <code>CAMPO03</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "CAMPO03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String campo03;

    /**
     * <p>
     * Campo <code>CAMPO04</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "CAMPO04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String campo04;

    /**
     * <p>
     * Campo <code>CAMPO05</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "CAMPO05", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String campo05;

    /**
     * <p>
     * Campo <code>CAMPO06</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "CAMPO06", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String campo06;

    /**
     * <p>
     * Campo <code>CAMPO07</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "CAMPO07", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
    private String campo07;

    /**
     * <p>
     * Campo <code>CAMPO08</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "CAMPO08", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String campo08;

    /**
     * <p>
     * Campo <code>CAMPO09</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "CAMPO09", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String campo09;

    /**
     * <p>
     * Campo <code>CAMPO10</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "CAMPO10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String campo10;

    /**
     * <p>
     * Campo <code>CAMPO11</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "CAMPO11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String campo11;

    /**
     * <p>
     * Campo <code>CAMPO12</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "CAMPO12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String campo12;

    /**
     * <p>
     * Campo <code>CAMPO13</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "CAMPO13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String campo13;

    /**
     * <p>
     * Campo <code>CAMPO14</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "CAMPO14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String campo14;

    /**
     * <p>
     * Campo <code>CAMPO15</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "CAMPO15", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String campo15;

    /**
     * <p>
     * Campo <code>CAMPO16</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "CAMPO16", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String campo16;

    /**
     * <p>
     * Campo <code>CAMPO17</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "CAMPO17", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String campo17;

    /**
     * <p>
     * Campo <code>CAMPO18</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "CAMPO18", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String campo18;

    /**
     * <p>
     * Campo <code>CAMPO19</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "CAMPO19", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String campo19;

    /**
     * <p>
     * Campo <code>CAMPO20</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "CAMPO20", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String campo20;

    /**
     * <p>
     * Campo <code>CAMPO21</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 21, nombre = "CAMPO21", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String campo21;

    /**
     * <p>
     * Campo <code>CAMPO22</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 22, nombre = "CAMPO22", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
    private String campo22;

    /**
     * <p>
     * Campo <code>CAMPO23</code>, &iacute;ndice: <code>23</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 23, nombre = "CAMPO23", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
    private Integer campo23;

}
