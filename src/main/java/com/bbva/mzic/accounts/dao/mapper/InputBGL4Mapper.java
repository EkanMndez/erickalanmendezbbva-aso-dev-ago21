package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4E;
import com.bbva.mzic.accounts.dao.model.bgl4.PeticionTransaccionBgl4;

public class InputBGL4Mapper implements IMapper<DtoIntFilterAccount, PeticionTransaccionBgl4> {

	@Override
	public PeticionTransaccionBgl4 map(DtoIntFilterAccount dtoIntFilterAccount) {

		final PeticionTransaccionBgl4 peticionTransaccionBgl4 = new PeticionTransaccionBgl4();

		final FormatoBGML4E formato = new FormatoBGML4E();

		if (dtoIntFilterAccount.getAccountFamilyId() == null) {
			formato.setFamprod("VI");
		} else if (dtoIntFilterAccount.getAccountFamilyId().equals("COMMON")) {
			formato.setFamprod("VI");
		}
		formato.setClienpu(dtoIntFilterAccount.getClientId());

		// construccion del objeto para llamar a host
		peticionTransaccionBgl4.getCuerpo().getPartes().add(formato);

		return peticionTransaccionBgl4;

	}

}
