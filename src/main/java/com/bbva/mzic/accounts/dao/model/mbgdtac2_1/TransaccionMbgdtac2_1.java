package com.bbva.mzic.accounts.dao.model.mbgdtac2_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDTAC2</code>
 *
 * @see PeticionTransaccionMbgdtac2_1
 * @see RespuestaTransaccionMbgdtac2_1
 */
@Component
public class TransaccionMbgdtac2_1 implements InvocadorTransaccion<PeticionTransaccionMbgdtac2_1, RespuestaTransaccionMbgdtac2_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdtac2_1 invocar(PeticionTransaccionMbgdtac2_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtac2_1.class, RespuestaTransaccionMbgdtac2_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdtac2_1 invocarCache(PeticionTransaccionMbgdtac2_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtac2_1.class, RespuestaTransaccionMbgdtac2_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdtac2_1.class, "vaciearCache");
	}
}
