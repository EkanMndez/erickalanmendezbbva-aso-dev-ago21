package com.bbva.mzic.accounts.dao.model.mbgdtcon_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>paginationOutDTO</code>, utilizado por
 * la clase <code>RespuestaTransaccionMbgdtcon_1</code>
 * </p>
 *
 * @see RespuestaTransaccionMbgdtcon_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Paginationoutdto {

	/**
	 * <p>
	 * Campo <code>hasMoreData</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ENTERO</code>
	 */
	@Campo(indice = 1,
			nombre = "hasMoreData",
			tipo = TipoCampo.ENTERO,
			longitudMaxima = 1,
			signo = true)
	private Integer hasmoredata;

	/**
	 * <p>
	 * Campo <code>paginationKey</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2,
			nombre = "paginationKey",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 12,
			signo = true)
	private String paginationkey;

}
