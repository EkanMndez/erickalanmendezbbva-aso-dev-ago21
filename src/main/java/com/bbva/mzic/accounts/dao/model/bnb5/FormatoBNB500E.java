package com.bbva.mzic.accounts.dao.model.bnb5;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>BNB500E</code> de la transacci&oacute;n <code>BNB5</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BNB500E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBNB500E {

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String cuenta;

    /**
     * <p>
     * Campo <code>CLIENTE</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CLIENTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliente;

}
