package com.bbva.mzic.accounts.dao.model.mbgdtac1_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>participantData</code>, utilizado por la clase <code>PeticionTransaccionMbgdtac1_1</code></p>
 * 
 * @see PeticionTransaccionMbgdtac1_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Participantdata {
	
	/**
	 * <p>Campo <code>participeClientData</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "participeClientData", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
	private String participeclientdata;
	
	/**
	 * <p>Campo <code>typeParticipationData</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "typeParticipationData", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
	private String typeparticipationdata;
	
	/**
	 * <p>Campo <code>categoryData</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "categoryData", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true, obligatorio = true)
	private String categorydata;
	
	/**
	 * <p>Campo <code>combinationPowersData</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "combinationPowersData", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String combinationpowersdata;
	
	/**
	 * <p>Campo <code>limitData</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "limitData", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String limitdata;
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String currency;
	
}
