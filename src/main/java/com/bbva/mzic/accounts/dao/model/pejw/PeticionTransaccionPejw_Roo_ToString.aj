package com.bbva.mzic.accounts.dao.model.pejw;

import java.lang.String;

privileged aspect PeticionTransaccionPejw_Roo_ToString {
    
    public String PeticionTransaccionPejw.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cuerpo: ").append(getCuerpo());
        return sb.toString();
    }
    
}
