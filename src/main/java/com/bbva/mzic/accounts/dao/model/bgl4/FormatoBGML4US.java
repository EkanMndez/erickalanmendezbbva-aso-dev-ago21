package com.bbva.mzic.accounts.dao.model.bgl4;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGML4US</code> de la transacci&oacute;n <code>BGL4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGML4US")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGML4US implements IFormat {

    /**
     * <p>
     * Campo <code>TORVIMX</code>, &iacute;ndice: <code>1</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 1, nombre = "TORVIMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torvimx;

    /**
     * <p>
     * Campo <code>TORVIUS</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "TORVIUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torvius;

    /**
     * <p>
     * Campo <code>TORVIEU</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 3, nombre = "TORVIEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torvieu;

    /**
     * <p>
     * Campo <code>TORINMX</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 4, nombre = "TORINMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torinmx;

    /**
     * <p>
     * Campo <code>TORINUS</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 5, nombre = "TORINUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torinus;

    /**
     * <p>
     * Campo <code>TORINEU</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 6, nombre = "TORINEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torineu;

    /**
     * <p>
     * Campo <code>TORPKMX</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 7, nombre = "TORPKMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torpkmx;

    /**
     * <p>
     * Campo <code>TORPKUS</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 8, nombre = "TORPKUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torpkus;

    /**
     * <p>
     * Campo <code>TORPKEU</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 9, nombre = "TORPKEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal torpkeu;

}
