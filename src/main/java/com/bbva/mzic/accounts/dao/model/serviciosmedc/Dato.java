
package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for Dato complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Dato"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nombreDato" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Dato", propOrder = {"nombreDato", "valor"})
public class Dato {

    @XmlElement(required = true, nillable = true)
    protected String nombreDato;
    @XmlElement(required = true, nillable = true)
    protected String valor;

    /**
     * Gets the value of the nombreDato property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNombreDato() {
        return nombreDato;
    }

    /**
     * Sets the value of the nombreDato property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setNombreDato(String value) {
        this.nombreDato = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setValor(String value) {
        this.valor = value;
    }

}
