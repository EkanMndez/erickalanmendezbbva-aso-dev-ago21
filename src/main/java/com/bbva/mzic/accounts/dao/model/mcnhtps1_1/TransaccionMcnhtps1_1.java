package com.bbva.mzic.accounts.dao.model.mcnhtps1_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCNHTPS1</code>
 * 
 * @see PeticionTransaccionMcnhtps1_1
 * @see RespuestaTransaccionMcnhtps1_1
 */
@Component
public class TransaccionMcnhtps1_1 implements InvocadorTransaccion<PeticionTransaccionMcnhtps1_1, RespuestaTransaccionMcnhtps1_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMcnhtps1_1 invocar(PeticionTransaccionMcnhtps1_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMcnhtps1_1.class, RespuestaTransaccionMcnhtps1_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMcnhtps1_1 invocarCache(PeticionTransaccionMcnhtps1_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMcnhtps1_1.class, RespuestaTransaccionMcnhtps1_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMcnhtps1_1.class, "vaciearCache");
	}
}
