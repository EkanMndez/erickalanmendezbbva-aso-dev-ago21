package com.bbva.mzic.accounts.dao.model.pejw;

import java.lang.String;

privileged aspect FormatoPEM0JWS_Roo_ToString {
    
    public String FormatoPEM0JWS.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Contrac: ").append(getContrac()).append(", ");
        sb.append("Produid: ").append(getProduid()).append(", ");
        sb.append("Produna: ").append(getProduna()).append(", ");
        sb.append("Statusi: ").append(getStatusi()).append(", ");
        sb.append("Statusn: ").append(getStatusn());
        return sb.toString();
    }
    
}
