package com.bbva.mzic.accounts.dao.model.apx.mbgft003_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MBGDT003</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMbgdt003_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMbgdt003_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MBGDT003-layout-V4.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MBGDT003&quot; application=&quot;MBGD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;group name=&quot;paginationIn&quot; order=&quot;1&quot;&gt;
 * &lt;!--la primera vez = 0, subsecuente numero de movimiento previo de nodo PaginationOut--&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;paginationKey&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationSize&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- default 20 --&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;saldoAntMov&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;criteria&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;order&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot; /&gt;	&lt;!-- D/A  --&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;contractNumber&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;&lt;!-- YYYY-MM-DD --&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;operationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- YYYY-MM-DD --&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;fromOperationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- YYYY-MM-DD --&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;toOperationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- YYYY-MM-DD --&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;localAmount&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;fromLocalAmount&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;toLocalAmount&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;fromMovementNumber&quot; type=&quot;Long&quot; size=&quot;13&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;toMovementNumber&quot; type=&quot;Long&quot; size=&quot;13&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;moneyFlowId&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- I=Income/E=expense --&gt;
 * 
 * 
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;paginationOut&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;paginationKey&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;hasMoreData&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;saldoAntMov&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;records&quot; order=&quot;2&quot;&gt;
 * &lt;!-- se quitaron todos los mandatorios porque cuando es vacio lanza error ejecucion--&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;Long&quot; size=&quot;13&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- numero movimiento --&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;localAmount&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;localAmountCurrency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;	&lt;!-- divisa --&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;moneyFlowId&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- I=Income/E=expense --&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;concept&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;transactionTypeId&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- CASH_WITHDRAWAL/CASH_INCOME --&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;transactionTypeName&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;transactionTypeInternalCode&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;transactionTypeInternalCodeName&quot; type=&quot;String&quot; size=&quot;34&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;operationDate&quot; type=&quot;String&quot; size=&quot;29&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- Fecha operacion mas hora operacion --&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;valuationDate&quot; type=&quot;String&quot; size=&quot;29&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;accountedDate&quot; type=&quot;String&quot; size=&quot;29&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;financingTypeId&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- NON_FINANCING --&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;statusId&quot; type=&quot;String&quot; size=&quot;7&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- SETTLED --&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;contractNumberTypeId&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- LIC --&gt;
 * &lt;parameter order=&quot;16&quot; name=&quot;contractProductId&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- ACCOUNTS --&gt;
 * &lt;parameter order=&quot;17&quot; name=&quot;contractProductName&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- ACCOUNTS --&gt;
 * &lt;parameter order=&quot;18&quot; name=&quot;contractAlias&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;&lt;!-- pendiente --&gt;
 * &lt;parameter order=&quot;19&quot; name=&quot;additionalInformationReference&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;20&quot; name=&quot;additionalInformationAdditionalData&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;21&quot; name=&quot;operationBalance&quot; type=&quot;Double&quot; size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * 
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Consulta UMO&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMbgdt003_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MBGDT003", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMbgdt003_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMbgdt003_1 {

    /**
     * <p>
     * Campo <code>paginationIn</code>, &iacute;ndice: <code>1</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 1, nombre = "paginationIn", tipo = TipoCampo.TABULAR)
    private List<Paginationin> paginationin;

    /**
     * <p>
     * Campo <code>criteria</code>, &iacute;ndice: <code>2</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 2, nombre = "criteria", tipo = TipoCampo.TABULAR)
    private List<Criteria> criteria;

}
