package com.bbva.mzic.accounts.dao.model.apx.mtkdt019_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MTKDT019</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt019_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt019_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MTKDT019.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT019&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountNumber&quot; type=&quot;String&quot;
 * size=&quot;6&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;holderFirstName&quot; type=&quot;String&quot;
 * size=&quot;60&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;holderLastName&quot; type=&quot;String&quot;
 * size=&quot;40&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;holderSecondLastName&quot; type=&quot;String&quot;
 * size=&quot;40&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;currencyId&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;paginationIn&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;pageSize&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;account&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountNumber&quot; type=&quot;String&quot;
 * size=&quot;6&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;holderFirstName&quot; type=&quot;String&quot;
 * size=&quot;60&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;holderLastName&quot; type=&quot;String&quot;
 * size=&quot;40&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;holderSecondLastName&quot; type=&quot;String&quot;
 * size=&quot;40&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;customerId&quot; type=&quot;String&quot; size=&quot;8&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;holderMiddleName&quot; type=&quot;String&quot;
 * size=&quot;16&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;suffix&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 *
 *
 * &lt;group name=&quot;formats&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;12&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;numberType&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;accountFamily&quot; order=&quot;9&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;40&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;subType&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;subproduct&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;tittle&quot; order=&quot;10&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 *
 * &lt;group name=&quot;status&quot; order=&quot;11&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 *
 *
 * &lt;group name=&quot;participantType&quot; order=&quot;12&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;60&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 *
 * &lt;group name=&quot;numberType&quot; order=&quot;13&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 *
 * &lt;group name=&quot;accountType&quot; order=&quot;14&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;paginationOut&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;paginationKey&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;hasMoreData&quot; type=&quot;String&quot; size=&quot;1&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion encargada de listar cuentas CED&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMtkdt019_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MTKDT019", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx", respuesta = RespuestaTransaccionMtkdt019_1.class, atributos = {
		@Atributo(nombre = "country", valor = "MX") })
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt019_1 {

	/**
	 * <p>
	 * Campo <code>accountNumber</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "accountNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true)
	private String accountnumber;

	/**
	 * <p>
	 * Campo <code>holderFirstName</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "holderFirstName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 60, signo = true)
	private String holderfirstname;

	/**
	 * <p>
	 * Campo <code>holderLastName</code>, &iacute;ndice: <code>3</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "holderLastName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
	private String holderlastname;

	/**
	 * <p>
	 * Campo <code>holderSecondLastName</code>, &iacute;ndice: <code>4</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "holderSecondLastName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
	private String holdersecondlastname;

	/**
	 * <p>
	 * Campo <code>currencyId</code>, &iacute;ndice: <code>5</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "currencyId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
	private String currencyid;

	/**
	 * <p>
	 * Campo <code>paginationIn</code>, &iacute;ndice: <code>6</code>, tipo:
	 * <code>TABULAR</code>
	 */
	@Campo(indice = 6, nombre = "paginationIn", tipo = TipoCampo.TABULAR)
	private List<Paginationin> paginationin;

}
