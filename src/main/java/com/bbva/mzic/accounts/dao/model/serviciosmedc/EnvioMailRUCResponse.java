
package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="envioCorreo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EXIT_STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ERR_DESC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"envioCorreo", "exitstatus", "errdesc"})
@XmlRootElement(name = "envioMailRUCResponse")
public class EnvioMailRUCResponse {

    @XmlElement(required = true, nillable = true)
    protected String envioCorreo;
    @XmlElement(name = "EXIT_STATUS", required = true, nillable = true)
    protected String exitstatus;
    @XmlElement(name = "ERR_DESC", required = true, nillable = true)
    protected String errdesc;

    /**
     * Gets the value of the envioCorreo property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEnvioCorreo() {
        return envioCorreo;
    }

    /**
     * Sets the value of the envioCorreo property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setEnvioCorreo(String value) {
        this.envioCorreo = value;
    }

    /**
     * Gets the value of the exitstatus property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEXITSTATUS() {
        return exitstatus;
    }

    /**
     * Sets the value of the exitstatus property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setEXITSTATUS(String value) {
        this.exitstatus = value;
    }

    /**
     * Gets the value of the errdesc property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getERRDESC() {
        return errdesc;
    }

    /**
     * Sets the value of the errdesc property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setERRDESC(String value) {
        this.errdesc = value;
    }

}
