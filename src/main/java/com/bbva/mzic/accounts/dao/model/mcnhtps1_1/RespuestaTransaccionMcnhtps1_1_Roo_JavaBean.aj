// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mcnhtps1_1;

import java.lang.String;

privileged aspect RespuestaTransaccionMcnhtps1_1_Roo_JavaBean {
    
    public String RespuestaTransaccionMcnhtps1_1.getCodigoAviso() {
        return this.codigoAviso;
    }
    
    public void RespuestaTransaccionMcnhtps1_1.setCodigoAviso(String codigoAviso) {
        this.codigoAviso = codigoAviso;
    }
    
    public String RespuestaTransaccionMcnhtps1_1.getDescripcionAviso() {
        return this.descripcionAviso;
    }
    
    public void RespuestaTransaccionMcnhtps1_1.setDescripcionAviso(String descripcionAviso) {
        this.descripcionAviso = descripcionAviso;
    }
    
    public String RespuestaTransaccionMcnhtps1_1.getAplicacionAviso() {
        return this.aplicacionAviso;
    }
    
    public void RespuestaTransaccionMcnhtps1_1.setAplicacionAviso(String aplicacionAviso) {
        this.aplicacionAviso = aplicacionAviso;
    }
    
    public String RespuestaTransaccionMcnhtps1_1.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    public void RespuestaTransaccionMcnhtps1_1.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }
    
    public String RespuestaTransaccionMcnhtps1_1.getResult() {
        return this.result;
    }
    
    public void RespuestaTransaccionMcnhtps1_1.setResult(String result) {
        this.result = result;
    }
    
}
