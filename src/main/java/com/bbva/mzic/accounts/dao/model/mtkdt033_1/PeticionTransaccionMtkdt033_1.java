package com.bbva.mzic.accounts.dao.model.mtkdt033_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>MTKDT033</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt033_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt033_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MTKDT033-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MTKDT033&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;functionaryId&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;bankId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;bankName&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;group name=&quot;deposit&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;14&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;mainReference&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;creationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;applicationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;typeId&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;documentId&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;group name=&quot;movementReferences&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;firstDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;secondDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;thirdDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;fourthDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ticketId&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion utilizada para la creacion de un deposito para cuentas en dolares &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt033_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MTKDT033", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMtkdt033_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt033_1 {

    /**
     * <p>
     * Campo <code>accountId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
    private String accountid;

    /**
     * <p>
     * Campo <code>functionaryId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "functionaryId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String functionaryid;

    /**
     * <p>
     * Campo <code>bankId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "bankId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
    private String bankid;

    /**
     * <p>
     * Campo <code>branchId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
    private String branchid;

    /**
     * <p>
     * Campo <code>currency</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
    private String currency;

    /**
     * <p>
     * Campo <code>deposit</code>, &iacute;ndice: <code>6</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 6, nombre = "deposit", tipo = TipoCampo.TABULAR)
    private List<Deposit> deposit;

    /**
     * <p>
     * Campo <code>bankName</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "bankName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
    private String bankname;

}
