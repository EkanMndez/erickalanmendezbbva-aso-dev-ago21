package com.bbva.mzic.accounts.dao.model.wydp;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>WYDP</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWydp</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionWydp</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: QGDTCCT.WYDP.txt
 * WYDPMODIFICACION DE ALIAS              KN        KN1CWYDP     01 KNDBEYDP            WYDP  SS0050CNNNNN        SSTS   M    NNNSNNNN  NN                2014-02-13CICSDM112016-08-2920.08.32CICSDM112014-02-13-16.54.40.645170CICSDM110001-01-010001-01-01
 * FICHERO: QGDTFDF.WYDP.txt
 * KNDBEYDP MODIFICACION DE ALIAS          F 04 00050 01 00001 NUMCLIE NUMERO DE CLIENTE    A 008 0 R
 * KNDBEYDP MODIFICACION DE ALIAS          F 04 00050 02 00009 TIPCUEN TIPO DE CUENTA       A 002 0 R
 * KNDBEYDP MODIFICACION DE ALIAS          F 04 00050 03 00011 NUMCUEN NUMERO DE CUENTA     A 020 0 R
 * KNDBEYDP MODIFICACION DE ALIAS          F 04 00050 04 00031 ALIAS   ALIAS                A 020 0 O
 * KNDBSYDP MODIFICACION DE ALIAS          X 01 00010 01 00001 FOLIOCN FOLIO CANAL          A 010 0 S
 * FICHERO: QGDTFDX.WYDP.txt
 * WYDPKNDBSYDPKNDBSYDPKN1CWYDP1S0010 000                     CICSDM112016-08-29-19.08.13.007532CICSDM112016-08-29-19.08.13.007559
</pre></code>
 * 
 * @see RespuestaTransaccionWydp
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "WYDP", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionWydp.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoKNDBEYDP.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWydp implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
