package com.bbva.mzic.accounts.dao.model.mchft103_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>month</code>, utilizado por la clase
 * <code>RespuestaTransaccionMchft103_1</code>
 * </p>
 * 
 * @see RespuestaTransaccionMchft103_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Month {

    /**
     * <p>
     * Campo <code>monthName</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "monthName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
    private String monthname;

    /**
     * <p>
     * Campo <code>totalCreditPayment</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "totalCreditPayment", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true, obligatorio = true)
    private BigDecimal totalcreditpayment;

    /**
     * <p>
     * Campo <code>totalSpends</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 3, nombre = "totalSpends", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true)
    private BigDecimal totalspends;

    /**
     * <p>
     * Campo <code>totalSavings</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 4, nombre = "totalSavings", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true)
    private BigDecimal totalsavings;

    /**
     * <p>
     * Campo <code>totalInvestments</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 5, nombre = "totalInvestments", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true)
    private BigDecimal totalinvestments;

    /**
     * <p>
     * Campo <code>totalInsurance</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 6, nombre = "totalInsurance", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true)
    private BigDecimal totalinsurance;

    /**
     * <p>
     * Campo <code>initialBalance</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 7, nombre = "initialBalance", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true)
    private BigDecimal initialbalance;

    /**
     * <p>
     * Campo <code>totalDeposits</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 8, nombre = "totalDeposits", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true)
    private BigDecimal totaldeposits;

    /**
     * <p>
     * Campo <code>available</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 9, nombre = "available", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true)
    private BigDecimal available;

}
