
package com.bbva.mzic.accounts.dao.model.getsaldoseq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mktpgm101Result complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mktpgm101Result">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WK_DISPO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_INVER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_DEPRET" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_RETRET" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_SALALF" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_MONEDA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_MENSAJ" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_CUENTASA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mktpgm101Result", propOrder = {
    "wkdispo",
    "wkinver",
    "wkdepret",
    "wkretret",
    "wksalalf",
    "wkmoneda",
    "wkmensaj",
    "wkcuentasa"
})
public class Mktpgm101Result {

    @XmlElement(name = "WK_DISPO", required = true)
    protected String wkdispo;
    @XmlElement(name = "WK_INVER", required = true)
    protected String wkinver;
    @XmlElement(name = "WK_DEPRET", required = true)
    protected String wkdepret;
    @XmlElement(name = "WK_RETRET", required = true)
    protected String wkretret;
    @XmlElement(name = "WK_SALALF", required = true)
    protected String wksalalf;
    @XmlElement(name = "WK_MONEDA", required = true)
    protected String wkmoneda;
    @XmlElement(name = "WK_MENSAJ", required = true)
    protected String wkmensaj;
    @XmlElement(name = "WK_CUENTASA", required = true)
    protected String wkcuentasa;

    /**
     * Gets the value of the wkdispo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKDISPO() {
        return wkdispo;
    }

    /**
     * Sets the value of the wkdispo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKDISPO(String value) {
        this.wkdispo = value;
    }

    /**
     * Gets the value of the wkinver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKINVER() {
        return wkinver;
    }

    /**
     * Sets the value of the wkinver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKINVER(String value) {
        this.wkinver = value;
    }

    /**
     * Gets the value of the wkdepret property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKDEPRET() {
        return wkdepret;
    }

    /**
     * Sets the value of the wkdepret property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKDEPRET(String value) {
        this.wkdepret = value;
    }

    /**
     * Gets the value of the wkretret property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKRETRET() {
        return wkretret;
    }

    /**
     * Sets the value of the wkretret property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKRETRET(String value) {
        this.wkretret = value;
    }

    /**
     * Gets the value of the wksalalf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKSALALF() {
        return wksalalf;
    }

    /**
     * Sets the value of the wksalalf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKSALALF(String value) {
        this.wksalalf = value;
    }

    /**
     * Gets the value of the wkmoneda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKMONEDA() {
        return wkmoneda;
    }

    /**
     * Sets the value of the wkmoneda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKMONEDA(String value) {
        this.wkmoneda = value;
    }

    /**
     * Gets the value of the wkmensaj property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKMENSAJ() {
        return wkmensaj;
    }

    /**
     * Sets the value of the wkmensaj property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKMENSAJ(String value) {
        this.wkmensaj = value;
    }

    /**
     * Gets the value of the wkcuentasa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKCUENTASA() {
        return wkcuentasa;
    }

    /**
     * Sets the value of the wkcuentasa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKCUENTASA(String value) {
        this.wkcuentasa = value;
    }

	@Override
	public String toString() {
		return "Mktpgm101Result [wkdispo=" + wkdispo + ", wkinver=" + wkinver + ", wkdepret=" + wkdepret + ", wkretret="
				+ wkretret + ", wksalalf=" + wksalalf + ", wkmoneda=" + wkmoneda + ", wkmensaj=" + wkmensaj
				+ ", wkcuentasa=" + wkcuentasa + "]";
	}
    
      

}
