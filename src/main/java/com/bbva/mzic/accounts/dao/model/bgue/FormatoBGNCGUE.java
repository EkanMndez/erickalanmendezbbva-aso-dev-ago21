package com.bbva.mzic.accounts.dao.model.bgue;


import java.util.Date;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>BGNCGUE</code> de la transacci&oacute;n <code>BGUE</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGNCGUE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGNCGUE {

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String cuenta;

    /**
     * <p>
     * Campo <code>PRICHEQ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 2, nombre = "PRICHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer pricheq;

    /**
     * <p>
     * Campo <code>ULTCHEQ</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 3, nombre = "ULTCHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer ultcheq;

    /**
     * <p>
     * Campo <code>NOMBRS</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "NOMBRS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String nombrs;

    /**
     * <p>
     * Campo <code>APEPAT</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "APEPAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String apepat;

    /**
     * <p>
     * Campo <code>APEMAT</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "APEMAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String apemat;

    /**
     * <p>
     * Campo <code>CODIDE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 7, nombre = "CODIDE", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
    private Integer codide;

    /**
     * <p>
     * Campo <code>NUMIDE</code>, &iacute;ndice: <code>8</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 8, nombre = "NUMIDE", tipo = TipoCampo.ENTERO, longitudMinima = 13, longitudMaxima = 13)
    private Long numide;

    /**
     * <p>
     * Campo <code>FECVEN</code>, &iacute;ndice: <code>9</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 9, nombre = "FECVEN", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fecven;

    /**
     * <p>
     * Campo <code>FECEXP</code>, &iacute;ndice: <code>10</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 10, nombre = "FECEXP", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fecexp;

}
