package com.bbva.mzic.accounts.dao.model.mtkdt031_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT031</code>
 * 
 * @see PeticionTransaccionMtkdt031_1
 * @see RespuestaTransaccionMtkdt031_1
 */
@Component
public class TransaccionMtkdt031_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt031_1, RespuestaTransaccionMtkdt031_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt031_1 invocar(PeticionTransaccionMtkdt031_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt031_1.class, RespuestaTransaccionMtkdt031_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt031_1 invocarCache(PeticionTransaccionMtkdt031_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt031_1.class, RespuestaTransaccionMtkdt031_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt031_1.class, "vaciearCache");
	}
}
