package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>contentReport</code>, utilizado por la clase <code>Reportlisttransaction</code></p>
 * 
 * @see Reportlisttransaction
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Contentreport {
	
	/**
	 * <p>Campo <code>reportDate</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "reportDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String reportdate;
	
	/**
	 * <p>Campo <code>totalAmountReport</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "totalAmountReport", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 16, signo = true)
	private String totalamountreport;
	
	/**
	 * <p>Campo <code>totalAmountCurrency</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "totalAmountCurrency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
	private String totalamountcurrency;
	
	/**
	 * <p>Campo <code>agruperOperations</code>, &iacute;ndice: <code>4</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 4, nombre = "agruperOperations", tipo = TipoCampo.TABULAR)
	private List<Agruperoperations> agruperoperations;
	
}
