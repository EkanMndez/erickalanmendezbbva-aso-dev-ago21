package com.bbva.mzic.accounts.dao.model.bgpx;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGMFPXA</code> de la transacci&oacute;n <code>BGPX</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMFPXA")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGMFPXA implements IFormat {

    /**
     * <p>
     * Campo <code>ID</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "ID", tipo = TipoCampo.ENTERO, longitudMinima = 13, longitudMaxima = 13)
    private Long id;

    /**
     * <p>
     * Campo <code>AMOUNT</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "AMOUNT", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
    private BigDecimal amount;

    /**
     * <p>
     * Campo <code>CURENCY</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "CURENCY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String curency;

    /**
     * <p>
     * Campo <code>MONEYFL</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "MONEYFL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String moneyfl;

    /**
     * <p>
     * Campo <code>CONCEPT</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "CONCEPT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String concept;

    /**
     * <p>
     * Campo <code>TTYPEID</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "TTYPEID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
    private String ttypeid;

    /**
     * <p>
     * Campo <code>TTYPENA</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "TTYPENA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String ttypena;

    /**
     * <p>
     * Campo <code>TTYPEIC</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "TTYPEIC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String ttypeic;

    /**
     * <p>
     * Campo <code>TTYPEIN</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "TTYPEIN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 34, longitudMaxima = 34)
    private String ttypein;

    /**
     * <p>
     * Campo <code>OPEDATE</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "OPEDATE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 29, longitudMaxima = 29)
    private String opedate;

    /**
     * <p>
     * Campo <code>VALDATE</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "VALDATE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 29, longitudMaxima = 29)
    private String valdate;

    /**
     * <p>
     * Campo <code>ACCDATE</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "ACCDATE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 29, longitudMaxima = 29)
    private String accdate;

    /**
     * <p>
     * Campo <code>FTYPEID</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "FTYPEID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 13, longitudMaxima = 13)
    private String ftypeid;

    /**
     * <p>
     * Campo <code>STATUSI</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "STATUSI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
    private String statusi;

    /**
     * <p>
     * Campo <code>CNTYPEI</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "CNTYPEI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String cntypei;

    /**
     * <p>
     * Campo <code>COPROID</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "COPROID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String coproid;

    /**
     * <p>
     * Campo <code>COPRONA</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "COPRONA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String coprona;

    /**
     * <p>
     * Campo <code>COALIAS</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "COALIAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String coalias;

    /**
     * <p>
     * Campo <code>REFEREN</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "REFEREN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String referen;

    /**
     * <p>
     * Campo <code>ADDDATA</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "ADDDATA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String adddata;

    /**
     * <p>
     * Campo <code>OPBALAN</code>, &iacute;ndice: <code>21</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 21, nombre = "OPBALAN", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
    private BigDecimal opbalan;

}
