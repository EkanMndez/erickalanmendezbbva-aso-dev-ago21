package com.bbva.mzic.accounts.dao.model.mtkdt034_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Bean de respuesta para la transacci&oacute;n <code>MTKDT034</code>
 * 
 * @see PeticionTransaccionMtkdt034_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMtkdt034_1 {
	
	/**
	 * <p>Cabecera <code>COD-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_AVISO)
	private String codigoAviso;
	
	/**
	 * <p>Cabecera <code>DES-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.DESCRIPCION_AVISO)
	private String descripcionAviso;
	
	/**
	 * <p>Cabecera <code>COD-UUAA-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.APLICACION_AVISO)
	private String aplicacionAviso;
	
	/**
	 * <p>Cabecera <code>COD-RETORNO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_RETORNO)
	private String codigoRetorno;
	
	/**
	 * <p>Campo <code>ticketId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "ticketId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
	private String campo_1_ticketid;
	
	/**
	 * <p>Campo <code>accountId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true)
	private String campo_2_accountid;
	
	/**
	 * <p>Campo <code>functionaryId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "functionaryId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String functionaryid;
	
	/**
	 * <p>Campo <code>functionaryName</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "functionaryName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 90, signo = true)
	private String functionaryname;
	
	/**
	 * <p>Campo <code>bankId</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "bankId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String bankid;
	
	/**
	 * <p>Campo <code>bankName</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "bankName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
	private String bankname;
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
	private String currency;
	
	/**
	 * <p>Campo <code>branchId</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String branchid;
	
	/**
	 * <p>Campo <code>branchName</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "branchName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
	private String branchname;
	
	/**
	 * <p>Campo <code>deposit</code>, &iacute;ndice: <code>10</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 10, nombre = "deposit", tipo = TipoCampo.TABULAR)
	private List<Deposit> deposit;
	
}
