package com.bbva.mzic.accounts.dao.model.wyis;

import java.math.BigDecimal;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Formato de datos <code>KNDBSIS2</code> de la transacci&oacute;n <code>WYIS</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBSIS2")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBSIS2 {

    /**
     * <p>
     * Campo <code>CTANOM</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CTANOM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String ctanom;

    /**
     * <p>
     * Campo <code>IDSDCTA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "IDSDCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String idsdcta;

    /**
     * <p>
     * Campo <code>NUMCRED</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "NUMCRED", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String numcred;

    /**
     * <p>
     * Campo <code>IDSDCRE</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "IDSDCRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String idsdcre;

    /**
     * <p>
     * Campo <code>MONTCRE</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 5, nombre = "MONTCRE", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
    private BigDecimal montcre;

    /**
     * <p>
     * Campo <code>PRELAC</code>, &iacute;ndice: <code>6</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 6, nombre = "PRELAC", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
    private Integer prelac;

    /**
     * <p>
     * Campo <code>DESCCRE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "DESCCRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String desccre;

}
