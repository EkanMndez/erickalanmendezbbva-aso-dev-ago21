// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mbgdtac1_1;

import java.lang.String;

privileged aspect Participantdata_Roo_JavaBean {
    
    public String Participantdata.getParticipeclientdata() {
        return this.participeclientdata;
    }
    
    public void Participantdata.setParticipeclientdata(String participeclientdata) {
        this.participeclientdata = participeclientdata;
    }
    
    public String Participantdata.getTypeparticipationdata() {
        return this.typeparticipationdata;
    }
    
    public void Participantdata.setTypeparticipationdata(String typeparticipationdata) {
        this.typeparticipationdata = typeparticipationdata;
    }
    
    public String Participantdata.getCategorydata() {
        return this.categorydata;
    }
    
    public void Participantdata.setCategorydata(String categorydata) {
        this.categorydata = categorydata;
    }
    
    public String Participantdata.getCombinationpowersdata() {
        return this.combinationpowersdata;
    }
    
    public void Participantdata.setCombinationpowersdata(String combinationpowersdata) {
        this.combinationpowersdata = combinationpowersdata;
    }
    
    public String Participantdata.getLimitdata() {
        return this.limitdata;
    }
    
    public void Participantdata.setLimitdata(String limitdata) {
        this.limitdata = limitdata;
    }
    
    public String Participantdata.getCurrency() {
        return this.currency;
    }
    
    public void Participantdata.setCurrency(String currency) {
        this.currency = currency;
    }
    
}
