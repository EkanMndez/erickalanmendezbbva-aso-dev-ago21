package com.bbva.mzic.accounts.rm.enums;

public enum DigitalCheckStatusEnum {
    VALID,    //Its indicates that de digital check is fully operative
    CANCELED,    //Its indicates that The digital check is canceled.
    CANCELED_WITHOUT_FUNDS,    //It applies when the receiver tries to cash the digital check and there are not enough funds.
    CASHED,    //It indicates the digital check was cashed.
    EXPIRED    //Indicates that the digital check expired because it was not cashed before the time defined after its issuance.
}
