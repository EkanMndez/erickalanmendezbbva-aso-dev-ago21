package com.bbva.mzic.accounts.rm.enums;

public enum ConditionEnum {
	OPENING_FEE, // This condition only applies when the user has opened a new account.
	MIN_BALANCE_FEE, // This condition applies for maintenance of the account.
	TRANSFER_FEE_PERCENTAGE, // This condition applies when the user makes a transfer.
	MEMBERSHIP_FEE, // This condition applies for fixing an account maintenance fee.
}
