package com.bbva.mzic.accounts.rm.enums;

public enum StatusSavingGoalEnum {
	ACTIVATED, // The Saving Goal is fully operative.
	FINISHED, // The Saving Goal is accomplished.
	WITHDRAWN, // The customer withdrew some money.
	LIQUIDATED, // The client reached the Saving Goal and the money was returned to his
				// associated account.
	CANCELATED // The money was fully withdrawn before reaching the Saving Goal.
}
