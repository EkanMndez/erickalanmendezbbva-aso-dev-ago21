package com.bbva.mzic.accounts.rm.enums;

public enum DigitalCheckTypeEnum {

    UNRESERVED_AMOUNT_CHECK,    //It indicates that digital check don't have reserved amount.
    RESERVED_AMOUNT_CHECK       //It indicates that digital check has reservated amount.
}
