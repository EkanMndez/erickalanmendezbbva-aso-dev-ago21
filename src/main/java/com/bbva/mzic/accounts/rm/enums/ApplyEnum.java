package com.bbva.mzic.accounts.rm.enums;

public enum ApplyEnum {
	MONTHLY_AVERAGE_BALANCE, // This fact applies monthly average balance.
	CURRENT_BALANCE, // This fact applies current balance.
	TRANSFER_AMOUNT // This fact applies transfer amount.
}
