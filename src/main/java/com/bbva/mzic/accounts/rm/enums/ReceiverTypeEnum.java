package com.bbva.mzic.accounts.rm.enums;

public enum ReceiverTypeEnum {

    EMAIL,    //The type of contact information is an email.
    PHONE    //The type of contact information is a phone.
}
