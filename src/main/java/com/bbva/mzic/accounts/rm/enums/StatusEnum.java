package com.bbva.mzic.accounts.rm.enums;

public enum StatusEnum {
	ACTIVATED, // The account is fully operative.
	BLOCKED, // The account has been blocked by a justified reason.
	CANCELED, // The account is canceled and the user can't operate with it.
	DORMANT, // The account has been unused for 361 days.
	ABANDONED, // The account has been in dormant status for 3 years.
	ESCHEATED, // If the account has been in abandoned status can be moved to this state if the
				// owner cannot be contacted. The funds are then a property of the state.
	CHARGE_OFF, // The account is overdrawn and has been closed for an extended period of time.

}
