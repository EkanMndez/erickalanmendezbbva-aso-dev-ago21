package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.mzic.accounts.rm.enums.StatusSavingGoalEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "savingGoals", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "savingGoals", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoStatusSavingGoal implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Current status of the saving goal.")
	private StatusSavingGoalEnum id;

	public StatusSavingGoalEnum getId() {
		return id;
	}

	public void setId(StatusSavingGoalEnum id) {
		this.id = id;
	}
}
