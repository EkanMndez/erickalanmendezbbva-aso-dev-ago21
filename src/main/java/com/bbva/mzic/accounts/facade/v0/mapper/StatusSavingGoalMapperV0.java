package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntStatusSavingGoal;
import com.bbva.mzic.accounts.facade.v0.dto.DtoStatusSavingGoal;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface StatusSavingGoalMapperV0 {
    DtoIntStatusSavingGoal mapToInner(DtoStatusSavingGoal statusSavingGoal);

    DtoStatusSavingGoal mapToOuter(DtoIntStatusSavingGoal statusSavingGoal);
}
