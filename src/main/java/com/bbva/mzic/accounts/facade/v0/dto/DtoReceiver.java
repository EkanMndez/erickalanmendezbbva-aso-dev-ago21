package com.bbva.mzic.accounts.facade.v0.dto;

import com.bbva.mzic.accounts.rm.enums.ReceiverTypeEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "receiver", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "receiver", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoReceiver implements Serializable {

    @ApiModelProperty(value = "Contact information type identifier.")
    private ReceiverTypeEnum type;

    @ApiModelProperty(value = "Contact information related to the type of identifier")
    private String value;

    @ApiModelProperty(value = "Information about the holder receiver digital check.")
    private DtoHolder holder;

    public ReceiverTypeEnum getType() {
        return type;
    }

    public void setType(ReceiverTypeEnum type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DtoHolder getHolder() {
        return holder;
    }

    public void setHolder(DtoHolder holder) {
        this.holder = holder;
    }
}
