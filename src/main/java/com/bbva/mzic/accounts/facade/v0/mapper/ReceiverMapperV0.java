package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntReceiver;
import com.bbva.mzic.accounts.facade.v0.dto.DtoReceiver;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = {HolderMapperV0.class})

public interface ReceiverMapperV0 {
    DtoIntReceiver mapToInner(DtoReceiver receiver);

    DtoReceiver mapToOuter(DtoIntReceiver receiver);
}
