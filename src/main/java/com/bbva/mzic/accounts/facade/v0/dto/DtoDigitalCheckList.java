package com.bbva.mzic.accounts.facade.v0.dto;

import com.bbva.jee.arq.spring.core.catalog.gabi.PageableDto;
import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheck;

import java.util.ArrayList;
import java.util.List;

public class DtoDigitalCheckList implements PageableDto {
    private List<DtoIntDigitalCheck> digitalCheckList;
    private Pagination pagination;

    @Override
    public Pagination getPagination() {
        return pagination;
    }

    @Override
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<DtoIntDigitalCheck> getDigitalCheckList() {
        if (digitalCheckList == null) {
            digitalCheckList = new ArrayList<>();
        }
        return digitalCheckList;
    }

    public void setDigitalCheckList(List<DtoIntDigitalCheck> digitalCheckList) {
        this.digitalCheckList = digitalCheckList;
    }
}
