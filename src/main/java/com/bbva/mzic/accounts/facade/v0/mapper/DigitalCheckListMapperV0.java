package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheckList;
import com.bbva.mzic.accounts.facade.v0.dto.DtoDigitalCheckList;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = {DigitalCheckMapperV0.class})
public interface DigitalCheckListMapperV0 {
    DtoIntDigitalCheckList mapToInner(DtoDigitalCheckList digitalCheckList);

    DtoDigitalCheckList mapToOuter(DtoIntDigitalCheckList digitalCheckList);
}
