package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheck;
import com.bbva.mzic.accounts.facade.v0.dto.DtoDigitalCheck;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = {ReceiverMapperV0.class, SentMoneyMapperV0.class, DigitalCheckStatusMapperV0.class,
        DigitalCheckTypeMapperV0.class, CashoutMapperV0.class})
public interface DigitalCheckMapperV0 {
    DtoIntDigitalCheck mapToInner(DtoDigitalCheck digitalCheck);

    DtoDigitalCheck mapToOuter(DtoIntDigitalCheck digitalCheck);
}
