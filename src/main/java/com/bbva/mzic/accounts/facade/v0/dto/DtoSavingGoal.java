package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "savingGoals", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "savingGoals", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoSavingGoal implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Identifier of the saving goal.")
	private String id;

	@ApiModelProperty(value = "Defines the status of the saving goal account.")
	private DtoStatusSavingGoal status;

	@ApiModelProperty(value = "Defines the status of the saving goal account.")
	private String savingGoalNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DtoStatusSavingGoal getStatus() {
		return status;
	}

	public void setStatus(DtoStatusSavingGoal status) {
		this.status = status;
	}

	public String getSavingGoalNumber() {
		return savingGoalNumber;
	}

	public void setSavingGoalNumber(String savingGoalNumber) {
		this.savingGoalNumber = savingGoalNumber;
	}

}
