package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "listSavingGoals", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "listSavingGoals", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoSavingGoalList implements Serializable {
	public List<DtoSavingGoal> getListSavingGoals() {
		return listSavingGoals;
	}

	public void setListSavingGoals(List<DtoSavingGoal> listSavingGoals) {
		this.listSavingGoals = listSavingGoals;
	}

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "This balance means the maximum expendable amount at the moment of retrieval.")
	private List<DtoSavingGoal> listSavingGoals;

}
