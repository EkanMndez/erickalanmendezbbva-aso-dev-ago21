package com.bbva.mzic.accounts.facade.v0.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseInterface;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseNoContent.ServiceResponseNoContentBuilder;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.jee.arq.spring.core.servicing.annotations.*;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.mzic.accounts.business.ISrvIntAccountsV0;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.facade.v0.ISrvAccountsV0;
import com.bbva.mzic.accounts.facade.v0.dto.*;
import com.bbva.mzic.accounts.facade.v0.mapper.*;
import com.bbva.mzic.accounts.facade.v0.validators.ValidatorAccountsV0;
import com.bbva.mzic.serviceutils.rm.utils.errors.EnumError;
import com.bbva.mzic.serviceutils.rm.utils.ids.IdAccountUtils;
import com.bbva.mzic.serviceutils.rm.utils.web.ContextUtils;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Path("/v0")
@SN(registryID = "SNMX1510047", logicalID = "accounts")
@VN(vnn = "v0")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Service("srvAccountsV0")
@Api(value = "/accounts/v0", description = "SN Accounts")
public class SrvAccountsV0 implements ISrvAccountsV0 {

    private static final String ACCOUNT_ID = "accountId";

    @Autowired
    private ContextUtils contextUtils;

    @Autowired
    private AccountMapperV0 accountMapperV0;

    @Autowired
    private ISrvIntAccountsV0 iSrvIntAccountsV0;

    @Autowired
    private IdAccountUtils idAccountUtils;

    @Autowired
    private AggregatedAvailableBalanceMapperV0 aggregatedAvailableBalanceMapperV0;

    @Autowired
    private SavingGoalListMapperV0 savingGoalListMapperV0;

    @Autowired
    private DigitalCheckListMapperV0 digitalCheckListMapperV0;

    @Autowired
    private DigitalCheckMapperV0 digitalCheckMapperV0;

    private static final String GABI_CATALOG = "gabiCatalog";
    private static final String UNATTENDED_CHANNEL = "1";

    @Override
    @Path("/accounts/{accountId}")
    @GET
    @SMC(registryID = "SMCMX1610002", logicalID = "getAccount", forcedCatalog = GABI_CATALOG)
    @ApiOperation(value = "Recuperación de la información detallada de una cuenta específica.", notes = "Recuperación de la información detallada de una cuenta específica.", response = DtoAccount.class, nickname = "getAccount", httpMethod = HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ServiceResponseInterface getAccounts(
            @CasContract(mask = IdAccountUtils.ACCOUNT_MASK) @SecurityFunction(outFunction = "cypher") @ApiParam(value = ACCOUNT_ID) @PathParam(ACCOUNT_ID) String accountId,
            @QueryParam("expands") String expands, @QueryParam("fields") String fields) {

        // Validar los obligatorios
        ValidatorAccountsV0.getAccount(accountId);

        final DtoIntFilterAccount filterAccount = new DtoIntFilterAccount();
        filterAccount.setAccountId(accountId);
        filterAccount.setExpands(expands);

        // Extracción de la cuenta
        filterAccount.setAccountNumber(idAccountUtils.getAccountNumber(accountId));
        filterAccount.setAccountType(idAccountUtils.getAccountType(accountId));

        // Extraer del contexto el cliente a 8 posiciones
        filterAccount.setClientId(contextUtils.getBackendProperties().getClientId());

        DtoAccount dtoResponse = new DtoAccount();

        // Llamar a business
        dtoResponse = accountMapperV0.mapToOuter(iSrvIntAccountsV0.getAccount(filterAccount));

        // Filtrar expands no necesarios
        // TODO: Implementar: filterExpands(expands, dtoResponse);

        return ServiceResponseOK.data(dtoResponse).build();
    }

    @Override
    @Path("/balances")
    @GET
    @CasIgnored
    @SMC(forcedCatalog = GABI_CATALOG, registryID = "SMCMX1710049", logicalID = "getBalance")
    @ApiOperation(value = "Consultar el saldo de las cuentas asociadas a un usuario.", notes = "Consultar el saldo de las cuentas asociadas a un usuario.", response = DtoAggregatedAvailableBalance.class, nickname = "getBalance", httpMethod = HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ServiceResponseInterface getBalances(@QueryParam(value = "accountFamily.id") String accountFamilyId) {
        ValidatorAccountsV0.validaAccountFamily(accountFamilyId);
        final DtoIntFilterAccount dtoIntFilterAccount = new DtoIntFilterAccount();
        dtoIntFilterAccount.setAccountFamilyId(accountFamilyId);

        final DtoAggregatedAvailableBalance dtoAggregatedAvailableBalance = aggregatedAvailableBalanceMapperV0
                .mapToOuter(iSrvIntAccountsV0.getBalance(dtoIntFilterAccount));
        return ServiceResponseOK.data(dtoAggregatedAvailableBalance).build();
    }

    @Override
    @Path("/accounts/saving-goals")
    @GET
    @CasIgnored
    @SMC(forcedCatalog = GABI_CATALOG, registryID = "SMCMX1910039", logicalID = "listSavingGoals")
    @ApiOperation(value = "Lista las metas de ahorro asociadas a todas las cuentas que tiene un cliente.", notes = "Lista las metas de ahorro asociadas a todas las cuentas que tiene un cliente.", response = List.class, nickname = "listSavingGoals", httpMethod = HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ServiceResponseInterface listSavingGoals(@QueryParam(value = "customer-id") String customerId) {
        DtoIntFilterAccount filterAccount = new DtoIntFilterAccount();
        String channelType = contextUtils.getBackendProperties().getType();

        if (UNATTENDED_CHANNEL.equals(channelType)) {
            filterAccount.setCustomerId(contextUtils.getBackendProperties().getClientId());
        } else {
            if (StringUtils.isBlank(customerId)) {
                throw new BusinessServiceException(EnumError.WRONG_PARAMETERS_EXPLICIT.getAlias());
            } else {
                filterAccount.setCustomerId(customerId);
            }
        }

        DtoSavingGoalList savingGoalList = savingGoalListMapperV0.mapToOuter(iSrvIntAccountsV0.listSavingGoals(filterAccount));

        if (savingGoalList.getListSavingGoals().isEmpty()) {
            return ServiceResponseNoContentBuilder.build();
        }
        return ServiceResponseOK.data(savingGoalList.getListSavingGoals()).build();
    }

    @Override
    @Path("/accounts/{account-id}/digital-checks")
    @GET
    @CasIgnored
    @SMC(forcedCatalog = GABI_CATALOG, registryID = "SMCMX1710129", logicalID = "listDigitalCheck")
    @ApiOperation(value = "Recupera la lista de cheques digitales de un cliente especifico.", notes = "Recupera la lista de cheques digitales de un cliente especifico.", response = List.class, nickname = "listDigitalCheck", httpMethod = HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ServiceResponseInterface listDigitalCheck(@CasContract(mask = IdAccountUtils.ACCOUNT_MASK) @SecurityFunction(inFunction = "decypher")
                                                     @PathParam("acount-id") String accountId,
                                                     @QueryParam("fromOperationDate") String fromOperationDate,
                                                     @QueryParam("toOperationDate") String toOperationDate,
                                                     @QueryParam("isHistorical") Boolean isHistorical,
                                                     @QueryParam("pageSize") Integer pageSize,
                                                     @QueryParam("paginationKey") String paginationKey,
                                                     @QueryParam("customerId") String customerId) {

        DtoDigitalCheckList digitalChecks;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        DtoIntFilterAccount filterAccount = new DtoIntFilterAccount();
        ValidatorAccountsV0.validateDateFormatInput(fromOperationDate);
        ValidatorAccountsV0.validateDateFormatInput(toOperationDate);

        filterAccount.setAccountId(accountId);
        filterAccount.setHistorical(isHistorical);
        filterAccount.setPageSize(pageSize);
        filterAccount.setPaginationKey(paginationKey);
        filterAccount.setCustomerId(customerId);

        try {
            filterAccount.setFromOperationDate(format.parse(fromOperationDate));
            filterAccount.setToOperationDate(format.parse(toOperationDate));
        } catch (ParseException pe) {
            throw new BusinessServiceException(EnumError.WRONG_PARAMETERS.getAlias());
        }

        digitalChecks = digitalCheckListMapperV0.mapToOuter(iSrvIntAccountsV0.listDigitalCheckMBGDTCHD(filterAccount));

        return ServiceResponseOK.data(digitalChecks.getDigitalCheckList()).pagination(digitalChecks.getPagination()).build();
    }

    @Override
    @Path("/accounts/{account-id}/digital-checks/{digital-check-id}")
    @GET
    @CasIgnored
    @SMC(forcedCatalog = GABI_CATALOG, registryID = "SMCMX1710130", logicalID = "getDigitalCheck")
    @ApiOperation(value = "Obtener el detalle de un cheque digital.", notes = "Obtener el detalle de un cheque digital.", response = DtoDigitalCheck.class, nickname = "getDigitalCheck", httpMethod = HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ServiceResponseInterface getDigitalCheck(@ApiParam("account-id") @PathParam("account-id") String accountId,
                                                    @SecurityFunction(inFunction = "decypher")@ApiParam("digital-check-id") @PathParam("digital-check-id") String digitalCheckId,
                                                    @QueryParam("curtomer.id") String customerId) {
        DtoIntFilterAccount filterAccount = new DtoIntFilterAccount();
        String channelType = contextUtils.getBackendProperties().getType();

        ValidatorAccountsV0.validateInputNotBlank(accountId);
        ValidatorAccountsV0.validateInputNotBlank(digitalCheckId);

        if (UNATTENDED_CHANNEL.equals(channelType)) {
            filterAccount.setCustomerId(contextUtils.getBackendProperties().getClientId());
        } else {
            ValidatorAccountsV0.validateInputNotBlank(customerId);
            filterAccount.setCustomerId(customerId);
        }
        ValidatorAccountsV0.validateDigitalCheckIdFormat(digitalCheckId);
        filterAccount.setDigitalCheckId(digitalCheckId);

        return ServiceResponseOK.data(digitalCheckMapperV0.mapToOuter(iSrvIntAccountsV0.getDigitalCheck(filterAccount))).build();
    }

}
