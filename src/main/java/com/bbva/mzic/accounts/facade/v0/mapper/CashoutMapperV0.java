package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntCashout;
import com.bbva.mzic.accounts.facade.v0.dto.DtoCashout;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface CashoutMapperV0 {
    DtoIntCashout mapToInner(DtoCashout cashout);

    DtoCashout mapToOuter(DtoIntCashout cashout);
}
