package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntHolder;
import com.bbva.mzic.accounts.facade.v0.dto.DtoHolder;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface HolderMapperV0 {
    DtoIntHolder mapToInner(DtoHolder holder);

    DtoHolder mapToOuter(DtoIntHolder holder);
}
