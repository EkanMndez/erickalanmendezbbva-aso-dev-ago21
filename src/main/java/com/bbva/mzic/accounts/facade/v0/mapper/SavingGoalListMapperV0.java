package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntSavingGoalList;
import com.bbva.mzic.accounts.facade.v0.dto.DtoSavingGoalList;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = {SavingGoalMapperV0.class})
public interface SavingGoalListMapperV0 {
    DtoIntSavingGoalList mapToInner(DtoSavingGoalList in);

    DtoSavingGoalList mapToOuter(DtoIntSavingGoalList out);

}
