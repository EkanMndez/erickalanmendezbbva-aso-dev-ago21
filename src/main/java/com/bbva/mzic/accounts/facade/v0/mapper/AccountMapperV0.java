package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccount;
import com.bbva.mzic.accounts.rm.Constants;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = {
		AccountTypeMapperV0.class })
public interface AccountMapperV0 {
	DtoIntAccount mapToInner(DtoAccount in);

	DtoAccount mapToOuter(DtoIntAccount out);
}
