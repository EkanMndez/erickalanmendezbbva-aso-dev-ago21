package com.bbva.mzic.accounts.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseInterface;

public interface ISrvAccountsV0 {
    ServiceResponseInterface getAccounts(String accountId, String expands, String fields);

    ServiceResponseInterface getBalances(String accountFamilyId);

    ServiceResponseInterface listSavingGoals(String customerId);

    ServiceResponseInterface listDigitalCheck(String accountId, String fromOperationDate, String toOperationDate, Boolean isHistorical, Integer pageSize, String paginationKey, String customerId);

    ServiceResponseInterface getDigitalCheck(String accountId, String digitalCheckId, String customerId);
}
