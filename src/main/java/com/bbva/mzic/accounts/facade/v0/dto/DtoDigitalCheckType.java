package com.bbva.mzic.accounts.facade.v0.dto;

import com.bbva.mzic.accounts.rm.enums.DigitalCheckTypeEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


@XmlRootElement(name = "digitalCheckType", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "digitalCheckType", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoDigitalCheckType implements Serializable {
    @ApiModelProperty(value = "Identifier associated to the digital check Type.")
    private DigitalCheckTypeEnum id;

    @ApiModelProperty(value = "Name associated to the digital check Type.")
    private String name;

    public DigitalCheckTypeEnum getId() {
        return id;
    }

    public void setId(DigitalCheckTypeEnum id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
