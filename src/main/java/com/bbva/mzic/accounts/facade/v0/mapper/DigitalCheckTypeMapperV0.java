package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheckType;
import com.bbva.mzic.accounts.facade.v0.dto.DtoDigitalCheckType;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface DigitalCheckTypeMapperV0 {
    DtoIntDigitalCheckType mapToInner(DtoDigitalCheckType digitalCheckType);

    DtoDigitalCheckType mapToOuter(DtoIntDigitalCheckType digitalCheckType);
}
