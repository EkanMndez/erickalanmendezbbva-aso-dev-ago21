package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntSavingGoal;
import com.bbva.mzic.accounts.facade.v0.dto.DtoSavingGoal;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = {StatusSavingGoalMapperV0.class})
public interface SavingGoalMapperV0 {
    DtoIntSavingGoal mapToInner(DtoSavingGoal savingGoal);

    DtoSavingGoal mapToOuter(DtoIntSavingGoal savingGoal);
}
