package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "aggregatedAvailableBalances", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "aggregatedAvailableBalances", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAggregatedAvailableBalance implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "This balance means the maximum expendable amount at the moment of retrieval.")
	private List<DtoBalance> currentBalances;

	@ApiModelProperty(value = "This balance is the maximum expendable amount calculated at the end of the last business day. ")
	private List<DtoBalance> postedBalances;

	@ApiModelProperty(value = "This balance is the aggregated amount of all pending transactions. This amount is the result of substracting postedBalance to currentBalance.")
	private List<DtoBalance> pendingBalances;

	public List<DtoBalance> getCurrentBalances() {
		return currentBalances;
	}

	public void setCurrentBalances(List<DtoBalance> currentBalances) {
		this.currentBalances = currentBalances;
	}

	public List<DtoBalance> getPostedBalances() {
		return postedBalances;
	}

	public void setPostedBalances(List<DtoBalance> postedBalances) {
		this.postedBalances = postedBalances;
	}

	public List<DtoBalance> getPendingBalances() {
		return pendingBalances;
	}

	public void setPendingBalances(List<DtoBalance> pendingBalances) {
		this.pendingBalances = pendingBalances;
	}

}
