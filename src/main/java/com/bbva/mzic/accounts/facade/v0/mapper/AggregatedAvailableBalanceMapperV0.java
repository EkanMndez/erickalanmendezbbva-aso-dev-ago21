package com.bbva.mzic.accounts.facade.v0.mapper;

import org.mapstruct.Mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAggregatedAvailableBalance;
import com.bbva.mzic.accounts.rm.Constants;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = { BalanceMapperV0.class })
public interface AggregatedAvailableBalanceMapperV0 {
	DtoIntAggregatedAvailableBalance mapToInner(DtoAggregatedAvailableBalance in);

	DtoAggregatedAvailableBalance mapToOuter(DtoIntAggregatedAvailableBalance out);

}
