package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

//import com.bbva.mzic.accounts.rm.utils.adapters.BigDecimalFlatAdapter;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "balances", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "balances", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoBalance implements Serializable {

	@ApiModelProperty(value = "Current available balance monetary amount.")
	//@XmlJavaTypeAdapter(BigDecimalFlatAdapter.class)
	private BigDecimal amount;

	@ApiModelProperty(value = "String based on ISO-4217 for specifying the currency related to the current available balance.")

	private String currency;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
