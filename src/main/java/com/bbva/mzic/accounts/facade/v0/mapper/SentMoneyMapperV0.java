package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntSentMoney;
import com.bbva.mzic.accounts.facade.v0.dto.DtoSentMoney;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface SentMoneyMapperV0 {
    DtoIntSentMoney mapToInner(DtoSentMoney sentMoney);

    DtoSentMoney mapToOuter(DtoIntSentMoney sentMoney);
}
