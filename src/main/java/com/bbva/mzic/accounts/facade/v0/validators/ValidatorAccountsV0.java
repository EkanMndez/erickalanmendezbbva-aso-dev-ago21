package com.bbva.mzic.accounts.facade.v0.validators;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.mzic.serviceutils.rm.utils.errors.EnumError;
import com.bbva.mzic.serviceutils.rm.utils.validation.Validator;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

public class ValidatorAccountsV0 {

    private static Validator validator = new Validator();

    public static void getAccount(String accountId) {
        validator.validateNotEmpty(accountId, EnumError.RESOURCE_NOT_FOUND);
    }

    public static void validaAccountFamily(final String accountFamilyId) {
        final List<String> familyIds = Arrays.asList("COMMON", "CREDIT", "CREDIT-EXTENSION", "SAVINGS");
        if (StringUtils.isBlank(accountFamilyId)) {
        } else if (!familyIds.contains(accountFamilyId)) {
            throw new BusinessServiceException(EnumError.WRONG_PARAMETERS.getAlias());
        }
    }

    public static void validateDateFormatInput(final String date) {
        try {
            LocalDate.parse(date);
        } catch (DateTimeParseException dtpe) {
            throw new BusinessServiceException(EnumError.WRONG_PARAMETERS_EXPLICIT.getAlias());
        } catch (NullPointerException dtpe) {
            throw new BusinessServiceException(EnumError.MANDATORY_PARAMETERS_MISSING.getAlias());
        }

    }

    public static void validateInputNotBlank(String inputDatum) {
        if (StringUtils.isBlank(inputDatum)) {
            throw new BusinessServiceException(EnumError.WRONG_PARAMETERS_EXPLICIT.getAlias());
        }
    }

    public static void validateDigitalCheckIdFormat(String digitalCheckId) {
        if (digitalCheckId.chars().filter(e -> e == '|').count() != 3) {
            throw new BusinessServiceException(EnumError.WRONG_PARAMETERS.getAlias());
        }
    }
}
