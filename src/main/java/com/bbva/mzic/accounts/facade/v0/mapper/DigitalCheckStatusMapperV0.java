package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheckStatus;
import com.bbva.mzic.accounts.facade.v0.dto.DtoDigitalCheckStatus;
import com.bbva.mzic.accounts.rm.Constants;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface DigitalCheckStatusMapperV0 {
    DtoIntDigitalCheckStatus mapToInner(DtoDigitalCheckStatus digitalCheckStatus);

    DtoDigitalCheckStatus mapToOuter(DtoIntDigitalCheckStatus digitalCheckStatus);
}
