package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.dao.model.bgl5.FormatoBGML5YS;
import com.bbva.mzic.accounts.dao.model.bgl5.RespuestaTransaccionBgl5;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class OutputBGML5MapperTest {

	@InjectMocks
	private OutputBGML5Mapper outputBGL5Mapper;

	@Test
	public void mapOutputMapper() {
		// Arrange
		final RespuestaTransaccionBgl5 respuestaTransaccion = new RespuestaTransaccionBgl5();
		final CopySalida cs = new CopySalida();
		final FormatoBGML5YS formato = new FormatoBGML5YS();
	/*	formato.setIdcta("CCC");
		formato.setEstatus(StatusEnum.ACTIVATED.name());
		formato.setSmancc("0.0");
		formato.setCommanc("0.0");
		formato.setCpromem("0.0");*/
		cs.setCopy(formato);
		respuestaTransaccion.getCuerpo().getPartes().add(cs);
		// Act
		final DtoIntAccount response = outputBGL5Mapper.map(respuestaTransaccion);
		// Assert
		Assert.assertNotNull(response);
	}

	@Test
	public void mapInputNull() {
		// Arrange
		// Act
		final DtoIntAccount response = outputBGL5Mapper.map(null);
		// Assert
		Assert.assertNull(response);
	}

}
