package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.wyrf.PeticionTransaccionWyrf;
import com.bbva.mzic.testutils.rm.utils.ServiceFactorySingleton;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class InputWYRFMapperTest {

    @InjectMocks
    private InputWYRFMapper inputWYRFMapper;

    @Test
    public void mapDigitalCheck() {
        // Arrange
        final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().getStubObject(DtoIntFilterAccount.class);
        dtoIntFilterAccount.setDigitalCheckId("123456789012|1234567890|2017-10-23|VG");

        // Act
        final PeticionTransaccionWyrf response = inputWYRFMapper.map(dtoIntFilterAccount);

        // Assert
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getCuerpo());
        Assert.assertNotNull(response.getCuerpo().getPartes());
        Assert.assertNotNull(response.getCuerpo().getPartes().get(0));
    }

    @Test(expected = BusinessServiceException.class)
    public void mapDigitalCheckInvalidDateFormat() {
        // Arrange
        final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().getStubObject(DtoIntFilterAccount.class);
        dtoIntFilterAccount.setDigitalCheckId("123456789012|1234567890|20|VG");

        // Act
        inputWYRFMapper.map(dtoIntFilterAccount);
    }

}
