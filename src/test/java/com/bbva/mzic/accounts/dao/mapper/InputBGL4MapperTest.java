package com.bbva.mzic.accounts.dao.mapper;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.bgl4.PeticionTransaccionBgl4;
import com.bbva.mzic.testutils.rm.utils.ServiceFactorySingleton;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class InputBGL4MapperTest {

	@InjectMocks
	private InputBGL4Mapper bgl4Mapper;

	@Test
	public void mapFamilyAccountId() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().getStubObject(DtoIntFilterAccount.class);
		dtoIntFilterAccount.setAccountFamilyId("COMMON");

		// Act
		final PeticionTransaccionBgl4 response = bgl4Mapper.map(dtoIntFilterAccount);

		// Assert
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getCuerpo());
		Assert.assertNotNull(response.getCuerpo().getPartes());
		Assert.assertNotNull(response.getCuerpo().getPartes().get(0));
	}

	@Test
	public void mapFamilyAccountIdNull() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().getStubObject(DtoIntFilterAccount.class);
		dtoIntFilterAccount.setAccountFamilyId(null);

		// Act
		final PeticionTransaccionBgl4 response = bgl4Mapper.map(dtoIntFilterAccount);

		// Assert
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getCuerpo());
		Assert.assertNotNull(response.getCuerpo().getPartes());
		Assert.assertNotNull(response.getCuerpo().getPartes().get(0));
	}

	@Test
	public void mapFamilyAccountIdSavings() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().getStubObject(DtoIntFilterAccount.class);
		dtoIntFilterAccount.setAccountFamilyId("SAVINGS");

		// Act
		final PeticionTransaccionBgl4 response = bgl4Mapper.map(dtoIntFilterAccount);

		// Assert
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getCuerpo());
		Assert.assertNotNull(response.getCuerpo().getPartes());
		Assert.assertNotNull(response.getCuerpo().getPartes().get(0));
	}
}
