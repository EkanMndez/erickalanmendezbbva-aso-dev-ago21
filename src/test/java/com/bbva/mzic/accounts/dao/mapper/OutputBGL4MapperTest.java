package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4TS;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4US;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4VS;
import com.bbva.mzic.accounts.dao.model.bgl4.RespuestaTransaccionBgl4;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class OutputBGL4MapperTest {

	@InjectMocks
	private OutputBGL4Mapper bgl4Mapper;

	@Test
	public void mapOutputMapper() {
		// Arrange
		final RespuestaTransaccionBgl4 respuestaTransaccion = new RespuestaTransaccionBgl4();
		final CopySalida cs1 = new CopySalida();
		final CopySalida cs2 = new CopySalida();
		final CopySalida cs3 = new CopySalida();
		final FormatoBGML4TS formatoBGML4TS = new FormatoBGML4TS();
		final FormatoBGML4VS formatoBGML4VS = new FormatoBGML4VS();
		final FormatoBGML4US formatoBGML4US = new FormatoBGML4US();

		formatoBGML4TS.setTotvimx(new BigDecimal("1768142.00"));
		formatoBGML4TS.setTotvius(new BigDecimal("198473.98"));
		formatoBGML4TS.setTotvieu(new BigDecimal("7892315.38"));

		cs1.setCopy(formatoBGML4TS);
		respuestaTransaccion.getCuerpo().getPartes().add(cs1);

		formatoBGML4VS.setTopvimx(new BigDecimal("147894.12"));
		formatoBGML4VS.setTopvius(new BigDecimal("87124.12"));
		formatoBGML4VS.setTopvieu(new BigDecimal("182349.99"));

		cs2.setCopy(formatoBGML4VS);
		respuestaTransaccion.getCuerpo().getPartes().add(cs2);

		formatoBGML4US.setTorvimx(new BigDecimal("768134.98"));
		formatoBGML4US.setTorvius(new BigDecimal("1768.09"));
		formatoBGML4US.setTorvieu(new BigDecimal("15237.23"));
		cs3.setCopy(formatoBGML4US);
		respuestaTransaccion.getCuerpo().getPartes().add(cs3);

		// Act
		final DtoIntAggregatedAvailableBalance response = bgl4Mapper.map(respuestaTransaccion);
		// Assert
		Assert.assertNotNull(response);
	}

	@Test
	public void mapOutputMapperResponseNull() {
		// Arrange
		final RespuestaTransaccionBgl4 respuestaTransaccion = null;

		// Act

		final DtoIntAggregatedAvailableBalance response = bgl4Mapper.map(respuestaTransaccion);
		// Assert
		Assert.assertNull(response);
	}
}
