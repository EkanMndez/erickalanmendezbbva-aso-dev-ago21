package com.bbva.mzic.accounts.dao.impl;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheck;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.wyrf.PeticionTransaccionWyrf;
import com.bbva.mzic.testutils.rm.utils.ServiceFactorySingleton;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class AccountsDaoV0Test {
    @InjectMocks
    private AccountsDaoV0 accountsDaoV0;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void getAccountBGL5() {
        new DtoIntFilterAccount();
        new DtoIntAccount();
        final DtoIntAccount response = new DtoIntAccount();

        // Mockito.when(servicioTransacciones.invoke(Mockito.<Class<PeticionTransaccionBgl5>>any(),
        // Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);

        // Act
        // TODO: Implementar DAO response =
        // accountsDaoV0.getAccountBGL5(dtoIntFilterAccount);

        // Assert
        // Mockito.verify(servicioTransacciones).invoke(Mockito.<Class<PeticionTransaccionBgl5>>any(),
        // Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
    }

    @Test
    public void getBalanceBGL4Common() {
        final DtoIntFilterAccount dtoIntFilterAccount = new DtoIntFilterAccount();
        new DtoIntAggregatedAvailableBalance();
        final DtoIntAggregatedAvailableBalance response;

        dtoIntFilterAccount.setAccountFamilyId("COMMON");

        // Mockito.when(servicioTransacciones.invoke(Mockito.<Class<PeticionTransaccionBgl4>>any(),
        // Mockito.any(DtoIntFilterAccount.class)))
        // .thenReturn(dtoIntAggregatedAvailableBalance);

        // Act
        // TODO: Implementar DAO
        response = accountsDaoV0.getBalanceBGL4(dtoIntFilterAccount);

        // Mockito.verify(servicioTransacciones).invoke(Mockito.<Class<PeticionTransaccionBgl4>>any(),
        // Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
    }

    @Test
    public void getBalanceBGL4() {
        final DtoIntFilterAccount dtoIntFilterAccount = new DtoIntFilterAccount();
        final DtoIntAggregatedAvailableBalance response;

        dtoIntFilterAccount.setAccountFamilyId("CREDIT");

        // Act
        // TODO: Implementar DAO
        response = accountsDaoV0.getBalanceBGL4(dtoIntFilterAccount);

        Assert.assertNotNull(response);
    }

    @Test
    public void getDigitalCheckWYRF() {
        //Arrenge
        DtoIntFilterAccount filterAccount = new DtoIntFilterAccount();
        DtoIntDigitalCheck digitalCheck = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntDigitalCheck.class);
        DtoIntDigitalCheck response;
        filterAccount.setAccountId("1567841423");
        filterAccount.setCustomerId("1235125");
        filterAccount.setDigitalCheckId("123456789012|1234567890|2017-100-23|VG");
        Mockito.when(servicioTransacciones.invoke(Mockito.<Class<PeticionTransaccionWyrf>>any(),
                Mockito.any(DtoIntFilterAccount.class))).thenReturn(digitalCheck);

        //Action
        response = accountsDaoV0.getDigitalCheckWYRF(filterAccount);
        Mockito.verify(servicioTransacciones).invoke(Mockito.<Class<PeticionTransaccionWyrf>>any(),
                Mockito.any(DtoIntFilterAccount.class));
        //Assert
        Assert.assertNotNull(response);

    }

}
