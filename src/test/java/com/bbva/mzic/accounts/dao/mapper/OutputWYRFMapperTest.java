package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheck;
import com.bbva.mzic.accounts.dao.model.wyrf.FormatoKNDBSYRF;
import com.bbva.mzic.accounts.dao.model.wyrf.RespuestaTransaccionWyrf;
import com.bbva.mzic.accounts.rm.enums.DigitalCheckTypeEnum;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class OutputWYRFMapperTest {

    @InjectMocks
    private OutputWYRFMapper outputWYRFMapper;

    @Test
    public void mapDigitalCheckType01() {
        // Arrange
        RespuestaTransaccionWyrf respuestaTransaccion = new RespuestaTransaccionWyrf();
        CopySalida cs = new CopySalida();
        final FormatoKNDBSYRF formatoKNDBSYRF = new FormatoKNDBSYRF();

        formatoKNDBSYRF.setNumcel("5512345678");
        formatoKNDBSYRF.setAlias("Pedro Perez Hernandez");
        formatoKNDBSYRF.setConcept("Pago oficinas mes de enero");
        formatoKNDBSYRF.setFecalta(new Date());
        formatoKNDBSYRF.setImpope(new BigDecimal("12456"));
        formatoKNDBSYRF.setCveotp("12347891234789");
        formatoKNDBSYRF.setFolioch("19786234");
        formatoKNDBSYRF.setTipcheq("01");

        cs.setCopy(formatoKNDBSYRF);
        respuestaTransaccion.getCuerpo().getPartes().add(cs);

        // Act
        final DtoIntDigitalCheck response = outputWYRFMapper.map(respuestaTransaccion);


        // Assert
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getDigitalCheckType().getId(), DigitalCheckTypeEnum.UNRESERVED_AMOUNT_CHECK);
        Assert.assertEquals(response.getReceiver().getValue(),"5512345678");
    }

    @Test
    public void mapDigitalCheckType02() {
        // Arrange
        RespuestaTransaccionWyrf respuestaTransaccion = new RespuestaTransaccionWyrf();
        CopySalida cs = new CopySalida();
        final FormatoKNDBSYRF formatoKNDBSYRF = new FormatoKNDBSYRF();

        formatoKNDBSYRF.setNumcel("5587654321");
        formatoKNDBSYRF.setAlias("Martha Martinez Monroy");
        formatoKNDBSYRF.setConcept("Pago oficinas mes de junio");
        formatoKNDBSYRF.setFecalta(new Date());
        formatoKNDBSYRF.setImpope(new BigDecimal("514313"));
        formatoKNDBSYRF.setCveotp("4136311234");
        formatoKNDBSYRF.setFolioch("180479");
        formatoKNDBSYRF.setTipcheq("02");

        cs.setCopy(formatoKNDBSYRF);
        respuestaTransaccion.getCuerpo().getPartes().add(cs);

        // Act
        final DtoIntDigitalCheck response = outputWYRFMapper.map(respuestaTransaccion);


        // Assert
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getDigitalCheckType().getId(), DigitalCheckTypeEnum.RESERVED_AMOUNT_CHECK);
    }

    @Test
    public void mapDigitalCheckType03() {
        // Arrange
        RespuestaTransaccionWyrf respuestaTransaccion = new RespuestaTransaccionWyrf();
        CopySalida cs = new CopySalida();
        final FormatoKNDBSYRF formatoKNDBSYRF = new FormatoKNDBSYRF();

        formatoKNDBSYRF.setNumcel("5587654321");
        formatoKNDBSYRF.setAlias("Martha Martinez Monroy");
        formatoKNDBSYRF.setConcept("Pago mes de junio");
        formatoKNDBSYRF.setFecalta(new Date());
        formatoKNDBSYRF.setImpope(new BigDecimal("514313"));
        formatoKNDBSYRF.setCveotp("4136311234");
        formatoKNDBSYRF.setFolioch("180479");
        formatoKNDBSYRF.setTipcheq("03");

        cs.setCopy(formatoKNDBSYRF);
        respuestaTransaccion.getCuerpo().getPartes().add(cs);

        // Act
        final DtoIntDigitalCheck response = outputWYRFMapper.map(respuestaTransaccion);


        // Assert
        Assert.assertNotNull(response);
        Assert.assertNull(response.getDigitalCheckType().getId());
    }
}
