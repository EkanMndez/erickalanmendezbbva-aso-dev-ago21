package com.bbva.mzic.accounts.facade.v0;

import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntAccountType;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccount;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccountType;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoBalance;
import com.bbva.mzic.testutils.rm.utils.ServiceFactorySingleton;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FacadeTestFactory {

    private static final ServiceFactorySingleton FACTORY_SINGLETON = ServiceFactorySingleton.getInstance();

    public static <T> T createDto(final Class<T> klass) {
        return FACTORY_SINGLETON.getStubObject(klass);
    }

    public DtoAccount getDtoAccountDummy() {
        final DtoAccount dto = new DtoAccount();

        dto.setAccountId("1234567890123456");
        dto.setAccountType(new DtoAccountType());

        return dto;
    }

    public DtoIntAccount getDtoIntAccountDummy() {
        final DtoIntAccount dto = new DtoIntAccount();

        dto.setAccountId("1234567890123456");
        dto.setAccountType(new DtoIntAccountType());
        dto.setAlias("alias");

        return dto;
    }

    public DtoIntAggregatedAvailableBalance getDtoINtAggregatedAvailableBalanceDummy() {
        final DtoIntAggregatedAvailableBalance aggregatedAvailableBalance = new DtoIntAggregatedAvailableBalance();
        final List<DtoIntBalance> currentBalances = new ArrayList<>();
        final List<DtoIntBalance> postedBalances = new ArrayList<>();
        final List<DtoIntBalance> pendingBalances = new ArrayList<>();
        final DtoIntBalance currentBalance = new DtoIntBalance();
        final DtoIntBalance postedBalance = new DtoIntBalance();
        final DtoIntBalance pendingBalance = new DtoIntBalance();

        currentBalance.setAmount(new BigDecimal("10000.00"));
        currentBalance.setCurrency("MXN");
        currentBalances.add(currentBalance);

        postedBalance.setAmount(new BigDecimal("6151234.02"));
        postedBalance.setCurrency("USD");
        postedBalances.add(pendingBalance);

        pendingBalance.setAmount(new BigDecimal("231430.89"));
        pendingBalance.setCurrency("EUR");
        pendingBalances.add(pendingBalance);

        aggregatedAvailableBalance.setCurrentBalances(currentBalances);
        aggregatedAvailableBalance.setPostedBalances(postedBalances);
        aggregatedAvailableBalance.setPendingBalances(pendingBalances);
        return aggregatedAvailableBalance;
    }

    public DtoAggregatedAvailableBalance getDtoAggregatedAvailableBalanceDummy() {
        final DtoAggregatedAvailableBalance aggregatedAvailableBalance = new DtoAggregatedAvailableBalance();
        final List<DtoBalance> currentBalances = new ArrayList<>();
        final List<DtoBalance> postedBalances = new ArrayList<>();
        final List<DtoBalance> pendingBalances = new ArrayList<>();
        final DtoBalance currentBalance = new DtoBalance();
        final DtoBalance postedBalance = new DtoBalance();
        final DtoBalance pendingBalance = new DtoBalance();

        currentBalance.setAmount(new BigDecimal("10000.00"));
        currentBalance.setCurrency("MXN");
        currentBalances.add(currentBalance);

        postedBalance.setAmount(new BigDecimal("6151234.02"));
        postedBalance.setCurrency("USD");
        postedBalances.add(pendingBalance);

        pendingBalance.setAmount(new BigDecimal("231430.89"));
        pendingBalance.setCurrency("EUR");
        pendingBalances.add(pendingBalance);

        aggregatedAvailableBalance.setCurrentBalances(currentBalances);
        aggregatedAvailableBalance.setPostedBalances(postedBalances);
        aggregatedAvailableBalance.setPendingBalances(pendingBalances);
        return aggregatedAvailableBalance;
    }

}
