package com.bbva.mzic.accounts.business.impl;

import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntDigitalCheck;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.impl.AccountsDaoV0;
import com.bbva.mzic.accounts.rm.enums.DigitalCheckStatusEnum;
import com.bbva.mzic.testutils.rm.utils.ServiceFactorySingleton;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class SrvIntAccountsV0Test {

    @InjectMocks
    private SrvIntAccountsV0 srvIntAccountsV0;

    @Mock
    private AccountsDaoV0 accountsDaoV0;

    private static final String ACCOUNT_NUMBER_6 = "123456";
    private static final String ACCOUNT_NUMBER_12 = "123456789012";
    private static final String ACCOUNT_NUMBER_16 = "1234567890123456";

    private static final String CUSTOMER_ID = "41324123";

    private static final String LOCATION_ID = "/location/id";

    @Test
    public void testGetAccountNullFilterAccount() throws Exception {
        // Arrange
        final DtoIntFilterAccount dtoIntFilterAccount = null;

        // Act
        final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);
        // Assert
        Assert.assertNull(response);
    }

    @Test
    public void getAccountWithoutExpandsAndAccountLength12() {
        // Arrange
        final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
                .getStubObject(DtoIntFilterAccount.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_12);
        final DtoIntAccount dtoIntAccount = new DtoIntAccount();

        // TODO:
        // Mockito.when(accountsDaoV0.getAccountBGL5(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);

        // ACT
        final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);

        // Assert

        // TODO:
        // Mockito.verify(accountsDaoV0).getAccountBGL5(Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
    }

    @Test
    public void getAccountWithoutExpandsAndAccountLength16() {
        // Arrange
        final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
                .getStubObject(DtoIntFilterAccount.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_16);
        final DtoIntAccount dtoIntAccount = new DtoIntAccount();
        // Mockito.when(accountsDaoV0.getAccountBGL5(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);

        // Act
        final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);

        // Assert
        // Mockito.verify(accountsDaoV0).getAccountBGL5(Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
    }

    @Test
    public void getAccountHoldsExpandsAndAccountLength16() {
        // Arrange
        final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
                .getStubObject(DtoIntFilterAccount.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_16);
        dtoIntFilterAccount.setExpands("holds");
        final DtoIntAccount dtoIntAccount = new DtoIntAccount();

        // Mockito.when(accountsDaoV0.getAccountBGL5(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
        // Act
        final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);

        // Assert
        // Mockito.verify(accountsDaoV0).getAccountBGL5(Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
    }

    @Test
    public void getAccountBlocksExpandsAndAccountLength16() {
        // Arrange
        final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
                .getStubObject(DtoIntFilterAccount.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_16);
        dtoIntFilterAccount.setExpands("blocks");
        final DtoIntAccount dtoIntAccount = new DtoIntAccount();
        // Mockito.when(accountsDaoV0.getAccountBGL5(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
        // Act
        final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);

        // Assert
        // Mockito.verify(accountsDaoV0).getAccountBGL5(Mockito.any(DtoIntFilterAccount.class));

        Assert.assertNotNull(response);
    }

    @Test
    public void getDigitalCheckVG() {
        //Arrenge
        DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntFilterAccount.class);
        DtoIntDigitalCheck digitalCheck = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntDigitalCheck.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_12);
        dtoIntFilterAccount.setCustomerId(CUSTOMER_ID);
        dtoIntFilterAccount.setDigitalCheckId("123456789012|1234567890|2017-100-23|VG");
        Mockito.when(accountsDaoV0.getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class)))
                .thenReturn(digitalCheck);
        //Action
        DtoIntDigitalCheck response = srvIntAccountsV0.getDigitalCheck(dtoIntFilterAccount);
        //Assert
        Mockito.verify(accountsDaoV0).getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getDigitalCheckStatus().getId(), DigitalCheckStatusEnum.VALID);
    }

    @Test
    public void getDigitalCheckCO() {
        //Arrenge
        DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntFilterAccount.class);
        DtoIntDigitalCheck digitalCheck = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntDigitalCheck.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_12);
        dtoIntFilterAccount.setCustomerId(CUSTOMER_ID);
        dtoIntFilterAccount.setDigitalCheckId("123456789012|1234567890|2017-100-23|CO");
        Mockito.when(accountsDaoV0.getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class)))
                .thenReturn(digitalCheck);
        //Action
        DtoIntDigitalCheck response = srvIntAccountsV0.getDigitalCheck(dtoIntFilterAccount);
        //Assert
        Mockito.verify(accountsDaoV0).getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getDigitalCheckStatus().getId(), DigitalCheckStatusEnum.CASHED);
    }

    @Test
    public void getDigitalCheckCN() {
        //Arrenge
        DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntFilterAccount.class);
        DtoIntDigitalCheck digitalCheck = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntDigitalCheck.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_12);
        dtoIntFilterAccount.setCustomerId(CUSTOMER_ID);
        dtoIntFilterAccount.setDigitalCheckId("123456789012|1234567890|2017-100-23|CN");
        Mockito.when(accountsDaoV0.getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class)))
                .thenReturn(digitalCheck);
        //Action
        DtoIntDigitalCheck response = srvIntAccountsV0.getDigitalCheck(dtoIntFilterAccount);
        //Assert
        Mockito.verify(accountsDaoV0).getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getDigitalCheckStatus().getId(), DigitalCheckStatusEnum.CANCELED);
    }

    @Test
    public void getDigitalCheckCI() {
        //Arrenge
        DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntFilterAccount.class);
        DtoIntDigitalCheck digitalCheck = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntDigitalCheck.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_12);
        dtoIntFilterAccount.setCustomerId(CUSTOMER_ID);
        dtoIntFilterAccount.setDigitalCheckId("123456789012|1234567890|2017-100-23|CI");
        Mockito.when(accountsDaoV0.getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class)))
                .thenReturn(digitalCheck);
        //Action
        DtoIntDigitalCheck response = srvIntAccountsV0.getDigitalCheck(dtoIntFilterAccount);
        //Assert
        Mockito.verify(accountsDaoV0).getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getDigitalCheckStatus().getId(), DigitalCheckStatusEnum.CANCELED_WITHOUT_FUNDS);
    }

    @Test
    public void getDigitalCheckCD() {
        //Arrenge
        DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntFilterAccount.class);
        DtoIntDigitalCheck digitalCheck = ServiceFactorySingleton.getInstance().
                getStubObject(DtoIntDigitalCheck.class);
        dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_12);
        dtoIntFilterAccount.setCustomerId(CUSTOMER_ID);
        dtoIntFilterAccount.setDigitalCheckId("123456789012|1234567890|2017-100-23|CD");
        Mockito.when(accountsDaoV0.getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class)))
                .thenReturn(digitalCheck);
        //Action
        DtoIntDigitalCheck response = srvIntAccountsV0.getDigitalCheck(dtoIntFilterAccount);
        //Assert
        Mockito.verify(accountsDaoV0).getDigitalCheckWYRF(Mockito.any(DtoIntFilterAccount.class));
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getDigitalCheckStatus().getId(), DigitalCheckStatusEnum.EXPIRED);
    }
}
